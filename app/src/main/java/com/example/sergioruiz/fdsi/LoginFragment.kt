package com.example.sergioruiz.fdsi


import android.app.Activity.RESULT_OK
import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.os.Environment
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Toast
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import butterknife.ButterKnife
import com.example.sergioruiz.fdsi.ClientApi.RetrofitClient
import com.example.sergioruiz.fdsi.Interfaces.ApiFDSI
import com.example.sergioruiz.fdsi.Models.MSG
import com.example.sergioruiz.fdsi.Models.Usuarios
import com.example.sergioruiz.fdsi.Preferences.PreferenceHelper
import com.itextpdf.text.*
import com.itextpdf.text.pdf.*
import dmax.dialog.SpotsDialog
import com.itextpdf.text.pdf.PdfPTable
import kotlinx.android.synthetic.main.fragment_login.*
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.*
import java.lang.Exception
import java.text.DecimalFormat

/**
 * A simple [Fragment] subclass.
 */
class LoginFragment : Fragment(), View.OnClickListener {

    var apiFDSI: ApiFDSI? = null
    var entero: Int? = 20
    val user = Usuarios()
    private val filePath = "MisPdf"
    private val fileName = "Sample.pdf"
    private val LOG_TAG = "GeneratePDF"
    private var pdfFile: File? = null
    private var baseFont: BaseFont? = null
    private var pDialog: ProgressDialog? = null
    private var REQUEST_SIGNUP: Int = 0
    private var TAG = "Login"
    private lateinit var preferenceHelper: PreferenceHelper

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ButterKnife.bind(activity!!)
        preferenceHelper = PreferenceHelper(activity?.applicationContext!!)
        if (preferenceHelper.getIsLogin()) {
            findNavController().navigate(R.id.action_loginFragment_to_stablishmentFragment, null, NavOptions.Builder()
                .setPopUpTo(R.id.loginFragment, true).build())
        }
        apiFDSI = RetrofitClient().getClient()?.create(ApiFDSI::class.java)
        btnLogin.setOnClickListener(this)
       // btnSignup.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v){
            //btnLogin -> /*loginUser()*/goToStablishmentWindow()/*goToStateFragment()*/
            //btnLogin -> login()
            btnLogin -> loginUser()
           // btnSignup -> goToRegister()
            //btnSignup -> createPDFClick() //goToRegister()
        }
    }

    /*fun onLoginSuccess() {
        btnLogin.isEnabled = true
        activity?.finish()
    }

    fun login() {
        Log.d(TAG, "Login")

        if (validate() == false) {
            onLoginFailed()
            return
        }

        loginByServer()
    }

    private fun loginByServer() {
        pDialog = ProgressDialog(activity)
        pDialog?.isIndeterminate = true
        pDialog?.setMessage("Accediendo a la cuenta...")
        pDialog?.setCancelable(false)

        showDialog()

        val usr = txtUser.text.toString()
        val pass = txtPassword.text.toString()

        user.Usuario = usr
        user.Contras = pass

        val postCallLogin = apiFDSI?.loginUser(user)
        postCallLogin?.enqueue(object : Callback<MSG> {
            override fun onFailure(call: Call<MSG>, t: Throwable) {
                hidepDialog()
                Log.d("onFailure", t.message.toString())
            }

            override fun onResponse(call: Call<MSG>, response: Response<MSG>) {
                hidepDialog()
                Log.d("onResponse", response.body()?.message.toString())

                if (response.body()?.success == 1) {
                    findNavController().navigate(R.id.stablishmentFragment)
                } else {
                    Toast.makeText(activity?.baseContext!!, response.body()?.message, Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

    private fun showDialog() {
        if (!pDialog!!.isShowing) {
            pDialog?.show()
        }
    }

    private fun hidepDialog() {
        if (pDialog!!.isShowing) {
            pDialog?.dismiss()
        }
    }

    fun onLoginFailed() {
        Toast.makeText(activity?.baseContext!!, "Fallo en el logueo", Toast.LENGTH_SHORT).show()
        btnLogin.isEnabled = true
    }

    fun validate(): Boolean? {
        var valid = true
        val usr = txtUser.text.toString().trim()
        val pass = txtPassword.text.toString().trim()

        if (usr.isEmpty()) {
            txtUser.error = "Ingrese su nombre de usuario"
            requestFocus(txtUser)
            valid = false
        } else {
            txtUser.error = null
        }

        if (pass.isEmpty()) {
            txtPassword.error = "Ingrese su contraseña"
            requestFocus(txtPassword)
            valid = false
        } else {
            txtPassword.error = null
        }

        return valid
    }*/

    fun requestFocus(view: View) {
        if (view.requestFocus()) {
            activity?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
        }
    }

    /*interface OnLoginActivityListener{
        fun performLogin(name: String)
    }*/

    fun loginUser() {
        /*user.username = txtUser.text.toString()
        user.password = txtPassword.text.toString()
        val loginPostCall = apiFDSI?.loginUser(user)

        loginPostCall?.enqueue(object : Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {

            }

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    findNavController().navigate(entero!!)
                }
            }

        })*/

        /*user.Nombre = txtUser.text.toString()
        user.Contras = txtPassword.text.toString()
        val alertDialog: AlertDialog = SpotsDialog.Builder().setContext(context).build()
        alertDialog.show()
        val loginPostCall = apiFDSI?.loginUser(user)
        loginPostCall?.enqueue(object : Callback<Usuarios> {
            override fun onFailure(call: Call<Usuarios>, t: Throwable) {
                alertDialog.dismiss()
                Toast.makeText(context, "Error ${t.message}", Toast.LENGTH_SHORT).show()
            }

            override fun onResponse(call: Call<Usuarios>, response: Response<Usuarios>) {
                alertDialog.dismiss()
                if (response.isSuccessful) {
                    findNavController().navigate(R.id.stablishmentFragment)
                } else {
                    Toast.makeText(context, "Error ${response.message()}", Toast.LENGTH_SHORT).show()
                }
            }
        })*/

        //findNavController().navigate(R.id.valuationFormatFragment)

        pDialog = ProgressDialog(activity)
        pDialog?.isIndeterminate = true
        pDialog?.setMessage("Accediendo a la cuenta...")
        pDialog?.setCancelable(false)

        showDialog()

        val username = txtUser.text.toString().trim()
        val password = txtPassword.text.toString().trim()

        /*user.Usuario = username
        user.Contras = password*/

        val callUser = apiFDSI?.loginUser(username, password)
        callUser?.enqueue(object : Callback<String> {
            override fun onFailure(call: Call<String>, t: Throwable) {
            }

            override fun onResponse(call: Call<String>, response: Response<String>) {
                hidepDialog()
                Log.i("ResponseString", response.body().toString())
                if (response.isSuccessful) {
                    if (response.body() != null) {
                        Log.i("onSuccess", response.body().toString())
                        val jsonresponse = response.body().toString()
                        parseLoginData(jsonresponse)
                    } else {
                        Log.i("onEmptyResponse", "Returned empty response")
                    }
                }
            }
        })
    }

    private fun showDialog() {
        if (!pDialog!!.isShowing) {
            pDialog?.show()
        }
    }

    private fun hidepDialog() {
        if (pDialog!!.isShowing) {
            pDialog?.dismiss()
        }
    }

    private fun parseLoginData(response: String) {
        try {
            val jsonObject = JSONObject(response)
            if (jsonObject.getString("status").equals("true")) {
                saveInfo(response)
                /*findNavController().navigate(R.id.action_loginFragment_to_stablishmentFragment, null, NavOptions.Builder()
                    .setPopUpTo(R.id.loginFragment, true).build())*/
                findNavController().navigate(R.id.stablishmentFragment)
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

    private fun saveInfo(response: String) {
        preferenceHelper?.putIsLogin(true)
        try {
            val jsonObject = JSONObject(response)
            if (jsonObject.getString("status").equals("true")) {
                val dataArray = jsonObject.getJSONArray("data")
                for (i in 0 until dataArray.length()) {
                    val dataObj = dataArray.getJSONObject(i)
                    preferenceHelper?.putNombre(dataObj.getString("Nombre"))
                    preferenceHelper?.putUsuario(dataObj.getString("Usuario"))
                    //preferenceHelper?.putContras(dataObj.getString("Contras"))
                }
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

    private fun goToStablishmentWindow() {
        findNavController().navigate(R.id.stablishmentFragment)
    }

    private fun goToStateFragment() {
        findNavController().navigate(R.id.statesFragment)
    }

    private fun createPDFClick() {
        val personName = txtUser.text.toString()
        pdfFile = File(activity?.getExternalFilesDir(filePath), "Archivo_${txtUser.text.toString()}.pdf")
        generatePDF(personName)
    }

    fun goToRegister() {
        findNavController().navigate(R.id.registerFragment)
    }

    fun generatePDF(personName: String) {
        val document = Document()
        try {
            val docWriter = PdfWriter.getInstance(document, FileOutputStream(pdfFile))
            document.open()

            val cb = docWriter.directContent
            initializeFonts()
            val inputStream = activity?.assets!!.open("fdsiimagen.png")
            val bitmap = BitmapFactory.decodeStream(inputStream)
            val stream = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream)
            val companyLogo = Image.getInstance(stream.toByteArray())
            companyLogo.setAbsolutePosition(25f, 700f)
            companyLogo.scalePercent(25f)
            document.add(companyLogo)
            createHeadings(cb, 400f, 780f, "Usuario")
            createHeadings(cb, 400f, 765f, "Contraseña")

            val columnWidths = floatArrayOf(2f, 2f)
            val table = PdfPTable(columnWidths)
            table.setTotalWidth(300f)
            // INSERT CODE HERE CACHARPO
            //002180701416609911
            var cell = PdfPCell(Phrase("Usuario"))
            cell.horizontalAlignment = Element.ALIGN_RIGHT
            table.addCell(cell)

            cell = PdfPCell(Phrase("Contraseña"))
            cell.horizontalAlignment = Element.ALIGN_LEFT
            table.addCell(cell)
            table.headerRows = 1

            //val df = DecimalFormat("0.00")
            /*for (i in 0..14) {
            }*/
            table.writeSelectedRows(0, -1, document.leftMargin(), 650f, docWriter.directContent)
            createHeadings(cb, 450f, 150f, personName)
            document.close()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun createHeadings(cb: PdfContentByte, x: Float, y: Float, text: String) {
        cb.beginText()
        cb.setFontAndSize(baseFont, 8f)
        cb.setTextMatrix(x, y)
        cb.showText(text.trim())
        cb.endText()
    }

    private fun isExternalStorageReadOnly(): Boolean {
        val extStorageState = Environment.getExternalStorageState()
        if (Environment.MEDIA_MOUNTED_READ_ONLY == extStorageState) {
            return true
        }
        return false
    }

    private fun isExternalStorageAvailable(): Boolean {
        val extStorageState = Environment.getExternalStorageState()
        if (Environment.MEDIA_MOUNTED == extStorageState) {
            return true
        }
        return false
    }

    private fun initializeFonts() {
        try {
            baseFont = BaseFont.createFont(BaseFont.HELVETICA_BOLD, BaseFont.CP1252, BaseFont.EMBEDDED)
        } catch (e: DocumentException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    /*private fun checkExternalStorage() {
        if (!isExternalStorageAvailable() || isExternalStorageReadOnly()) {
            Log.v(LOG_TAG, "Almacenamiento externo no disponible")
        } else {
            pdfFile = File(activity?.getExternalFilesDir(filePath), "Archivo_${txtUser.text.toString()}.pdf")
        }
    }*/

    fun goToSignatureWindow() {
        findNavController().navigate(R.id.digitalSignatureFragment)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_SIGNUP) {
            if (resultCode == RESULT_OK) {
                activity?.finish()
            }
        }
    }

}
