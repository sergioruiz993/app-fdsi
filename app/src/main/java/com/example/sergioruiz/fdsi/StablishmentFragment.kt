package com.example.sergioruiz.fdsi


import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.example.sergioruiz.fdsi.Adapters.StablishmentAdapter
import com.example.sergioruiz.fdsi.Models.AdapterModel
import com.example.sergioruiz.fdsi.Preferences.PreferenceHelper
import kotlinx.android.synthetic.main.fragment_stablishment.*

/**
 * A simple [Fragment] subclass.
 */
class StablishmentFragment : Fragment(), View.OnClickListener {

    var gridLayoutManager: GridLayoutManager? = null
    var listStablishment: MutableList<AdapterModel> = mutableListOf()
    var stablishmentData: AdapterModel? = null
    lateinit var myAdapter: StablishmentAdapter
    private lateinit var preferenceHelper: PreferenceHelper

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        preferenceHelper = PreferenceHelper(activity?.applicationContext!!)
        return inflater.inflate(R.layout.fragment_stablishment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //imageButton.setOnClickListener(this)
        btnCloseSession.setOnClickListener(this)
        glStablishment()
    }

    override fun onClick(v: View?) {
        when(v) {
            btnCloseSession -> closeSession()
        }
    }

    private fun closeSession() {
        preferenceHelper.putIsLogin(false)
        findNavController().navigate(R.id.loginFragment)
    }

    fun glStablishment() {
        gridLayoutManager = GridLayoutManager(activity?.applicationContext, 2)
        rvStablishment.layoutManager = gridLayoutManager

        stablishmentData = AdapterModel("Soriana", R.drawable.soriana)
        listStablishment.add(stablishmentData!!)
        stablishmentData = AdapterModel("Aeromexico", R.drawable.aeromexico)
        listStablishment.add(stablishmentData!!)
        stablishmentData = AdapterModel("CECAP", R.drawable.cecap)
        listStablishment.add(stablishmentData!!)
        stablishmentData = AdapterModel("UH Cardiologia", R.drawable.cardiologia)
        listStablishment.add(stablishmentData!!)
        stablishmentData = AdapterModel("Club Premier 250", R.drawable.clubpremier)
        listStablishment.add(stablishmentData!!)
        stablishmentData = AdapterModel("FDSI", R.drawable.fdsi)
        listStablishment.add(stablishmentData!!)
        stablishmentData = AdapterModel("Swiss Re", R.drawable.swissre)
        listStablishment.add(stablishmentData!!)
        stablishmentData = AdapterModel("Servicios Bunge", R.drawable.bunge)
        listStablishment.add(stablishmentData!!)
        stablishmentData = AdapterModel("Polymershapes", R.drawable.polymershapes)
        listStablishment.add(stablishmentData!!)
        stablishmentData = AdapterModel("Banorte", R.drawable.banorte)
        listStablishment.add(stablishmentData!!)
        stablishmentData = AdapterModel("Si Vale", R.drawable.sivale)
        listStablishment.add(stablishmentData!!)
        stablishmentData = AdapterModel("Manufacturera Mexicana", R.drawable.manufacturera)
        listStablishment.add(stablishmentData!!)
        stablishmentData = AdapterModel("Molinos Bunge", R.drawable.molinosbounge)
        listStablishment.add(stablishmentData!!)
        stablishmentData = AdapterModel("Travel R´us", R.drawable.travelrus)
        listStablishment.add(stablishmentData!!)
        stablishmentData = AdapterModel("Timex", R.drawable.timex)
        listStablishment.add(stablishmentData!!)
        stablishmentData = AdapterModel("Dist. e Imp. Alsea", R.drawable.alsea)
        listStablishment.add(stablishmentData!!)
        stablishmentData = AdapterModel("Casa Cerro", R.drawable.casacerro)
        listStablishment.add(stablishmentData!!)
        stablishmentData = AdapterModel("Philips", R.drawable.philips)
        listStablishment.add(stablishmentData!!)
        stablishmentData = AdapterModel("Flexigrip de Mexico", R.drawable.flexigrip)
        listStablishment.add(stablishmentData!!)
        stablishmentData = AdapterModel("Bufete Diaz Miron y Asociados", R.drawable.bufete)
        listStablishment.add(stablishmentData!!)
        stablishmentData = AdapterModel("Villa Tours", R.drawable.villatours)
        listStablishment.add(stablishmentData!!)
        stablishmentData = AdapterModel("WSP Mexico", R.drawable.wsp)
        listStablishment.add(stablishmentData!!)
        stablishmentData = AdapterModel("Comsustenta", R.drawable.comsustenta)
        listStablishment.add(stablishmentData!!)
        stablishmentData = AdapterModel("Com. y Serv. Bexica", R.drawable.bexica)
        listStablishment.add(stablishmentData!!)
        stablishmentData = AdapterModel("7 eleven", R.drawable.eleven)
        listStablishment.add(stablishmentData!!)
        stablishmentData = AdapterModel("Grupo Internacional del Descanso", R.drawable.grupo)
        listStablishment.add(stablishmentData!!)
        stablishmentData = AdapterModel("Inmobiliaria Vigas", R.drawable.vigas)
        listStablishment.add(stablishmentData!!)

        myAdapter = StablishmentAdapter(context, listStablishment!!)
        rvStablishment.adapter = myAdapter
    }
}



