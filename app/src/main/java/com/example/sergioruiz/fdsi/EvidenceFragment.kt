package com.example.sergioruiz.fdsi


import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity.RESULT_OK
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.fragment_evidence.*
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*


/**
 * A simple [Fragment] subclass.
 */
class EvidenceFragment : Fragment(), View.OnClickListener {

    private var file: File? = null
    private var directoryName = "Evidencias FDSI"
    private var deviceIdentifier: String? = null
    private var currentPhotoPath: String? = null
    private var REQUEST_PICTURE_CAPTURE: Int? = 1

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_evidence, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        imgEvidencia.setOnClickListener(this)
        btnGoToSignature.setOnClickListener(this)
        getInstallationIndentifier()
        if (ContextCompat.checkSelfPermission(activity?.applicationContext!!, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity!!, arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE), 0)
            if (!activity?.packageManager!!.hasSystemFeature(PackageManager.FEATURE_CAMERA)){
                imgEvidencia.isEnabled = false
            }
        }
        val myDrawable = activity?.resources?.getDrawable(R.drawable.camara)
        imgEvidencia.setImageDrawable(myDrawable)
    }

    override fun onClick(v: View?) {
        when(v) {
            imgEvidencia -> capture()
            btnGoToSignature -> findNavController().navigate(R.id.digitalSignatureFragment)
        }
    }

    private fun capture() {
        if (activity?.packageManager!!.hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            sendTakePictureIntent()
        }
    }

    private fun sendTakePictureIntent() {
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        cameraIntent.putExtra(MediaStore.EXTRA_FINISH_ON_COMPLETION, true)

        if (cameraIntent.resolveActivity(activity?.packageManager!!) != null) {
            try {
                file = getPictureFile()
            } catch (ex: IOException) {
                displayMessage(activity?.baseContext!!, "El archivo de foto no puede ser creada, por favor intentelo de nuevo")
                return
            }

            if (file != null) {
                val photoUri = FileProvider.getUriForFile(activity?.applicationContext!!, "com.example.sergioruiz.fdsi.fileprovider", file!!)
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri)
                startActivityForResult(cameraIntent, REQUEST_PICTURE_CAPTURE!!)
            }
        }
    }

    @SuppressLint("SimpleDateFormat")
    @Throws(IOException::class)
    private fun getPictureFile(): File? {
        val timeStamp = SimpleDateFormat("yyyyMMddHHmmss").format(Date())
        val pictureFile = "IMG_$timeStamp"
        //val storageDir = activity?.getExternalFilesDir(Environment.DIRECTORY_PICTURES)!!
        val storageDir = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), directoryName)
        if (!storageDir.exists()) {
            if (!storageDir.mkdirs()) {
                Log.d("FDSI", "failed to create directory")
                return null
            }
        }
        val image = File.createTempFile(pictureFile, ".jpg", storageDir)
        currentPhotoPath = image.absolutePath
        return image
    }

    private fun addToGallery() {
        val galleryIntent = Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE)
        val f = File(currentPhotoPath!!)
        val picUri = Uri.fromFile(f)
        galleryIntent.data = picUri
        activity?.sendBroadcast(galleryIntent)
        displayMessage(activity?.baseContext!!, "Se ha guardado la foto exitosamente")
    }

    @SuppressLint("CommitPrefEdits")
    @Synchronized
    protected fun getInstallationIndentifier(): String? {
        if (deviceIdentifier == null) {
            val sharedPref = activity?.getSharedPreferences("DEVICE_ID", Context.MODE_PRIVATE)
            deviceIdentifier = sharedPref?.getString("DEVICE_ID", null)
            if (deviceIdentifier == null) {
                deviceIdentifier = UUID.randomUUID().toString()
                val editor = sharedPref?.edit()
                editor?.putString("DEVICE_ID", deviceIdentifier)
                editor?.apply()
            }
        }
        return deviceIdentifier
    }

    private fun displayMessage(context: Context, message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_PICTURE_CAPTURE && resultCode == RESULT_OK) {
            val imgFile = File(currentPhotoPath!!)
            if (imgFile.exists()) {
                imgEvidencia.setImageDrawable(null)
                imgEvidencia.setImageURI(Uri.fromFile(imgFile))
            }
            addToGallery()
        }
    }
}
