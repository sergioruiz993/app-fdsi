package com.example.sergioruiz.fdsi


import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Rect
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import android.view.inputmethod.InputMethodInfo
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.navigation.fragment.findNavController
import com.github.gcacace.signaturepad.views.SignaturePad
import kotlinx.android.synthetic.main.fragment_digital_signature.*
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.OutputStreamWriter

/**
 * A simple [Fragment] subclass.
 */
class DigitalSignatureFragment : Fragment(), View.OnClickListener, SignaturePad.OnSignedListener {

    private var REQUEST_EXTERNAL_STORAGE: Int = 1
    private val PERMISSIONS_STORAGE = arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        //val v = inflater.inflate(R.layout.fragment_digital_signature, container, false)
        /*v.setOnTouchListener(object : View.OnTouchListener{
            override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                etNameSignature.clearFocus()
                return true
            }
        })*/
        return inflater.inflate(R.layout.fragment_digital_signature, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        verifyStoragePermissions(activity)
        btnBackTo.setOnClickListener(this)
        btnClean.setOnClickListener(this)
        btnSaveSignature.setOnClickListener(this)
        signature_pad.setOnSignedListener(this)
        scAreaFirmas.setOnTouchListener(object : View.OnTouchListener{
            override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                //etNameSignature.clearFocus()
                /*val imm = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(activity?.currentFocus?.windowToken, 0)*/
                val imm = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(activity?.currentFocus?.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
                return true
            }
        })
    }

    override fun onClick(v: View?) {
        when(v) {
            btnClean -> cleanSignature()
            btnSaveSignature -> saveSignatures()
            btnBackTo -> backTo()
        }
    }

    override fun onStartSigning() {
        //Toast.makeText(activity, "Ingrese su firma", Toast.LENGTH_SHORT).show()
    }

    override fun onClear() {
        btnClean.isEnabled = false
        btnSaveSignature.isEnabled = false
    }

    override fun onSigned() {
        btnClean.isEnabled = true
        btnSaveSignature.isEnabled = true
    }


    fun cleanSignature() {
        signature_pad.clear()
        etNameSignature.text.clear()
    }

    fun saveSignatures() {
        val signatureBitmap = signature_pad.signatureBitmap
        val signatureSvgs = signature_pad.signatureSvg
        if (addJPGSignatureToGallery(signatureBitmap)!!) {
            Toast.makeText(activity, "No se puede guardar la firma", Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(activity, "Firma guardada dentro de la galeria", Toast.LENGTH_SHORT).show()
        }

        if (addSvgSignatureToGallery(signatureSvgs)!!) {
            Toast.makeText(activity, "No se puede guardar la firma SVG", Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(activity, "La firma SVG se guardo dentro de la galeria", Toast.LENGTH_SHORT).show()
        }
    }

    fun backTo() {
        val imm = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(activity?.currentFocus?.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
        findNavController().navigate(R.id.loginFragment)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        //super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when(requestCode) {
            REQUEST_EXTERNAL_STORAGE -> {
                if (grantResults.size <= 0 || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(activity, "No se puede escribir en una galeria externa", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    fun getAlbumStorageDir(albumName: String): File? {
        val file = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), albumName)
        if (!file.mkdirs()) {
            Log.e("SignaturePad", "Directorio no creado")
        }
        return file
    }

    @Throws(IOException::class)
    private fun saveBitmapToJPG(bitmap: Bitmap?, photo: File?) {
        val newBitmap = Bitmap.createBitmap(bitmap!!.width, bitmap.height, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(newBitmap)
        canvas.drawColor(Color.parseColor("#FFFFFF"))
        canvas.drawBitmap(bitmap, 0f, 0f, null)
        val stream = FileOutputStream(photo)
        newBitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream)
        stream.close()
    }

    fun addJPGSignatureToGallery(signature: Bitmap): Boolean? {
        var result = false
        try {
            val nombreFirma = etNameSignature.text.toString()
            //val photo = File(getAlbumStorageDir("FirmasFDSI"), String.format("Firma_%d.jpg", System.currentTimeMillis()))
            val photo = File(getAlbumStorageDir("FirmasFDSI"), String.format("Firma_${nombreFirma}.jpg", System.currentTimeMillis()))
            saveBitmapToJPG(signature, photo)
            scanMediaFile(photo)
            result = true
        }catch (e: IOException) {
            e.printStackTrace()
        }
        return result
    }

    private fun scanMediaFile(photo: File?) {
        val mediaScanIntent = Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE)
        val contentUri = Uri.fromFile(photo)
        mediaScanIntent.setData(contentUri)
        activity?.sendBroadcast(mediaScanIntent)
    }

    fun addSvgSignatureToGallery(signatureSvg: String): Boolean? {
        var result = false
        try {
            val svgFile = File("FirmasFDSI", String.format("Firma_%d.svg", System.currentTimeMillis()))
            val stream = FileOutputStream(svgFile)
            val writer = OutputStreamWriter(stream)
            writer.write(signatureSvg)
            writer.close()
            stream.flush()
            stream.close()
            scanMediaFile(svgFile)
            result = true
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return result
    }

    fun verifyStoragePermissions(activity: Activity?) {
        val permission = ActivityCompat.checkSelfPermission(activity!!, Manifest.permission.WRITE_EXTERNAL_STORAGE)

        if (permission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity, PERMISSIONS_STORAGE, REQUEST_EXTERNAL_STORAGE)
        }
    }
}
