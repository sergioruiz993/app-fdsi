package com.example.sergioruiz.fdsi


import android.app.ProgressDialog
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import com.example.sergioruiz.fdsi.ClientApi.RetrofitClient
import com.example.sergioruiz.fdsi.Interfaces.ApiFDSI
import com.example.sergioruiz.fdsi.Models.Usuarios
import com.example.sergioruiz.fdsi.Preferences.PreferenceHelper
import kotlinx.android.synthetic.main.fragment_register.*
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * A simple [Fragment] subclass.
 */
class RegisterFragment : Fragment(), View.OnClickListener {

    private var apiFDSI: ApiFDSI? = null
    private var pDialog: ProgressDialog? = null
    private lateinit var preferenceHelper: PreferenceHelper

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_register, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        apiFDSI = RetrofitClient().getClient()?.create(ApiFDSI::class.java)
        preferenceHelper = PreferenceHelper(activity?.applicationContext!!)
        if (preferenceHelper.getIsLogin()) {
            findNavController().navigate(R.id.stablishmentFragment)
        }
        btnRegister.setOnClickListener(this)
        btnSignin.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v) {
            btnRegister -> userRegister()
            btnSignin -> goToLogin()
        }
    }

    fun userRegister() {
        pDialog = ProgressDialog(activity)
        pDialog?.isIndeterminate = true
        pDialog?.setMessage("Registrando cuenta nueva...")
        pDialog?.setCancelable(false)

        showDialog()

        val name = txtname.text.toString().trim()
        val user = txtUsername.text.toString().trim()
        val pass = txtPassword.text.toString().trim()

        /*users.Nombre = name
        users.Usuario = user
        users.Contras = pass*/

        when {
            name.isEmpty() -> {
                txtname.error = "Este campo es importante"
                txtname.requestFocus()
            }
            user.isEmpty() -> {
                txtUsername.error = "Este campo es importante"
                txtUsername.requestFocus()
            }
            pass.isEmpty() -> {
                txtPassword.error = "Este campo es requerido"
                txtPassword.requestFocus()
            }
        }

        val createPostCall = apiFDSI?.registerUser(name, user, pass)
        createPostCall?.enqueue(object : Callback<String>{
            override fun onFailure(call: Call<String>, t: Throwable) {
            }

            override fun onResponse(call: Call<String>, response: Response<String>) {
                hidepDialog()
                Log.i("ResponseString", response.body().toString())
                if (response.isSuccessful) {
                    if (response.body() != null) {
                        Log.i("onSuccess" , response.body().toString())

                        val jsonResponse = response.body().toString()
                        try {
                            parseRegData(jsonResponse)
                            //Toast.makeText(activity?.baseContext!!, "Usuario registrado exitosamente", Toast.LENGTH_SHORT).show()
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                    } else {
                        Log.i("onEmptyResponse", "Returned empty response")
                    }
                }
            }
        })
    }

    private fun showDialog() {
        if (!pDialog!!.isShowing) {
            pDialog?.show()
        }
    }

    private fun hidepDialog() {
        if (pDialog!!.isShowing) {
            pDialog?.dismiss()
        }
    }

    /*@Throws(JSONException::class)
    private fun parseRegData(response: String) {
        val jsonObject = JSONObject(response)
        if (jsonObject.optString("status").equals("true")) {
            saveInfo(response)
            Toast.makeText(activity?.baseContext!!, jsonObject.getString("message"), Toast.LENGTH_SHORT).show()
            val navOptions = NavOptions.Builder()
                .setPopUpTo(R.id.stablishmentFragment, true)
                .setLaunchSingleTop(true)
                .build()
            findNavController().navigate(R.id.loginFragment, null, navOptions)
        } else {
            Toast.makeText(activity?.baseContext!!, jsonObject.getString("message"), Toast.LENGTH_SHORT).show()
        }
    }*/

    private fun parseRegData(response: String) {
        try {
            val jsonObject = JSONObject(response)
            if (jsonObject.optString("status").equals("true")) {
                saveInfo(response)
                Toast.makeText(activity?.baseContext!!, jsonObject.getString("message"), Toast.LENGTH_SHORT).show()
                /*val navOptions = NavOptions.Builder()
                    .setPopUpTo(R.id.stablishmentFragment, true)
                    .setLaunchSingleTop(true)
                    .build()*/
                findNavController().navigate(R.id.loginFragment)
            } else {
                Toast.makeText(activity?.baseContext!!, jsonObject.getString("message"), Toast.LENGTH_SHORT).show()
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

    private fun saveInfo(response: String) {
        preferenceHelper.putIsLogin(true)
        try {
            val jsonObject = JSONObject(response)
            if (jsonObject.getString("status").equals("true")) {
                val dataArray = jsonObject.getJSONArray("data")
                for (i in 0 until dataArray.length()) {
                    val dataObj = dataArray.getJSONObject(i)
                    preferenceHelper.putNombre(dataObj.getString("Nombre"))
                    preferenceHelper.putUsuario(dataObj.getString("Usuario"))
                }
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

    fun goToLogin() {
        findNavController().navigate(R.id.loginFragment)
    }

}
