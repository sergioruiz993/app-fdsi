package com.example.sergioruiz.fdsi


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.sergioruiz.fdsi.Adapters.ListReportsAdapter
import com.example.sergioruiz.fdsi.BaseDatosSQLite.SQLiteClass
import com.example.sergioruiz.fdsi.Models.ReportsModel
import kotlinx.android.synthetic.main.fragment_list_evaluation.*

/**
 * A simple [Fragment] subclass.
 */
class ListEvaluation : Fragment(), View.OnClickListener {

    var reports: MutableList<ReportsModel> = mutableListOf()
    private lateinit var myAdapterList: ListReportsAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_list_evaluation, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnBackToValuation.setOnClickListener(this)
        /*val bundleSoriana = arguments?.getBundle("bundle1")
        val bundleBanorte = arguments?.getBundle("bundle2")*/

        //btnReturn.setOnClickListener(this)

        setAdapterSoriana()
    }

    private fun setAdapterSoriana() {
        myAdapterList = ListReportsAdapter(activity, reports)
        rvListValuation.layoutManager = LinearLayoutManager(context)
        rvListValuation.itemAnimator = DefaultItemAnimator()
        retrieveSoriana()
    }

    private fun setAdapterBanorte() {
        myAdapterList = ListReportsAdapter(activity, reports)
        rvListValuation.layoutManager = LinearLayoutManager(context)
        rvListValuation.itemAnimator = DefaultItemAnimator()
        retrieveBanorte()
    }

    /*private fun retrieve() {
        val sql = SQLiteClass(activity)
        reports.clear()
        val database = sql.readableDatabase
        val cursor = sql.obtenerTodo(database)
        print(cursor.count.toString())
        if (cursor.count > 0) {
            while (cursor.moveToNext()) {
                var cliente = cursor.getString(1)
                var sucursal = cursor.getString(2)
                var fecha_elaboracion = cursor.getString(6)

                val rep = ReportsModel(cliente, sucursal, fecha_elaboracion)
                reports.add(rep)
            }
        } else {
            Toast.makeText(activity?.applicationContext!!, "No hay datos", Toast.LENGTH_SHORT).show()
        }

        if (!(reports.size < 1)) {
            rvListValuation.adapter = myAdapterList
        }
    }*/

    private fun retrieveSoriana() {
        val bundle = arguments?.getBundle("bundle1")
        if (bundle != null) {
            val cliente = arguments?.getBundle("bundle1")?.getString("clientes")
            val fecha = arguments?.getBundle("bundle1")?.getString("fechas")

            val sql = SQLiteClass(activity)
            reports.clear()
            val database = sql.readableDatabase
            val cursor = sql.obtener(fecha!!, cliente!!, database)
            print(cursor.count.toString())
            if (cursor.count > 0) {
                while (cursor.moveToNext()) {
                    var cliente = cursor.getString(1)
                    var sucursal = cursor.getString(2)
                    var fecha_elaboracion = cursor.getString(6)

                    val rep = ReportsModel(cliente, sucursal, fecha_elaboracion)
                    reports.add(rep)
                }
            } else {
                Toast.makeText(activity?.applicationContext!!, "No hay datos", Toast.LENGTH_SHORT).show()
            }

            if (!(reports.size < 1)) {
                rvListValuation.adapter = myAdapterList
            }
        }
    }

    private fun retrieveBanorte() {
        val bundle = arguments?.getBundle("bundle2")
        if (bundle != null) {
            val cliente = arguments?.getBundle("bundle2")?.getString("clientes")
            val fecha = arguments?.getBundle("bundle2")?.getString("fechas")

            val sql = SQLiteClass(activity)
            reports.clear()
            val database = sql.readableDatabase
            val cursor = sql.obtenerBanorte(fecha!!, cliente!!, database)
            print(cursor.count.toString())
            if (cursor.count > 0) {
                while (cursor.moveToNext()) {
                    var cliente = cursor.getString(1)
                    var sucursal = cursor.getString(2)
                    var fecha_elaboracion = cursor.getString(6)

                    val rep = ReportsModel(cliente, sucursal, fecha_elaboracion)
                    reports.add(rep)
                }
            } else {
                Toast.makeText(activity?.applicationContext!!, "No hay datos", Toast.LENGTH_SHORT).show()
            }

            if (!(reports.size < 1)) {
                rvListValuation.adapter = myAdapterList
            }
        }
    }

    override fun onClick(v: View?) {
        when(v) {
            btnBackToValuation -> goToStates()
            //btnReturn -> returnToStablishments()
        }
    }

    fun goToStates() {
        findNavController().navigate(R.id.valuationFormatFragment)
    }

    fun returnToStablishments() {
        findNavController().navigate(R.id.stablishmentFragment)
    }
}
