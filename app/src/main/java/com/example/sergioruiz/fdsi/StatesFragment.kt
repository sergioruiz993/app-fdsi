package com.example.sergioruiz.fdsi


import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.core.view.get
import androidx.core.view.isEmpty
import androidx.navigation.fragment.findNavController
import com.example.sergioruiz.fdsi.BaseDatosSQLite.SQLiteClass
import kotlinx.android.synthetic.main.fragment_states.*
import kotlinx.android.synthetic.main.fragment_states.btnSpinners
import kotlinx.android.synthetic.main.fragment_valuation_format.*

/**
 * A simple [Fragment] subclass.
 */
class StatesFragment : Fragment(), View.OnClickListener {

    private var spinnerCities: Array<String>? = null
    //private var spinnerStates: Array<String>? = null
    private var spinnerStablishments: Array<String>? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_states, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val position = arguments?.getString("position")

        when(position) {
            "0" -> {
                var spinnerStates = arrayOf("Selecciona un Estado", "Estado de México", "CDMX", "Nuevo Leon")

                //spinnerStat.getSpinner().onItemSelectedListener = this

                val aa = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStates!!)

                aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

                spinnerStat.setAdapter(aa)
            }
            "1" -> {
                var spinnerStates = arrayOf("Selecciona un Estado", "CDMX")
                val aa = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStates)
                aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                spinnerStat.setAdapter(aa)
            }
            "2" -> {
                var spinnerStates = arrayOf("Selecciona un Estado", "CDMX")

                val aa = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStates!!)

                aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

                spinnerStat.setAdapter(aa)
            }

            "3" -> {
                var spinnerStates = arrayOf("Selecciona un Estado", "CDMX")

                val aa = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStates!!)

                aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

                spinnerStat.setAdapter(aa)
            }
            "4" -> {
                var spinnerStates = arrayOf("Selecciona un Estado", "CDMX")

                val aa = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStates!!)

                aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

                spinnerStat.setAdapter(aa)
            }
            "5" -> {
                var spinnerStates = arrayOf("Selecciona un Estado", "CDMX")

                val aa = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStates!!)

                aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

                spinnerStat.setAdapter(aa)
            }
            "6" -> {
                var spinnerStates = arrayOf("Selecciona un Estado","CDMX")

                val aa = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStates!!)

                aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

                spinnerStat.setAdapter(aa)
            }
            "7" -> {
                var spinnerStates = arrayOf("Selecciona un Estado","CDMX")

                val aa = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStates!!)

                aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

                spinnerStat.setAdapter(aa)
            }
            "8" -> {
                var spinnerStates = arrayOf("Selecciona un Estado", "CDMX", "Estado de México", "Nuevo Leon", "Jalisco", "Chihuahua", "Guanajuato", "Queretaro")

                val aa = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStates!!)

                aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

                spinnerStat.setAdapter(aa)
            }
            "9" -> {
                var spinnerStates = arrayOf("Selecciona un Estado", "Estado de México", "CDMX")

                val aa = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStates!!)

                aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

                spinnerStat.setAdapter(aa)
            }
            "10" -> {
                var spinnerStates = arrayOf("Selecciona un Estado", "Estado de México", "CDMX", "Baja California Norte", "Queretaro", "Jalisco")

                val aa = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStates!!)

                aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

                spinnerStat.setAdapter(aa)
            }
            "11" -> {
                var spinnerStates = arrayOf("Selecciona un Estado", "CDMX")

                val aa = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStates!!)

                aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

                spinnerStat.setAdapter(aa)
            }
            "12" -> {
                var spinnerStates = arrayOf("Selecciona un Estado", "Yucatan")

                val aa = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStates!!)

                aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

                spinnerStat.setAdapter(aa)
            }
            "13" -> {
                var spinnerStates = arrayOf("Selecciona un Estado", "CDMX")

                val aa = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStates!!)

                aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

                spinnerStat.setAdapter(aa)
            }

            "14" -> {
                var spinnerStates = arrayOf("Selecciona un Estado", "Estado De Mexico")

                val aa = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStates!!)

                aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

                spinnerStat.setAdapter(aa)
            }
            "15" -> {
                var spinnerStates = arrayOf("Selecciona un Estado", "Estado de México", "CDMX")

                val aa = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStates!!)

                aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

                spinnerStat.setAdapter(aa)
            }
            "16" -> {
                var spinnerStates = arrayOf("Selecciona un Estado","CDMX")

                val aa = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStates!!)

                aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

                spinnerStat.setAdapter(aa)
            }
            "17" -> {
                var spinnerStates = arrayOf("Selecciona un Estado","Estado de Mexico")

                val aa = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStates!!)

                aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

                spinnerStat.setAdapter(aa)
            }
            "18" -> {
                var spinnerStates = arrayOf("Selecciona un Estado", "CDMX")

                val aa = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStates!!)

                aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

                spinnerStat.setAdapter(aa)
            }
            "19" -> {
                var spinnerStates = arrayOf("Selecciona un Estado", "CDMX")

                val aa = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStates!!)

                aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

                spinnerStat.setAdapter(aa)
            }

            "20" -> {
                var spinnerStates = arrayOf("Selecciona un Estado", "CDMX")

                val aa = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStates!!)

                aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

                spinnerStat.setAdapter(aa)
            }
            "21" -> {
                var spinnerStates = arrayOf("Selecciona un Estado", "Yucatan")

                val aa = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStates!!)

                aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

                spinnerStat.setAdapter(aa)
            }
            "22" -> {
                var spinnerStates = arrayOf("Selecciona un Estado", "Estado de México")

                val aa = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStates!!)

                aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

                spinnerStat.setAdapter(aa)
            }
            "23" -> {
                var spinnerStates = arrayOf("Selecciona un Estado","Estado de México")

                val aa = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStates!!)

                aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

                spinnerStat.setAdapter(aa)
            }
            "24" -> {
                var spinnerStates = arrayOf("Selecciona un Estado", "Estado de México", "CDMX")

                val aa = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStates!!)

                aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

                spinnerStat.setAdapter(aa)
            }
            "25" -> {
                var spinnerStates = arrayOf("Selecciona un Estado","Estado de Mexico")

                val aa = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStates!!)

                aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

                spinnerStat.setAdapter(aa)
            }
            "26" -> {
                var spinnerStates = arrayOf("Selecciona un Estado", "Estado de Mexico")

                val aa = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStates!!)

                aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

                spinnerStat.setAdapter(aa)
            }
        }

        /*spinnerStates = arrayOf("Selecciona un Estado", "Estado de México", "CDMX", "Nuevo Leon")

        //spinnerStat.getSpinner().onItemSelectedListener = this

        val aa = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStates!!)

        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        spinnerStat.setAdapter(aa)*/

        spinnerStat.setLabel("Estado")
        spinnerCity.setLabel("Ciudad")
        spinnerStablishment.setLabel("Establecimientos")

        spinnerStat.getSpinner().onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                val position = arguments?.getString("position")

                if (spinnerStat.getSpinner().selectedItem == "Selecciona un Estado") {
                    spinnerStat.setError("Por favor seleccione un estado")
                } else {
                    spinnerStat.setErrorEnabled(false)
                }

                when(position) {
                    "0" -> {
                        when(spinnerStat.getSpinner().selectedItem) {
                            "Estado de México" -> {
                                EstadoDeMexicoCitiesSoriana()
                            }
                            "CDMX" -> {
                                CDMXCitiesSoriana()
                            }
                            "Nuevo Leon" -> {
                                NuevoLeonCitiesSoriana()
                            }
                        }
                    }
                    "1" -> {
                        when(spinnerStat.getSpinner().selectedItem) {
                            "CDMX" -> {
                                CDMXCitiesAeroMexico()
                            }
                        }
                    }
                    "2" -> {
                        when(spinnerStat.getSpinner().selectedItem) {

                            "CDMX" -> {
                                CDMXCitiesCECAP()
                            }
                        }
                    }
                    "3" -> {
                        when(spinnerStat.getSpinner().selectedItem) {
                            "CDMX" -> {
                                CDMXCitiesUHCardiologia()
                            }
                        }
                    }
                    "4" -> {
                        when(spinnerStat.getSpinner().selectedItem) {

                            "CDMX" -> {
                                CDMXCitiesClubPremier()
                            }
                        }
                    }
                    "5" -> {
                        when(spinnerStat.getSpinner().selectedItem) {
                            "CDMX" -> {
                                CDMXCitiesFDSIService()
                            }
                        }
                    }
                    "6" -> {
                        when(spinnerStat.getSpinner().selectedItem) {
                            "CDMX" -> {
                                CDMXCitiesSwissRe()
                            }
                        }
                    }

                    "7" -> {
                        when(spinnerStat.getSpinner().selectedItem) {
                            "CDMX" -> {
                                CDMXCitiesServiciosBunge()
                            }
                        }
                    }


                    "8" -> {
                        when(spinnerStat.getSpinner().selectedItem) {
                            "Estado de México" -> {
                                EstadoDeMexicoCitiesPolymershapes()
                            }
                            "CDMX" -> {
                                CDMXCitiesPolymershapes()
                            }
                            "Chihuahua" -> {
                                ChihuahuaCitiesPolymershapes()
                            }
                            "Guanajuato" -> {
                                GuanajuatoCitiesPolymershapes()
                            }
                            "Jalisco" -> {
                                JaliscoCitiesPolymershapes()
                            }
                            "Nuevo Leon" -> {
                                NuevoLeonCitiesPolymershapes()
                            }
                            "Queretaro" -> {
                                QueretaroCitiesPolymershapes()
                            }
                        }
                    }
                    "9" -> {
                        when(spinnerStat.getSpinner().selectedItem) {
                            "Estado de México" -> {
                                EstadoDeMexicoCitiesBanorte()
                            }
                            "CDMX" -> {
                                CDMXCitiesBanorte()
                            }
                        }
                    }
                    "10" -> {
                        when(spinnerStat.getSpinner().selectedItem) {
                            "Estado de México" -> {
                                EstadoDeMexicoCitiesSiVale()
                            }
                            "CDMX" -> {
                                CDMXCitiesSiVale()
                            }
                            "Baja California Norte" -> {
                                BajaCaliforniaNorteCitiesSiVale()
                            }
                            "Jalisco" -> {
                                JaliscoCitiesSiVale()
                            }
                            "Queretaro" -> {
                                QueretaroCitiesSiVale()
                            }
                        }
                    }
                    "11" -> {
                        when(spinnerStat.getSpinner().selectedItem) {

                            "CDMX" -> {
                                CDMXCitiesManufacturera()
                            }
                        }
                    }
                    "12" -> {
                        when(spinnerStat.getSpinner().selectedItem) {
                            "Yucatan" -> {
                                YucatanCitiesMolinosBunge()
                            }
                        }
                    }
                    "13" -> {
                        when(spinnerStat.getSpinner().selectedItem) {
                            "CDMX" -> {
                                CDMXCitiesTravelRUS()
                            }
                        }
                    }
                    "14" -> {
                        when(spinnerStat.getSpinner().selectedItem) {
                            "Estado De Mexico" -> {
                                EstadoDeMexicoCitiesTimex()
                            }
                        }
                    }
                    "15" -> {
                        when(spinnerStat.getSpinner().selectedItem) {

                            "CDMX" -> {
                                CDMXCitiesAlsea()
                            }

                            "Estado de México" -> {
                                EstadoDeMexicoCitiesAlsea()
                            }
                        }
                    }
                    "16" -> {
                        when(spinnerStat.getSpinner().selectedItem) {

                            "CDMX" -> {
                                CDMXCitiesCasaCerro()
                            }
                        }
                    }
                    "17" -> {
                        when(spinnerStat.getSpinner().selectedItem) {
                            "Estado de Mexico" -> {
                                EstadoDeMexicoCitiesPhilips()
                            }
                        }
                    }
                    "18" -> {
                        when(spinnerStat.getSpinner().selectedItem) {
                            "CDMX" -> {
                                CDMXCitiesFlexigrip()
                            }
                        }
                    }
                    "19" -> {
                        when(spinnerStat.getSpinner().selectedItem) {
                            "CDMX" -> {
                                CDMXCitiesBufete()
                            }
                        }
                    }
                    "20" -> {
                        when(spinnerStat.getSpinner().selectedItem) {
                            "CDMX" -> {
                                CDMXCitiesVillaTours()
                            }
                        }
                    }

                    "21" -> {
                        when(spinnerStat.getSpinner().selectedItem) {
                            "Yucatan" -> {
                                YucatanCitiesWPS()
                            }
                        }
                    }

                    "22" -> {
                        when(spinnerStat.getSpinner().selectedItem) {
                            "Estado de México" -> {
                                EstadoDeMexicoCitiesComsustenta()
                            }
                        }
                    }
                    "23" -> {
                        when(spinnerStat.getSpinner().selectedItem) {
                            "Estado de México" -> {
                                EstadoDeMexicoCitiesBexica()
                            }
                        }
                    }
                    "24" -> {
                        when(spinnerStat.getSpinner().selectedItem) {
                            "Estado de México" -> {
                                EstadoDeMexicoCitiesSevenEleven()
                            }
                            "CDMX" -> {
                                CDMXCitiesSevenEleven()
                            }
                        }
                    }
                    "25" -> {
                        when(spinnerStat.getSpinner().selectedItem) {
                            "Estado de Mexico" -> {
                                EstadoDeMexicoCitiesGrupoDescanso()
                            }
                        }
                    }

                    "26" -> {
                        when(spinnerStat.getSpinner().selectedItem) {
                            "Estado de Mexico" -> {
                                EstadoDeMexicoCitiesInmobiliariaVigas()
                            }
                        }
                    }


                }

                /*when (spinnerStat.getSpinner().selectedItem) {
                    "Estado de México" -> {
                        EstadoDeMexicoCities()
                    }
                    "CDMX" -> {
                        CDMXCities()
                    }
                    /*"Jalisco" -> {
                        JaliscoCities()
                    }*/
                    /*"Chihuahua" -> {
                        ChihuahuaCities()
                    }*/
                    "Nuevo Leon" -> {
                        NuevoLeonCities()
                    }
                    /*"Guanajuato" -> {
                        GuanajuatoCities()
                    }
                    "Queretaro" -> {
                        QueretaroCities()
                    }
                    "Baja California Norte" -> {
                        BajaCaliforniaNorteCities()
                    }*/
                }*/
            }
        }

        spinnerCity.getSpinner().onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val position = arguments?.getString("position")

                if (spinnerCity.getSpinner().selectedItem == "Selecciona una Ciudad") {
                    spinnerCity.setError("Por favor selecciona una ciudad")
                } else {
                    spinnerCity.setErrorEnabled(false)
                }

                when (position) {
                    "0" -> {
                        when (spinnerCity.getSpinner().selectedItem) {
                            "Naucalpan" -> {
                                NaucalpanStablishmentsSoriana()
                            }
                            "Cuautitlan Izcalli" -> {
                                CuautititlanIzcalliStablishmentsSoriana()
                            }
                            "Cuautitlan de Romero Rubio" -> {
                                CuautititlanRomeroRubioStablishmentsSoriana()
                            }
                            "Toluca" -> {
                                TolucaStablishmentsSoriana()
                            }
                            "Coyoacan" -> {
                                CoyoacanStablishmentsSoriana()
                            }
                            "Azcapotzalco" -> {
                                AzcapotzalcoStablishmentsSoriana()
                            }
                            "La Viga" -> {
                                La_vigaStablishmentsSoriana()
                            }
                            "El Salado" -> {
                                EL_SaladoStablishmentsSoriana()
                            }
                            "Vértiz" -> {
                                VertizStablishmentsSoriana()
                            }
                            "Polanco" -> {
                                PolancoStablishmentsSoriana()
                            }
                            "Coapa" -> {
                                CoapaStablishmentsSoriana()
                            }
                            "Iztapalapa" -> {
                                IztapalapaStablishmentsSoriana()
                            }
                            "Mixcoac" -> {
                                MixcoacStablishmentsSoriana()
                            }
                            "La Villa" -> {
                                La_VillaStablishmentsSoriana()
                            }
                            "Narvarte" -> {
                                NarvarteStablishmentsSoriana()
                            }
                            "Vallejo" -> {
                                VallejoStablishmentsSoriana()
                            }
                            "Zaragoza" -> {
                                ZaragozaStablishmentsSoriana()
                            }
                            "Tacubaya" -> {
                                TacubayaStablishmentsSoriana()
                            }
                            "Buenavista" -> {
                                BuenavistaStablishmentsSoriana()
                            }
                            "Peñon de los baños" -> {
                                Peñon_de_los_BañosStablishmentsSoriana()
                            }
                            "San Nicolas de los Garza" -> {
                                San_Nicolas_de_las_GarzatablishmentsSoriana()
                            }
                            "Colonia Del Valle" -> {
                                Colonia_Del_ValleStablishmentsSoriana()
                            }
                            "Guadalupe" -> {
                                GuadalupeStablishmentsSoriana()
                            }
                            "Monterrey" -> {
                                MonterreyStablishmentsSoriana()
                            }
                        }
                    }
                    "1" -> {
                        when(spinnerCity.getSpinner().selectedItem) {
                            "Centro" -> CentroStablishmentsAeromexico()
                            "Coyoacan" -> CoyoacanStablishmentsAeromexico()
                            "San Lazaro" -> SanLazaroStablishmentsAeromexico()
                        }
                    }

                    "2" -> {
                        when (spinnerCity.getSpinner().selectedItem) {
                            "Peñon de los Baños" -> PeñondelosbañosStablishmentsCECAP()
                        }
                    }
                    "3" -> {
                        when (spinnerCity.getSpinner().selectedItem) {
                            "Coyoacan" -> CoyoacanStablishmentsUH_Cardiologia()
                        }
                    }
                    "4" -> {
                        when (spinnerCity.getSpinner().selectedItem) {
                            "Reforma" -> ReformaStablishmentsClubPremier250()

                        }
                    }
                    "5" -> {
                        when (spinnerCity.getSpinner().selectedItem) {
                            "Azcapotzalco" -> AzcapotzalcoStablishmentsFDSI_Service()
                        }
                    }
                    "6" -> {
                        when (spinnerCity.getSpinner().selectedItem) {
                            "San Angel" -> San_AngelStablishmentsSwiss_Re()

                        }
                    }
                    "7" -> {
                        when (spinnerCity.getSpinner().selectedItem) {
                            "Cuajimalpa" -> CuajimalpaStablishmentsServicios_Bunge()
                        }
                    }
                    "8" -> {
                        when (spinnerCity.getSpinner().selectedItem) {
                            "Naucalpan" -> NaucalpanStablishmentsPolymershapes()
                        }

                        when (spinnerCity.getSpinner().selectedItem) {
                            "Tlahuac" -> TlahuacStablishmentsPolymershapes()
                        }

                        when (spinnerCity.getSpinner().selectedItem) {
                            "La Viga" -> La_VigaStablishmentsPolymershapes()
                        }

                        when (spinnerCity.getSpinner().selectedItem) {
                            "Apodaca" -> ApodacaStablishmentsPolymershapes()
                        }
                        when (spinnerCity.getSpinner().selectedItem) {
                            "Zapopan" -> ZapopanStablishmentsPolymershapes()
                        }
                        when (spinnerCity.getSpinner().selectedItem) {
                            "Chihuahua" -> ChihuahuaStablishmentsPolymershapes()
                        }
                        when (spinnerCity.getSpinner().selectedItem) {
                            "Celaya" -> CelayaStablishmentsPolymershapes()
                        }
                        when (spinnerCity.getSpinner().selectedItem) {
                            "Irapuato" -> IrapuatoStablishmentsPolymershapes()
                        }
                        when (spinnerCity.getSpinner().selectedItem) {
                            "Leon" -> LeonStablishmentsPolymershapes()
                        }
                        when (spinnerCity.getSpinner().selectedItem) {
                            "Queretaro" -> QueretaroStablishmentsPolymershapes()
                        }
                    }
                    "9" -> {
                        when (spinnerCity.getSpinner().selectedItem) {
                            "Agricola" -> AgricolasStablishmentsBanorte()
                            "Balbuena" -> BalbuenaStablishmentsBanorte()
                            "Polanco" -> PolancoStablishmentsBanorte()
                            "Perisur" -> PerisurStablishmentsBanorte()
                            "Jamaica" -> JamaicaStablishmentsBanorte()
                            "Peñon de los Baños" -> Peñon_de_los_BañosStablishmentsBanorte()
                            "Zaragoza" -> ZaragozaStablishmentsBanorte()
                            "Milpa Alta" -> Milpa_AltaStablishmentsBanorte()
                            "Granjas" -> GranjasStablishmentsBanorte()
                            "Churubusco" -> ChurubuscoStablismentsBanorte()
                            "Patriotismo" -> PatriotismoStablishmentsBanorte()
                            "Constituyentes" -> ConstituyentesStablishmentsBanorte()
                            "Santa Fe" -> Santa_FeStablishmentsBanorte()
                            "Aragon" -> AragonStablishmentsBanorte()
                            "La Viga" -> La_VigaStablishmentsBanorte()
                            "Cuajimalpa" -> CuajimalpaStablishmentsBanorte()
                            "Buenavista" -> BuenavistaStablishmentsBanorte()
                            "La Villa" -> La_VillaStablishmentsBanorte()
                            "Lindavista" -> LindavistaStablishmentsBanorte()
                            "Centro" -> CentroStablishmentsBanorte()
                            "Narvarte" -> NarvarteStablishmentsBanorte()
                            "Azcapotzalco" -> AzcapotzalcoStablishmentsBanorte()
                            "Tacubaya" -> TacubayaStablishmentsBanorte()
                            "Condesa" -> CondesaStablishmentsBanorte()
                            "Pedregal" -> PedregalStablishmentsBanorte()
                            "San Jeronimo" -> San_JeronimoStablishmentsBanorte()
                            "San Angel" -> San_AngelStablishmentsBanorte()
                            "Napoles" -> NapolesStablishmentsBanorte()
                            "Miguel Hidalgo" -> Miguel_HidalgoStablishmentsBanorte()
                            "Copilco" -> CopilcoStablishmentsBanorte()
                            "Tapo" -> TapoStablishmentsBanorte()
                            "Del Valle" -> Del_ValleStablishmentsBanorte()
                            "Coyoacan" -> CoyoacanStablishmentsBanorte()
                            "Tlalpan" -> TlalpanStablishmentsBanorte()
                            "San Lazaro" -> San_LazaroStablishmentsBanorte()
                            "Coapa" -> CoapaStablishmentsBanorte()
                            "Mixcoac" -> MixcoacStablishmentsBanorte()
                            "Atizapan" -> AtizapanStablishmentsBanorte()
                            "Coacalco" -> CoacalcoStablishmentsBanorte()
                            "Cuautitlan Izcalli" -> CuautitlanIzcalliStablishmentsBanorte()
                            "Cuautitlan de Romero Rubio" -> CuautitlanRomeroRubioStablishmentsBanorte()
                            "Ecatepec" -> EcatepecStablishmentsBanorte()
                            "Huixquilucan" -> HuixquilucanStablishmentsBanorte()
                            "Naucalpan" -> NaucalpanStablishmentsBanorte()
                            "Tecamac" -> TecamacStablishmentsBanorte()
                            "Tlalnepantla" -> TlalnepantlaStablishmentsBanorte()
                            "Toluca" -> TolucaStablishmentsBanorte()
                            "Zumpango" -> ZumpangoStablishmentsBanorte()
                        }
                    }
                    "10" -> {
                        when (spinnerCity.getSpinner().selectedItem) {
                            "Guadalajara" -> GuadalajaraStablishmentsSí_Vale()
                        }

                        when (spinnerCity.getSpinner().selectedItem) {
                            "Queretaro" -> QueretaroStablishmentsSí_Vale()
                        }

                        when (spinnerCity.getSpinner().selectedItem) {
                            "Reforma" -> ReformaStablishmentsSí_Vale()
                        }

                        when (spinnerCity.getSpinner().selectedItem) {
                            "Tijuana" -> TijuanaStablishmentsSí_Vale()
                        }
                        when (spinnerCity.getSpinner().selectedItem) {
                            "Tlalnepantla" -> TlalnepantlaStablishmentsSí_Vale()
                        }
                    }
                    "11" -> {
                        when (spinnerCity.getSpinner().selectedItem) {
                            "Vallejo" -> VallejoStablishmentsManufacturera_Mexicana_de_Partes()
                        }
                    }
                    "12" -> {
                        when (spinnerCity.getSpinner().selectedItem) {
                            "Merida" -> MeridaStablishmentsMolinos_Bunge()
                        }
                    }

                    "13" -> {
                        when (spinnerCity.getSpinner().selectedItem) {
                            "San Angel" -> San_AngelStablishmentsTravel_R_US()
                        }
                    }
                    "14" -> {
                        when (spinnerCity.getSpinner().selectedItem) {
                            "Naucalpan" -> NaucalpanStablishmentsTiempoTimex()

                        }
                    }
                    "15" -> {
                        when (spinnerCity.getSpinner().selectedItem) {
                            "Tláhuac" -> TlahuacStablishmentsDistribuidora_e_Importadora_Alsea()
                            "Cuautitlán" -> CuautitlanStablishmentsDistribuidora_e_Importadora_Alsea()
                        }
                    }

                    "16" -> {
                        when (spinnerCity.getSpinner().selectedItem) {
                            "La Villa" -> LaVillaStablishmentsCasaCerro()
                        }
                    }
                    "17" -> {
                        when (spinnerCity.getSpinner().selectedItem) {
                            "Huixquilucán" -> HuixquilucanStablishmentsPhilips_Mexico_Comercial()
                        }
                    }
                    "18" -> {
                        when (spinnerCity.getSpinner().selectedItem) {
                            "Centro" -> CentroStablishmentsFlexigrip_de_México()
                        }
                    }
                    "19" -> {
                        when (spinnerCity.getSpinner().selectedItem) {
                            "Coyoacan" -> CoyoacanStablishmentsBufete_Diaz_Miron_y_Asociados()
                        }
                    }
                    "20" -> {
                        when (spinnerCity.getSpinner().selectedItem) {
                            "Reforma" -> ReformaStablishmentsVilla_Tours()

                        }
                    }
                    "21" -> {
                        when (spinnerCity.getSpinner().selectedItem) {
                            "Merida" -> MeridaStablishmentsWSP_México()
                        }
                    }
                    "22" -> {
                        when (spinnerCity.getSpinner().selectedItem) {
                            "Ecatepec" -> EcatepecStablishmentsComsustenta()
                        }
                    }
                    "23" -> {
                        when (spinnerCity.getSpinner().selectedItem) {
                            "Los Reyes" -> LosReyesStablishmentsComercializadora_y_Servicios_Bexica()
                        }
                    }
                    "24" -> {
                        when (spinnerCity.getSpinner().selectedItem) {
                            "Mixcoac" -> MixcoacStablishments7Eleven()
                            "Naucalpan" -> NaucalpanStablishments7Eleven()
                            "Centro" -> CentroStablishments7Eleven()
                            "Coyoacán" -> CoyoacanStablishments7Eleven()
                            "TAPO" -> TapoStablishments7Eleven()
                            "Polanco" -> PolancoStablishments7Eleven()
                            "Santa Fe" -> SantaFeStablishments7Eleven()
                            "Azcapotzalco" -> AzcapotzalcoStablishments7Eleven()
                            "Tlalnepantla" -> TlalnepantlaStablishments7Eleven()
                            "Ecatepec" -> EcatepecStablishments7Eleven()
                        }
                    }

                    "25" -> {
                        when (spinnerCity.getSpinner().selectedItem) {
                            "Naucalpan" -> NaucalpanStablishmentsGrupo_Internacional_del_Descanso()
                        }
                    }
                    "26" -> {
                        when (spinnerCity.getSpinner().selectedItem) {
                            "Naucalpan" -> NaucalpanStablishmentsInmobiliaria_Vigas_Fuentes_de_Satélite()
                        }
                    }
                }
            }
        }

        spinnerStablishment.getSpinner().onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                if (position == 0) {
                    spinnerStablishment.setError("Por favor seleccione un establecimiento")
                } else {
                    spinnerStablishment.setErrorEnabled(false)
                }
            }

        }

        btnGoToWindowPhotoEvidence.setOnClickListener(this)
        btnSpinners.setOnClickListener(this)
    }

    /*override fun onNothingSelected(parent: AdapterView<*>?) {
    }*/

    /*override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        if (position == 0) {
            spinnerStat.setError("Por favor seleccione un estado")
            spinnerCity.setError("Por favor selecciona una ciudad")
            spinnerStablishment.setError("Por favor seleccione un establecimiento")
        } else {
            spinnerStat.setErrorEnabled(false)
            spinnerCity.setErrorEnabled(false)
            spinnerStablishment.setErrorEnabled(false)
        }

        when (spinnerStat.getSpinner().selectedItem) {
            "Estado de México" -> {
                EstadoDeMexicoCities()
            }
            "CDMX" -> {
                CDMXCities()
            }
            "Jalisco" -> {
                JaliscoCities()
            }
            "Chihuahua" -> {
                ChihuahuaCities()
            }
            "Nuevo Leon" -> {
                NuevoLeonCities()
            }
            "Guanajuato" -> {
                GuanajuatoCities()
            }
            "Queretaro" -> {
                QueretaroCities()
            }
            "Baja California Norte" -> {
                BajaCaliforniaNorteCities()
            }
        }

        /*when(position) {
            1 -> EstadoDeMexicoCities()
            2 -> CDMXCities()
            3 -> JaliscoCities()
            4 -> ChihuahuaCities()
            5 -> NuevoLeonCities()
            6 -> GuanajuatoCities()
            7 -> QueretaroCities()
            8 -> BajaCaliforniaNorteCities()
            3 -> YucatanCities ()

        }*/

        /*when(position) {
            1 -> NaucalpanStablishmentsSoriana()
            2 -> CuautititlanStablishmentsSoriana()
            3 -> TolucaStablishmentsSoriana()
            4 -> IztapalapaStablishmentsSoriana()
            5 -> VertizStablishmentsSoriana()
            6 -> CoyoacanStablishmentsSoriana()
            7 -> BuenavistaStablishmentsSoriana()
            8 -> MixcoacStablishmentsSoriana()
            10 -> ZaragozaStablishmentsSoriana()
            11 -> La_VillaStablishmentsSoriana()
            12 -> NarvarteStablishmentsSoriana()
            13 -> TacubayaStablishmentsSoriana()
            14 -> AzcapotzalcoStablishmentsSoriana()
            15 -> VallejoStablishmentsSoriana()
            16 -> La_vigaStablishmentsSoriana()
            17 -> EL_SaladoStablishmentsSoriana()
            18 -> Peñon_de_los_BañosStablishmentsSoriana()
            19 -> PolancoStablishmentsSoriana()
            20 -> CMDXStablishmentsSoriana()
            21 -> San_Nicolas_de_las_GarzatablishmentsSoriana()
            22 -> GuadalupeStablishmentsSoriana()
            23 -> MonterreyStablishmentsSoriana()
            24 -> PolancoStablishmentsBanorte()
            25 -> PerisurStablishmentsBanorte()
            25 -> BalbuenaStablishmentsBanorte()
            26 -> JamaicaStablishmentsBanorte()
            27 -> Peñon_de_los_BañosStablishmentsBanorte()
            28 -> ZaragozaStablishmentsBanorte()
            29 -> AgricolasStablishmentsBanorte()
            30 -> Milpa_AltaStablishmentsBanorte()
            31 -> GranjasStablishmentsBanorte()
            32 -> ChurubuscoStablishmentsBanorte()
            33 -> CentroStablishmentsBanorte()
            34 -> PatriotsmoStablishmentsBanorte()
            35 -> ConstituyentesStablishmentsBanorte()
            36 -> Santa_FeStablishmentsBanorte()
            37 -> AragonStablishmentsBanorte()
            38 -> La_VigaStablishmentsBanorte()
            39 -> CuajimalpaStablishmentsBanorte()
            40 -> BuenavistaStablishmentsBanorte()
            41 -> La_VillaStablishmentsBanorte()
            42 -> LindavistaStablishmentsBanorte()
            43 -> NarvarteStablishmentsBanorte()
            44 -> AzcapotzalcoStablishmentsBanorte()
            45 -> TacubayaStablishmentsBanorte()
            46 -> CondesaStablishmentsBanorte()
            47 -> PedregalStablishmentsBanorte()
            48 -> San_AngelStablishmentsBanorte()
            49 -> San_LazaroStablishmentsBanorte()
            50 -> NapolesStablishmentsBanorte()
            51 -> San_JeronimoStablishmentsBanorte()
            52 -> Miguel_HidalgoStablishmentsBanorte()
            53 -> CopilcoStablishmentsBanorte()
            54 -> TlalpanStablishmentsBanorte()
            55 -> TapoStablishmentsBanorte()
            56 -> Del_ValleStablishmentsBanorte()
            57 -> CoyoacanStablishmentsBanorte()
            58 -> CoapaStablishmentsBanorte()
            59 -> AtizapanStablishmentsBanorte()
            60 -> CoacalcoStablishmentsBanorte()
            61 -> CuautitlanStablishmentsBanorte()
            62 -> EcatepecStablishmentsBanorte()
            64 -> HuixquilucanStablishmentsBanorte()
            65 -> NaucalpanStablishmentsBanorte()
            66 -> TecamacStablishmentsBanorte()
            67 -> TlalnepantlaStablishmentsBanorte()
            68 -> TolucaStablishmentsBanorte()
            69 -> ZupangoStablishmentsBanorte()
            70 -> NaucalpanStablishments7Eleven()
            71 -> TlalnepantlaStablishments7Eleven()
            72 -> EcatepecStablishments7Eleven()
            73 -> MixcoacStablishments7Eleven()
            74 -> CentroStablishments7Eleven()
            75 -> CoyoacanStablishments7Eleven()
            75 -> TapoStablishments7Eleven()
            76 -> PolancoStablishments7Eleven()
            77 -> SantaFeStablishments7Eleven()
            78 -> AzcapotzalcoStablishments7Eleven()
            80 -> CentroStablishments7Aeromexico()
            81 -> CoyoacanStablishments7Aeromexico()
            82 -> SanLazaroStablishments7Aeromexico()
            83 -> CoyoacanStablishmentsBufete_Diaz_Miron_y_Asociados()
            84 -> LaVillaStablishmentsCasaCerro()
            85 -> ReformaStablishmentsClubPremier250()
            86 -> MixcoacStablishmentsComercialMexicana()
            87 -> DelValleStablishmentsComercialMexicana()
            88 -> NarvarteStablishmentsComercialMexicana()
            89 -> LosReyesStablishmentsComercializadora_y_Servicios_Bexica()
            90 -> EcatepecStablishmentsComsustenta()
            91 -> MonterreyStablishmentsCorporacion_Centralizadora_Soriana_Corpo_Oriente()
            92 -> AzcapotzalcoStablishmentsFDSI()
            93 -> TlahuacStablishmentsDistribuidora_e_Importadora_Alsea ()
            93 -> CuautitlanStablishmentsDistribuidora_e_Importadora_Alsea ()
            94 -> CentroStablishmentsFlexigrip_de_México ()
            95 -> NaucalpanStablishmentsInmobiliaria_Vigas_Fuentes_de_Satélite ()
            96 -> NaucalpanStablishmentsGrupo_Internacional_del_Descanso ()
            97 -> ReformaStablishmentsInstituto_de_Profesionalizacion_Academica  ()
            98 -> ReformaStablishmentsInternacional_Farmacéutica ()
            99 -> ValloejoStablishmentsManufacturera_Mexicana_de_Partes ()
            100 -> MeridaStablishmentsMolinos_Bunge ()
            101 -> CuajimalpaStablishmentsServicios_Bunge ()
            102 -> NaucalpanStablishmentsPolymershapes()
            103 -> TlahuacStablishmentsPolymershapes()
            104 -> La_VigaStablishmentsPolymershapes()
            105 -> ApodacaStablishmentsPolymershapes()
            106 -> ZapopanStablishmentsPolymershapes()
            107 -> ChihuahuaStablishmentsPolymershapes()
            108 -> CelayaStablishmentsPolymershapes()
            109 -> IrapuatoStablishmentsPolymershapes()
            110 -> LeonStablishmentsPolymershapes()
            111 -> QueretaroStablishmentsPolymershapes()
            112 -> HuixquilucanStablishmentsPhilips_Mexico_Comercial ()
            113 -> TlalnepantlaStablishmentsSMS_Service ()
            114 -> San_AngelStablishmentsSwiss_Re ()
            115 -> ReformaStablishmentsSí_Vale ()
            116 -> TijuanaStablishmentsSí_Vale()
            117 -> QueretaroStablishmentsSí_Vale()
            118 -> GuadalajaraStablishmentsSí_Vale()
            119 -> TlalnepantlaStablishmentsSí_Vale()
            120 -> CoyoacanStablishmentsUH_Cardiologia ()
            121 -> San_AngelStablishmentsTravel_R_US ()
            122 -> NaucalpanStablishmentsTiempo_Timex()
            123 -> ReformaStablishmentsVilla_Tours ()
            124 -> MeridaStablishmentsWSP_México ()


        }*/
    }*/

    /*-------------------------- Ciudades-------------------------------------------*/
    private fun EstadoDeMexicoCitiesSoriana() {
        //var spinnerCities1 = arrayOf("Selecciona una Ciudad", "Ecatepec", "Coacalco", "Tultitlan")
        spinnerCities = arrayOf("Selecciona una Ciudad", "Naucalpan", "Cuautitlan Izcalli", "Cuautitlan de Romero Rubio", "Toluca")
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerCities!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerCity.setAdapter(aa2)
    }

    private fun CDMXCitiesSoriana() {
        //var spinnerCities2 = arrayOf("Selecciona una Ciudad", "Los Mochis", "Culiacan", "Mazatlan")
        spinnerCities = arrayOf("Selecciona una Ciudad", "Iztapalapa", "Vértiz", "Coyoacan", "Buenavista", "Mixcoac", "Colonia Del Valle", "Zaragoza",
            "La Villa", "Narvarte", "Tacubaya", "Azcapotzalco", "Vallejo", "La Viga", "El Salado", "Peñon de los baños", "Polanco", "Coapa")
        val aa3 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerCities!!)
        aa3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerCity.setAdapter(aa3)
    }

    private fun NuevoLeonCitiesSoriana() {
        //var spinnerCities3 = arrayOf("Selecciona una Ciudad", "Ciudad Victoria", "Tampico", "Reynosa")
        spinnerCities = arrayOf("Selecciona una Ciudad", "San Nicolas de los Garza", "Guadalupe", "Monterrey")
        val aa3 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerCities!!)
        aa3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerCity.setAdapter(aa3)
    }


    private fun EstadoDeMexicoCitiesBanorte() {
        //var spinnerCities1 = arrayOf("Selecciona una Ciudad", "Ecatepec", "Coacalco", "Tultitlan")
        spinnerCities = arrayOf("Selecciona una Ciudad", "Naucalpan", "Cuautitlan Izcalli", "Cuautitlan de Romero Rubio", "Toluca", "Atizapan",
            "Coacalco", "Ecatepec", "Huixquilucan", "Tecamac", "Tlalnepantla", "Zumpango")
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerCities!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerCity.setAdapter(aa2)
    }

    private fun CDMXCitiesBanorte() {
        spinnerCities = arrayOf("Selecciona una Ciudad", "Agricola", "Aragon", "Azcapotzalco", "Balbuena", "Polanco",
            "Perisur", "Jamaica", "Peñon de los Baños","Milpa Alta", "Granjas", "Churubusco", "Patriotismo", "Constituyentes",
            "Santa Fe", "La Viga", "Cuajimalpa", "Del Valle", "Buenavista", "Centro", "Coapa", "Condesa", "Copilco", "Coyoacan", "La Villa",
            "Lindavista", "Miguel Hidalgo", "Mixcoac", "Napoles", "Narvarte", "Pedregal", "San Angel", "San Jeronimo", "San Lazaro", "Tacubaya", "Tlalpan", "Zaragoza","Tapo")
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerCities!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerCity.setAdapter(aa2)
    }

    private fun EstadoDeMexicoCitiesSevenEleven() {
        spinnerCities = arrayOf("Selecciona una Ciudad","Naucalpan","Tlalnepantla","Ecatepec")
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerCities!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerCity.setAdapter(aa2)
    }
    private fun CDMXCitiesSevenEleven() {
        spinnerCities = arrayOf("Selecciona una Ciudad", "Mixcoac","Centro","Coyoacán","TAPO","Polanco","Santa Fe","Azcapotzalco")
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerCities!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerCity.setAdapter(aa2)
    }

    private fun CDMXCitiesAeroMexico() {
        spinnerCities = arrayOf("Selecciona una Ciudad", "Centro","Coyoacan","San Lazaro")
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerCities!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerCity.setAdapter(aa2)
    }
    private fun CDMXCitiesBufete() {
        spinnerCities = arrayOf("Selecciona una Ciudad", "Coyoacan")
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerCities!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerCity.setAdapter(aa2)
    }
    private fun CDMXCitiesCasaCerro() {
        spinnerCities = arrayOf("Selecciona una Ciudad", "La Villa")
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerCities!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerCity.setAdapter(aa2)
    }

    private fun CDMXCitiesCECAP() {
        spinnerCities = arrayOf("Selecciona una Ciudad", "Peñon de los Baños")
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerCities!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerCity.setAdapter(aa2)
    }

    private fun CDMXCitiesClubPremier() {
        spinnerCities = arrayOf("Selecciona una Ciudad", "Reforma")
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerCities!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerCity.setAdapter(aa2)
    }


    private fun EstadoDeMexicoCitiesBexica() {
        spinnerCities = arrayOf("Selecciona una Ciudad", "Los Reyes")
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerCities!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerCity.setAdapter(aa2)
    }

    private fun EstadoDeMexicoCitiesComsustenta() {
        spinnerCities = arrayOf("Selecciona una Ciudad", "Ecatepec")
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerCities!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerCity.setAdapter(aa2)
    }


    private fun EstadoDeMexicoCitiesAlsea() {
        spinnerCities = arrayOf("Selecciona una Ciudad", "Cuautitlán")
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerCities!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerCity.setAdapter(aa2)
    }

    private fun CDMXCitiesAlsea(){
        spinnerCities = arrayOf("Selecciona una Ciudad", "Tláhuac")
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerCities!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerCity.setAdapter(aa2)
    }

    private fun CDMXCitiesFlexigrip() {
        spinnerCities = arrayOf("Selecciona una Ciudad", "Centro")
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerCities!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerCity.setAdapter(aa2)
    }

    private fun EstadoDeMexicoCitiesGrupoDescanso() {
        spinnerCities = arrayOf("Selecciona una Ciudad", "Naucalpan")
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerCities!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerCity.setAdapter(aa2)
    }

    private fun EstadoDeMexicoCitiesInmobiliariaVigas() {
        spinnerCities = arrayOf("Selecciona una Ciudad", "Naucalpan")
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerCities!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerCity.setAdapter(aa2)
    }

    private fun CDMXCitiesManufacturera() {
        spinnerCities = arrayOf("Selecciona una Ciudad", "Vallejo")
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerCities!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerCity.setAdapter(aa2)
    }

    private fun YucatanCitiesMolinosBunge() {
        spinnerCities = arrayOf("Selecciona una Ciudad", "Merida")
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerCities!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerCity.setAdapter(aa2)
    }

    private fun EstadoDeMexicoCitiesPhilips() {
        spinnerCities = arrayOf("Selecciona una Ciudad", "Huixquilucán")
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerCities!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerCity.setAdapter(aa2)
    }

    private fun EstadoDeMexicoCitiesPolymershapes() {
        spinnerCities = arrayOf("Selecciona una Ciudad", "Naucalpan")
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerCities!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerCity.setAdapter(aa2)
    }

    private fun CDMXCitiesPolymershapes() {
        spinnerCities = arrayOf("Selecciona una Ciudad", "Tlahuac","La Viga")
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerCities!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerCity.setAdapter(aa2)
    }

    private fun ChihuahuaCitiesPolymershapes() {
        spinnerCities = arrayOf("Selecciona una Ciudad", "Chihuahua")
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerCities!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerCity.setAdapter(aa2)
    }

    private fun GuanajuatoCitiesPolymershapes() {
        spinnerCities = arrayOf("Selecciona una Ciudad", "Celaya","Irapuato","Leon")
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerCities!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerCity.setAdapter(aa2)
    }

    private fun JaliscoCitiesPolymershapes() {
        spinnerCities = arrayOf("Selecciona una Ciudad", "Zapopan")
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerCities!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerCity.setAdapter(aa2)
    }
    private fun NuevoLeonCitiesPolymershapes() {
        spinnerCities = arrayOf("Selecciona una Ciudad", "Apodaca")
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerCities!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerCity.setAdapter(aa2)
    }

    private fun QueretaroCitiesPolymershapes() {
        spinnerCities = arrayOf("Selecciona una Ciudad", "Queretaro")
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerCities!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerCity.setAdapter(aa2)
    }

    private fun CDMXCitiesServiciosBunge() {
        spinnerCities = arrayOf("Selecciona una Ciudad", "Cuajimalpa")
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerCities!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerCity.setAdapter(aa2)
    }

    private fun EstadoDeMexicoCitiesSiVale() {
        spinnerCities = arrayOf("Selecciona una Ciudad", "Tlalnepantla")
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerCities!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerCity.setAdapter(aa2)
    }

    private fun CDMXCitiesSiVale() {
        spinnerCities = arrayOf("Selecciona una Ciudad", "Reforma")
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerCities!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerCity.setAdapter(aa2)
    }

    private fun BajaCaliforniaNorteCitiesSiVale() {
        spinnerCities = arrayOf("Selecciona una Ciudad", "Tijuana")
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerCities!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerCity.setAdapter(aa2)
    }

    private fun QueretaroCitiesSiVale() {
        spinnerCities = arrayOf("Selecciona una Ciudad", "Queretaro")
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerCities!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerCity.setAdapter(aa2)
    }

    private fun JaliscoCitiesSiVale() {
        spinnerCities = arrayOf("Selecciona una Ciudad", "Guadalajara")
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerCities!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerCity.setAdapter(aa2)
    }

    private fun CDMXCitiesFDSIService() {
        spinnerCities = arrayOf("Selecciona una Ciudad", "Azcapotzalco")
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerCities!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerCity.setAdapter(aa2)
    }

    private fun CDMXCitiesSwissRe() {
        spinnerCities = arrayOf("Selecciona una Ciudad", "San Angel")
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerCities!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerCity.setAdapter(aa2)
    }

    private fun EstadoDeMexicoCitiesTimex() {
        spinnerCities = arrayOf("Selecciona una Ciudad", "Naucalpan")
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerCities!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerCity.setAdapter(aa2)
    }

    private fun CDMXCitiesTravelRUS() {
        spinnerCities = arrayOf("Selecciona una Ciudad", "San Angel")
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerCities!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerCity.setAdapter(aa2)
    }

    private fun CDMXCitiesUHCardiologia() {
        spinnerCities = arrayOf("Selecciona una Ciudad", "Coyoacan")
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerCities!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerCity.setAdapter(aa2)
    }

    private fun CDMXCitiesVillaTours() {
        spinnerCities = arrayOf("Selecciona una Ciudad", "Reforma")
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerCities!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerCity.setAdapter(aa2)
    }

    private fun YucatanCitiesWPS() {
        spinnerCities = arrayOf("Selecciona una Ciudad", "Merida")
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerCities!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerCity.setAdapter(aa2)
    }



    /*-------------------------------- Establecimientos -----------------------------*/
    private fun NaucalpanStablishmentsSoriana() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val SorianaSM = arguments?.getString("SorianaSM")
        val SorianaEchegaray = arguments?.getString("SorianaEchegaray")
        val SorianaLomasVerdes = arguments?.getString("SorianaLomasVerdes")
        spinnerStablishments = arrayOf(SE!!, SorianaSM!!, SorianaEchegaray!!, SorianaLomasVerdes!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun CuautititlanRomeroRubioStablishmentsSoriana(){
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val SorianaCuautitlan = arguments?.getString("SorianaCuautitlan")
        spinnerStablishments = arrayOf(SE!!, SorianaCuautitlan!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun CuautititlanIzcalliStablishmentsSoriana(){
        val SE = arguments?.getString("SE")
        val SorianaPN = arguments?.getString("SorianaPN")
        spinnerStablishments = arrayOf(SE!!, SorianaPN!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun TolucaStablishmentsSoriana() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val SorianaST = arguments?.getString("SorianaST")
        spinnerStablishments = arrayOf(SE!!, SorianaST!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }
    private fun IztapalapaStablishmentsSoriana() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val SorianaIztapalapa = arguments?.getString("SorianaIztapalapa")
        spinnerStablishments = arrayOf(SE!!, SorianaIztapalapa!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun VertizStablishmentsSoriana() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val SorianaPD = arguments?.getString("SorianaPD")
        spinnerStablishments = arrayOf(SE!!, SorianaPD!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun CoyoacanStablishmentsSoriana() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val SorianaPC = arguments?.getString("SorianaPC")
        spinnerStablishments = arrayOf(SE!!, SorianaPC!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun BuenavistaStablishmentsSoriana() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val SorianaBuenavista = arguments?.getString("SorianaBuenavista")
        spinnerStablishments = arrayOf(SE!!, SorianaBuenavista!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun MixcoacStablishmentsSoriana() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val SorianaMixcoac = arguments?.getString("SorianaMixcoac")
        val ComercialMexicanaMix = arguments?.getString("ComercialMexicanaMix")
        spinnerStablishments = arrayOf(SE!!, SorianaMixcoac!!,ComercialMexicanaMix!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun ZaragozaStablishmentsSoriana() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val SorianaZaragoza = arguments?.getString("SorianaZaragoza")
        spinnerStablishments = arrayOf(SE!!, SorianaZaragoza!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun Colonia_Del_ValleStablishmentsSoriana() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val CMPilares = arguments?.getString("CMPilares")
        spinnerStablishments = arrayOf(SE!!, CMPilares!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun La_VillaStablishmentsSoriana() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val SorianaLV = arguments?.getString("SorianaLV")
        spinnerStablishments = arrayOf(SE!!, SorianaLV!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun NarvarteStablishmentsSoriana() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val SorianaEugenia = arguments?.getString("SorianaEugenia")
        val ComercialMexicanaNarv = arguments?.getString("ComercialMexicanaNarv")
        spinnerStablishments = arrayOf(SE!!, SorianaEugenia!!,ComercialMexicanaNarv!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun TacubayaStablishmentsSoriana() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val SorianaTacubaya = arguments?.getString("SorianaTacubaya")
        spinnerStablishments = arrayOf(SE!!, SorianaTacubaya!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun AzcapotzalcoStablishmentsSoriana() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val SorianaPA = arguments?.getString("SorianaPA")
        val SorianaER = arguments?.getString("SorianaER")
        spinnerStablishments = arrayOf(SE!!, SorianaPA!!, SorianaER!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun VallejoStablishmentsSoriana() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val SorianaCuitlahuac = arguments?.getString("SorianaCuitlahuac")
        val SorianaVV = arguments?.getString("SorianaVV")
        spinnerStablishments = arrayOf(SE!!, SorianaCuitlahuac!!, SorianaVV!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun La_vigaStablishmentsSoriana() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val SorianaLaViga = arguments?.getString("SorianaLaViga")
        spinnerStablishments = arrayOf(SE!!, SorianaLaViga!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun EL_SaladoStablishmentsSoriana() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val SorianaES = arguments?.getString("SorianaES")
        spinnerStablishments = arrayOf(SE!!, SorianaES!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun Peñon_de_los_BañosStablishmentsSoriana (){
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val SorianaConsulado = arguments?.getString("SorianaConsulado")
        spinnerStablishments = arrayOf(SE!!, SorianaConsulado!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun PolancoStablishmentsSoriana (){
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val SorianaMP = arguments?.getString("SorianaMP")
        spinnerStablishments = arrayOf(SE!!, SorianaMP!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun CoapaStablishmentsSoriana(){
        val SE = arguments?.getString("SE")
        val SorianaTC = arguments?.getString("SorianaTC")
        spinnerStablishments = arrayOf(SE!!, SorianaTC!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun San_Nicolas_de_las_GarzatablishmentsSoriana (){
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val SorianaLF = arguments?.getString("SorianaLF")
        val SorianaLA = arguments?.getString("SorianaLA")
        val SorianaMB = arguments?.getString("SorianaMB")
        spinnerStablishments = arrayOf(SE!!, SorianaLF!!, SorianaLA!!, SorianaMB!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun GuadalupeStablishmentsSoriana (){
        val SE = arguments?.getString("SE")
        val SorianaSantaM = arguments?.getString("SorianaSantaM")
        spinnerStablishments = arrayOf(SE!!, SorianaSantaM!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun MonterreyStablishmentsSoriana (){
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val SorianaVH = arguments?.getString("SorianaVH")
        val SorianaFundidora = arguments?.getString("SorianaFundidora")
        spinnerStablishments = arrayOf(SE!!, SorianaVH!!, SorianaFundidora!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }
    private fun TapoStablishmentsBanorte() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val BanorteVenustianoC = arguments?.getString("BanorteVenustianoC")
        val BanorteMerced = arguments?.getString("BanorteMerced")
        val BanorteSL = arguments?.getString("BanorteSL")
        spinnerStablishments = arrayOf(SE!!, BanorteVenustianoC!!,BanorteMerced!!, BanorteSL!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun PolancoStablishmentsBanorte() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val BanorteMasaryk = arguments?.getString("BanorteMasaryk")
        val BanortePM = arguments?.getString("BanortePM")
        val BanorteLF = arguments?.getString("BanorteLF")
        val BanorteJV= arguments?.getString("BanorteJV")
        val BanorteDROP = arguments?.getString("BanorteDROP")
        val BanorteOM = arguments?.getString("BanorteOM")
        spinnerStablishments = arrayOf(SE!!, BanorteMasaryk!!, BanortePM!!, BanorteLF!!, BanorteJV!!, BanorteDROP!!, BanorteOM!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun PerisurStablishmentsBanorte() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val BanorteITG = arguments?.getString("BanorteITG")
        val BanorteFBS = arguments?.getString("BanorteFBS")
        val BanortePerisur = arguments?.getString("BanortePerisur")
        spinnerStablishments = arrayOf(SE!!, BanorteITG!!,BanorteFBS!!, BanortePerisur!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun BalbuenaStablishmentsBanorte() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val BanorteBSB = arguments?.getString("BanorteBSB")
        val BanorteCH = arguments?.getString("BanorteCH")
        spinnerStablishments = arrayOf(SE!!, BanorteBSB!!, BanorteCH!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun JamaicaStablishmentsBanorte() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val BanorteTroncoso = arguments?.getString("BanorteTroncoso")
        val BanorteJM = arguments?.getString("BanorteJM")
        spinnerStablishments = arrayOf(SE!!, BanorteTroncoso!!, BanorteJM!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun Peñon_de_los_BañosStablishmentsBanorte() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val BanorteALLN = arguments?.getString("BanorteALLN")
        spinnerStablishments = arrayOf(SE!!, BanorteALLN!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun ZaragozaStablishmentsBanorte () {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val BanorteBAM = arguments?.getString("BanorteBAM")
        val BanorteZA = arguments?.getString("BanorteZA")
        spinnerStablishments = arrayOf(SE!!, BanorteBAM!!,BanorteZA!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun AgricolasStablishmentsBanorte() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val BanorteAO = arguments?.getString("BanorteAO")
        spinnerStablishments = arrayOf(SE!!, BanorteAO!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun Milpa_AltaStablishmentsBanorte() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val BanorteMA = arguments?.getString("BanorteMA")
        spinnerStablishments = arrayOf(SE!!, BanorteMA!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun GranjasStablishmentsBanorte() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val BanortePD = arguments?.getString("BanortePD")
        spinnerStablishments = arrayOf(SE!!, BanortePD!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun ChurubuscoStablismentsBanorte() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val BanortePC = arguments?.getString("BanortePC")
        spinnerStablishments = arrayOf(SE!!, BanortePC!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun PatriotismoStablishmentsBanorte() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val BanorteSPP = arguments?.getString("BanorteSPP")
        val BanortePH = arguments?.getString("BanortePH")
        spinnerStablishments = arrayOf(SE!!, BanorteSPP!!, BanortePH!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun ConstituyentesStablishmentsBanorte() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val BanorteConstituyentes = arguments?.getString("BanorteConstituyentes")
        spinnerStablishments = arrayOf(SE!!, BanorteConstituyentes!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun Santa_FeStablishmentsBanorte() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val BanorteCCSF = arguments?.getString("BanorteCCSF")
        val BanorteRL = arguments?.getString("BanorteRL")

        spinnerStablishments = arrayOf(SE!!, BanorteCCSF!!, BanorteRL!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    /*private fun () {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val BanortePM = arguments?.getString("Banorte Reforma Lomas")
        spinnerStablishments = arrayOf(SE!!, BanortePM!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }*/

    private fun AragonStablishmentsBanorte() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val BanorteMAI = arguments?.getString("BanorteMAI")
        val BanorteVA = arguments?.getString("BanorteVA")
        spinnerStablishments = arrayOf(SE!!, BanorteMAI!!,BanorteVA!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun La_VigaStablishmentsBanorte() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val BanorteLVA= arguments?.getString("BanorteLVA")
        spinnerStablishments = arrayOf(SE!!, BanorteLVA!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun CuajimalpaStablishmentsBanorte() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val BanorteCC= arguments?.getString("BanorteCC")
        spinnerStablishments = arrayOf(SE!!, BanorteCC!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }


    private fun BuenavistaStablishmentsBanorte() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val BanorteFB= arguments?.getString("BanorteFB")
        val BanorteNonoalco= arguments?.getString("BanorteNonoalco")
        spinnerStablishments = arrayOf(SE!!, BanorteFB!!, BanorteNonoalco!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun La_VillaStablishmentsBanorte() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val BanorteLVM= arguments?.getString("BanorteLVM")
        spinnerStablishments = arrayOf(SE!!, BanorteLVM!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun  LindavistaStablishmentsBanorte() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val BanorteLM= arguments?.getString("BanorteLM")
        val BanorteTL= arguments?.getString("BanorteTL")
        val BanorteDRL= arguments?.getString("BanorteDRL")
        val BanorteMontevideo= arguments?.getString("BanorteMontevideo")
        spinnerStablishments = arrayOf(SE!!, BanorteLM!!, BanorteTL!!, BanorteDRL!!, BanorteMontevideo!! )
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun CentroStablishmentsBanorte() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val BanorteSC= arguments?.getString("BanorteSC")
        val BanorteADLI= arguments?.getString("BanorteADLI")
        val BanorteCDM= arguments?.getString("BanorteCDM")
        val BanorteLC= arguments?.getString("BanorteLC")
        spinnerStablishments = arrayOf(SE!!, BanorteSC!!, BanorteADLI!!, BanorteCDM!!, BanorteLC!! )
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun NarvarteStablishmentsBanorte() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val BanorteDRSMM= arguments?.getString("BanorteDRSMM")
        val BanorteEugenia=arguments?.getString("BanorteEugenia")
        val BanorteUM= arguments?.getString("BanorteUM")
        spinnerStablishments = arrayOf(SE!!, BanorteDRSMM!!, BanorteEugenia!!, BanorteUM!! )
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun AzcapotzalcoStablishmentsBanorte() {
        val SE = arguments?.getString("SE")
        val BanorteAzcapotzalco= arguments?.getString("BanorteAzcapotzalco")
        val BanorteBEG=arguments?.getString("BanorteBEG")
        spinnerStablishments = arrayOf(SE!!, BanorteAzcapotzalco!!, BanorteBEG!! )
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun TacubayaStablishmentsBanorte() {
        val SE = arguments?.getString("SE")
        val BanorteTacubaya =arguments?.getString("BanorteTacubaya")
        spinnerStablishments = arrayOf(SE!!, BanorteTacubaya!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun CondesaStablishmentsBanorte() {
        val SE = arguments?.getString("SE")
        val BanorteCondesa =arguments?.getString("BanorteCondesa")
        val BanorteC =arguments?.getString("BanorteC")
        spinnerStablishments = arrayOf(SE!!, BanorteCondesa!!, BanorteC!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun PedregalStablishmentsBanorte() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val BanorteST =arguments?.getString("BanorteST")
        val BanortePedregal =arguments?.getString("BanortePedregal")
        spinnerStablishments = arrayOf(SE!!, BanorteST!!, BanortePedregal!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun San_JeronimoStablishmentsBanorte() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val BanorteSJ =arguments?.getString("BanorteSJ")
        spinnerStablishments = arrayOf(SE!!, BanorteSJ!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun San_AngelStablishmentsBanorte() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val BanorteADP =arguments?.getString("BanorteADP")
        val BanorteAT =arguments?.getString("BanorteAT")
        spinnerStablishments = arrayOf(SE!!, BanorteADP!!, BanorteAT!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun NapolesStablishmentsBanorte() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val BanorteWTC =arguments?.getString("BanorteWTC")
        spinnerStablishments = arrayOf(SE!!, BanorteWTC!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun Miguel_HidalgoStablishmentsBanorte () {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val BanorteLegaria =arguments?.getString("BanorteLegaria")
        val BanorteIrrigacion =arguments?.getString("BanorteIrrigacion")
        spinnerStablishments = arrayOf(SE!!, BanorteLegaria!!, BanorteIrrigacion!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun CopilcoStablishmentsBanorte () {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val BanorteCopilco =arguments?.getString("BanorteCopilco")
        spinnerStablishments = arrayOf(SE!!, BanorteCopilco!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun Del_ValleStablishmentsBanorte () {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val BanorteMS =arguments?.getString("BanorteMS")
        val BanorteManacar=arguments?.getString("BanorteManacar")
        spinnerStablishments = arrayOf(SE!!, BanorteMS!!, BanorteManacar!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun CoyoacanStablishmentsBanorte () {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val BanorteDDN =arguments?.getString("BanorteDDN")
        val BanorteBMS =arguments?.getString("BanorteBMS")
        val BanortePacifico =arguments?.getString("BanortePacifico")
        val BanorteCPIC =arguments?.getString("BanorteCPIC")
        spinnerStablishments= arrayOf(SE!!, BanorteDDN!!, BanorteBMS!!, BanortePacifico!!, BanorteCPIC!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }
    private fun  TlalpanStablishmentsBanorte () {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val BanorteTA =arguments?.getString("BanorteTA")
        val BanorteTaxqueña =arguments?.getString("BanorteTaxqueña")
        spinnerStablishments= arrayOf(SE!!, BanorteTA!!,BanorteTaxqueña!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)

    }

    private fun San_LazaroStablishmentsBanorte () {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val BanorteCDD =arguments?.getString("BanorteCDD")
        val BanorteCT1T2 =arguments?.getString("BanorteCT1T2")
        spinnerStablishments= arrayOf(SE!!, BanorteCDD!!,BanorteCT1T2!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun CoapaStablishmentsBanorte() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val BanorteVC =arguments?.getString("BanorteVC")
        val BanorteCoaplaza =arguments?.getString("BanorteCoaplaza")
        spinnerStablishments= arrayOf(SE!!, BanorteVC!!,BanorteCoaplaza!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun MixcoacStablishmentsBanorte() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val BanorteBDM =arguments?.getString("BanorteBDM")
        spinnerStablishments= arrayOf(SE!!, BanorteBDM!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun AtizapanStablishmentsBanorte() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val BanorteVHA =arguments?.getString("BanorteVHA")
        val BanorteAZE =arguments?.getString("BanorteAZE")
        val BanorteAA =arguments?.getString("BanorteAA")
        spinnerStablishments= arrayOf(SE!!, BanorteVHA!!, BanorteAZE!!, BanorteAA!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun CoacalcoStablishmentsBanorte() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val BanorteCPC =arguments?.getString("BanorteCPC")
        val BanortePJ =arguments?.getString("BanortePJ")
        spinnerStablishments= arrayOf(SE!!, BanorteCPC!!,BanortePJ!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun CuautitlanIzcalliStablishmentsBanorte() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val BanorteCI =arguments?.getString("BanorteCI")
        spinnerStablishments= arrayOf(SE!!,BanorteCI!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun CuautitlanRomeroRubioStablishmentsBanorte() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val BanorteCRR =arguments?.getString("BanorteCRR")
        spinnerStablishments= arrayOf(SE!!,BanorteCRR!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun EcatepecStablishmentsBanorte() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val BanorteEcatepec =arguments?.getString("BanorteEcatepec")
        val BanorteCAE =arguments?.getString("BanorteCAE")
        val BanortePSA =arguments?.getString("BanortePSA")
        spinnerStablishments= arrayOf(SE!!, BanorteEcatepec!!,BanorteCAE!!, BanortePSA!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun HuixquilucanStablishmentsBanorte() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val BanorteHuixquilucan =arguments?.getString("BanorteHuixquilucan")
        spinnerStablishments= arrayOf(SE!!,BanorteHuixquilucan!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun NaucalpanStablishmentsBanorte() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val BanortePIN =arguments?.getString("BanortePIN")
        val BanorteCM =arguments?.getString("BanorteCM")
        val BanorteLCN =arguments?.getString("BanorteLCN")
        val BanorteAB =arguments?.getString("BanorteAB")
        val BanorteCCCS =arguments?.getString("BanorteCCCS")
        val BanorteFS =arguments?.getString("BanorteFS")
        spinnerStablishments= arrayOf(SE!!,BanortePIN!!, BanorteCM!!, BanorteLCN!!, BanorteAB!!,BanorteCCCS!! ,BanorteFS!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun TecamacStablishmentsBanorte() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val BanorteHOA =arguments?.getString("BanorteHOA")
        val BanorteTecamac =arguments?.getString("BanorteTecamac")
        spinnerStablishments= arrayOf(SE!!,BanorteHOA!!, BanorteTecamac!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun TlalnepantlaStablishmentsBanorte() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val BanorteVD =arguments?.getString("BanorteVD")
        val BanorteLAT =arguments?.getString("BanorteLAT")
        val BanorteBGB =arguments?.getString("BanorteBGB")
        spinnerStablishments= arrayOf(SE!!,BanorteVD!!, BanorteLAT!!, BanorteBGB!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun TolucaStablishmentsBanorte(){
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val BanorteIFT =arguments?.getString("BanorteIFT")
        val BanorteTC =arguments?.getString("BanorteTC")
        val BanorteMT =arguments?.getString("BanorteMT")
        val BanortePT =arguments?.getString("BanortePT")
        val BanorteRD =arguments?.getString("BanorteRD")
        val BanorteUT =arguments?.getString("BanorteUT")
        val BanorteCIF =arguments?.getString("BanorteCIF")
        val BanorteVCT =arguments?.getString("BanorteVCT")
        val BanortePST =arguments?.getString("BanortePST")
        val BanorteHET =arguments?.getString("BanorteHET")
        spinnerStablishments= arrayOf(SE!!,BanorteIFT!!, BanorteTC!!, BanorteMT!!, BanortePT!!, BanorteRD!!, BanorteUT!!, BanorteCIF!!, BanorteVCT!!, BanortePST!!, BanorteHET!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun ZumpangoStablishmentsBanorte() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val BanorteZumpango =arguments?.getString("BanorteZumpango")
        spinnerStablishments= arrayOf(SE!!,BanorteZumpango!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun NaucalpanStablishments7Eleven() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val ElevenNYSM =arguments?.getString("ElevenNYSM")
        spinnerStablishments= arrayOf(SE!!,ElevenNYSM!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun TlalnepantlaStablishments7Eleven() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val ElevenGB =arguments?.getString("ElevenGB")
        spinnerStablishments= arrayOf(SE!!,ElevenGB!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun EcatepecStablishments7Eleven() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val ElevenSC =arguments?.getString("ElevenSC")
        spinnerStablishments= arrayOf(SE!!,ElevenSC!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun CoyoacanStablishments7Eleven() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val ElevenC =arguments?.getString("ElevenC")
        spinnerStablishments= arrayOf(SE!!,ElevenC!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun MixcoacStablishments7Eleven() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val ElevenCP =arguments?.getString("ElevenCP")
        spinnerStablishments= arrayOf(SE!!,ElevenCP!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun CentroStablishments7Eleven() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val ElevenGenova =arguments?.getString("ElevenGenova")
        spinnerStablishments= arrayOf(SE!!,ElevenGenova!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun PolancoStablishments7Eleven() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val ElevenMoliere =arguments?.getString("ElevenMoliere")
        spinnerStablishments= arrayOf(SE!!,ElevenMoliere!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun SantaFeStablishments7Eleven() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val ElevenSantaFe =arguments?.getString("ElevenSantaFe")
        spinnerStablishments= arrayOf(SE!!,ElevenSantaFe!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }
    private fun TapoStablishments7Eleven() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val ElevenCS =arguments?.getString("ElevenCS")
        spinnerStablishments= arrayOf(SE!!,ElevenCS!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }
    private fun AzcapotzalcoStablishments7Eleven() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val ElevenFerreria =arguments?.getString("ElevenFerreria")
        spinnerStablishments= arrayOf(SE!!,ElevenFerreria!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun CentroStablishmentsAeromexico() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val AeroMCentro =arguments?.getString("AeroMCentro")
        val AeroMC =arguments?.getString("AeroMC")
        spinnerStablishments= arrayOf(SE!!,AeroMCentro!!,AeroMC!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun CoyoacanStablishmentsAeromexico() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val AeroMCyoacan =arguments?.getString("AeroMCyoacan")
        val AeroMCoy =arguments?.getString("AeroMCoy")
        spinnerStablishments= arrayOf(SE!!,AeroMCyoacan!!,AeroMCoy!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun SanLazaroStablishmentsAeromexico() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val AeroMSL =arguments?.getString("AeroMSL")
        spinnerStablishments= arrayOf(SE!!,AeroMSL!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun CoyoacanStablishmentsBufete_Diaz_Miron_y_Asociados () {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val BufeteCoyoacan =arguments?.getString("BufeteCoyoacan")
        spinnerStablishments= arrayOf(SE!!,BufeteCoyoacan!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun LaVillaStablishmentsCasaCerro () {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val CasaCerroLV =arguments?.getString("CasaCerroLV")
        spinnerStablishments= arrayOf(SE!!,CasaCerroLV!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }


    private fun PeñondelosbañosStablishmentsCECAP () {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val CECAPPDL =arguments?.getString("CECAPPDL")
        spinnerStablishments= arrayOf(SE!!,CECAPPDL!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun ReformaStablishmentsClubPremier250 () {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val clubPReforma =arguments?.getString("clubPReforma")
        spinnerStablishments= arrayOf(SE!!,clubPReforma!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }
    private fun LosReyesStablishmentsComercializadora_y_Servicios_Bexica () {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val ComerciaizadoraBex =arguments?.getString("ComerciaizadoraBex")
        spinnerStablishments= arrayOf(SE!!,ComerciaizadoraBex!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }
    private fun EcatepecStablishmentsComsustenta() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val ComustentaEcatepec =arguments?.getString("ComustentaEcatepec")
        spinnerStablishments= arrayOf(SE!!,ComustentaEcatepec!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun TlahuacStablishmentsDistribuidora_e_Importadora_Alsea() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val DistribuidoraALSEAT =arguments?.getString("DistribuidoraALSEAT")
        spinnerStablishments= arrayOf(SE!!,DistribuidoraALSEAT!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }
    private fun CuautitlanStablishmentsDistribuidora_e_Importadora_Alsea() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val DistribuidoraALlseaC =arguments?.getString("DistribuidoraALlseaC")
        spinnerStablishments= arrayOf(SE!!,DistribuidoraALlseaC!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun CentroStablishmentsFlexigrip_de_México() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val FlexigripCentro =arguments?.getString("FlexigripCentro")
        spinnerStablishments= arrayOf(SE!!,FlexigripCentro!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun VallejoStablishmentsManufacturera_Mexicana_de_Partes() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val ManufactureraVallejo =arguments?.getString("ManufactureraVallejo")
        spinnerStablishments= arrayOf(SE!!,ManufactureraVallejo!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }
    private fun NaucalpanStablishmentsGrupo_Internacional_del_Descanso() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val GrupoDescanso =arguments?.getString("GrupoDescanso")
        spinnerStablishments= arrayOf(SE!!,GrupoDescanso!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }
    private fun NaucalpanStablishmentsInmobiliaria_Vigas_Fuentes_de_Satélite() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val InmoviliariaF =arguments?.getString("InmoviliariaF")
        spinnerStablishments= arrayOf(SE!!,InmoviliariaF!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun MeridaStablishmentsMolinos_Bunge() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val MolinosBMerida =arguments?.getString("MolinosBMerida")
        spinnerStablishments= arrayOf(SE!!,MolinosBMerida!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun CuajimalpaStablishmentsServicios_Bunge() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val BungeCuajimalpa =arguments?.getString("BungeCuajimalpa")
        spinnerStablishments= arrayOf(SE!!,BungeCuajimalpa!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }


    private fun TlahuacStablishmentsPolymershapes() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val PolymershapesTlahuac =arguments?.getString("PolymershapesTlahuac")
        spinnerStablishments= arrayOf(SE!!,PolymershapesTlahuac!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun La_VigaStablishmentsPolymershapes() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val PolymershapesLV =arguments?.getString("PolymershapesLV")
        spinnerStablishments= arrayOf(SE!!,PolymershapesLV!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun ApodacaStablishmentsPolymershapes() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val PolymershapesAPODACA =arguments?.getString("PolymershapesAPODACA")
        spinnerStablishments= arrayOf(SE!!,PolymershapesAPODACA!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun ZapopanStablishmentsPolymershapes() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val PolymershapesZAPOPAN =arguments?.getString("PolymershapesZAPOPAN")
        spinnerStablishments= arrayOf(SE!!,PolymershapesZAPOPAN!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }
    private fun NaucalpanStablishmentsPolymershapes() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val PolymershapesN =arguments?.getString("PolymershapesN")
        spinnerStablishments= arrayOf(SE!!,PolymershapesN!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }
    private fun ChihuahuaStablishmentsPolymershapes() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val PolymershapesCHIHUAHUA =arguments?.getString("PolymershapesCHIHUAHUA")
        spinnerStablishments= arrayOf(SE!!,PolymershapesCHIHUAHUA!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun CelayaStablishmentsPolymershapes() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val PolymershapesCelaya =arguments?.getString("PolymershapesCelaya")
        spinnerStablishments= arrayOf(SE!!,PolymershapesCelaya!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun IrapuatoStablishmentsPolymershapes() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val PolymershapesIrapuato =arguments?.getString("PolymershapesIrapuato")
        spinnerStablishments= arrayOf(SE!!,PolymershapesIrapuato!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun LeonStablishmentsPolymershapes() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val PolymershapesLeonCDC =arguments?.getString("PolymershapesLeonCDC")
        val PolymershapesLeonIB =arguments?.getString("PolymershapesLeonIB")
        spinnerStablishments= arrayOf(SE!!, PolymershapesLeonCDC!!, PolymershapesLeonIB!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun QueretaroStablishmentsPolymershapes() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val PolymershapesQueretaro =arguments?.getString("PolymershapesQueretaro")
        spinnerStablishments= arrayOf(SE!!,PolymershapesQueretaro!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }
    private fun AzcapotzalcoStablishmentsFDSI_Service() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val fdsiAsesores =arguments?.getString("fdsiAsesores")
        val fdsiAdministrativos = arguments?.getString("fdsiAdministrativos")
        val fdsiSupervisores = arguments?.getString("fdsiSupervisores")
        spinnerStablishments= arrayOf(SE!!, fdsiAsesores!!, fdsiAdministrativos!!, fdsiSupervisores!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }



    private fun San_AngelStablishmentsSwiss_Re() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val SWISSSANANGEL =arguments?.getString("SWISSSANANGEL")
        spinnerStablishments= arrayOf(SE!!,SWISSSANANGEL!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }
    private fun HuixquilucanStablishmentsPhilips_Mexico_Comercial() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val PhillipsM =arguments?.getString("PhillipsM")
        spinnerStablishments= arrayOf(SE!!,PhillipsM!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }


    private fun ReformaStablishmentsSí_Vale() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val SIVALEREFORMA =arguments?.getString("SIVALEREFORMA")
        spinnerStablishments= arrayOf(SE!!,SIVALEREFORMA!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun TijuanaStablishmentsSí_Vale() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val SIVALETIJUANA =arguments?.getString("SIVALETIJUANA")
        spinnerStablishments= arrayOf(SE!!,SIVALETIJUANA!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun QueretaroStablishmentsSí_Vale() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val SIVALEQUERETARO =arguments?.getString("SIVALEQUERETARO")
        spinnerStablishments= arrayOf(SE!!,SIVALEQUERETARO!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun GuadalajaraStablishmentsSí_Vale() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val SIVALEGUADALAJARA =arguments?.getString("SIVALEGUADALAJARA")
        spinnerStablishments= arrayOf(SE!!,SIVALEGUADALAJARA!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun TlalnepantlaStablishmentsSí_Vale() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val SIVALETLALNE =arguments?.getString("SIVALETLALNE")
        spinnerStablishments= arrayOf(SE!!,SIVALETLALNE!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun CoyoacanStablishmentsUH_Cardiologia() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val UHCOYOACAN =arguments?.getString("UHCOYOACAN")
        spinnerStablishments= arrayOf(SE!!,UHCOYOACAN!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun San_AngelStablishmentsTravel_R_US() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val TRAVELSANANGEL =arguments?.getString("TRAVELSANANGEL")
        spinnerStablishments= arrayOf(SE!!,TRAVELSANANGEL!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }
    private fun NaucalpanStablishmentsTiempoTimex() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val Timex =arguments?.getString("Timex")
        spinnerStablishments= arrayOf(SE!!, Timex!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }


    private fun ReformaStablishmentsVilla_Tours() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val TOURSREFORMA =arguments?.getString("TOURSREFORMA")
        spinnerStablishments= arrayOf(SE!!,TOURSREFORMA!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }

    private fun MeridaStablishmentsWSP_México() {
        //var spinnerCities1 = arrayOf("Selecciona una Establecimiento", "Soriana Via Morelos", "Soriana Lopez Portillo", "Soriana Gran Patio Ecatepec")
        val SE = arguments?.getString("SE")
        val WPSmERIDA =arguments?.getString("WPSmERIDA")
        spinnerStablishments= arrayOf(SE!!,WPSmERIDA!!)
        val aa2 = ArrayAdapter(activity?.applicationContext!!, android.R.layout.simple_spinner_item, spinnerStablishments!!)
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerStablishment.setAdapter(aa2)
    }


    override fun onClick(v: View?) {
        when(v) {
            btnGoToWindowPhotoEvidence -> saveInSqlite()
            btnSpinners -> goToSpiners()
        }
    }

    fun goToSpiners() {
        findNavController().navigate(R.id.stablishmentFragment)
    }

    private fun saveInSqlite() {
        if (spinnerStat.getSpinner().isEmpty() || spinnerCity.getSpinner().isEmpty() || spinnerStablishment.getSpinner().isEmpty()) {
            Toast.makeText(activity?.baseContext!!, "Por favor seleccione una Localidad, Ciudad y Establecimiento", Toast.LENGTH_SHORT).show()
        } else {
            val sp1 = spinnerStat.getSpinner().selectedItem.toString()
            val sp2 = spinnerCity.getSpinner().selectedItem.toString()
            val sp3 = spinnerStablishment.getSpinner().selectedItem.toString()
            val args = bundleOf("sp1" to sp1, "sp2" to sp2, "sp3" to sp3)
            val position = arguments?.getString("position")
            when(position) {
                "0" -> {
                    val bundle1 = Bundle()
                    bundle1.putBundle("bundle1", args)
                    findNavController().navigate(R.id.valuationFormatFragment, bundle1)
                }
                "1" -> {
                    val bundle1 = Bundle()
                    bundle1.putBundle("bundle1", args)
                    findNavController().navigate(R.id.valuationFormatRestoClientesFragment, bundle1)
                }
                "2" -> {
                    val bundle1 = Bundle()
                    bundle1.putBundle("bundle1", args)
                    findNavController().navigate(R.id.valuationFormatRestoClientesFragment, bundle1)
                }
                "3" -> {
                    val bundle1 = Bundle()
                    bundle1.putBundle("bundle1", args)
                    findNavController().navigate(R.id.valuationFormatRestoClientesFragment, bundle1)
                }
                "4" -> {
                    val bundle1 = Bundle()
                    bundle1.putBundle("bundle1", args)
                    findNavController().navigate(R.id.valuationFormatRestoClientesFragment, bundle1)
                }
                "5" -> {
                    val bundle1 = Bundle()
                    bundle1.putBundle("bundle1", args)
                    findNavController().navigate(R.id.valuationFormatRestoClientesFragment, bundle1)
                }
                "6" -> {
                    val bundle1 = Bundle()
                    bundle1.putBundle("bundle1", args)
                    findNavController().navigate(R.id.valuationFormatRestoClientesFragment, bundle1)
                }
                "7" -> {
                    val bundle1 = Bundle()
                    bundle1.putBundle("bundle1", args)
                    findNavController().navigate(R.id.valuationFormatRestoClientesFragment, bundle1)
                }
                "8" -> {
                    val bundle1 = Bundle()
                    bundle1.putBundle("bundle1", args)
                    findNavController().navigate(R.id.valuationFormatRestoClientesFragment, bundle1)
                }
                "9" -> {
                    val bundle1 = Bundle()
                    bundle1.putBundle("bundle1", args)
                    findNavController().navigate(R.id.valuationFormatBanorteFragment, bundle1)
                }
                "10" -> {
                    val bundle1 = Bundle()
                    bundle1.putBundle("bundle1", args)
                    findNavController().navigate(R.id.valuationFormatRestoClientesFragment, bundle1)
                }
                "11" -> {
                    val bundle1 = Bundle()
                    bundle1.putBundle("bundle1", args)
                    findNavController().navigate(R.id.valuationFormatRestoClientesFragment, bundle1)
                }

                "12" -> {
                    val bundle1 = Bundle()
                    bundle1.putBundle("bundle1", args)
                    findNavController().navigate(R.id.valuationFormatRestoClientesFragment, bundle1)
                }
                "13" -> {
                    val bundle1 = Bundle()
                    bundle1.putBundle("bundle1", args)
                    findNavController().navigate(R.id.valuationFormatRestoClientesFragment, bundle1)
                }
                "14" -> {
                    val bundle1 = Bundle()
                    bundle1.putBundle("bundle1", args)
                    findNavController().navigate(R.id.valuationFormatRestoClientesFragment, bundle1)
                }
                "15" -> {
                    val bundle1 = Bundle()
                    bundle1.putBundle("bundle1", args)
                    findNavController().navigate(R.id.valuationFormatRestoClientesFragment, bundle1)
                }
                "16" -> {
                    val bundle1 = Bundle()
                    bundle1.putBundle("bundle1", args)
                    findNavController().navigate(R.id.valuationFormatRestoClientesFragment, bundle1)
                }
                "17" -> {
                    val bundle1 = Bundle()
                    bundle1.putBundle("bundle1", args)
                    findNavController().navigate(R.id.valuationFormatRestoClientesFragment, bundle1)
                }
                "18" -> {
                    val bundle1 = Bundle()
                    bundle1.putBundle("bundle1", args)
                    findNavController().navigate(R.id.valuationFormatRestoClientesFragment, bundle1)
                }
                "19" -> {
                    val bundle1 = Bundle()
                    bundle1.putBundle("bundle1", args)
                    findNavController().navigate(R.id.valuationFormatRestoClientesFragment, bundle1)
                }
                "20" -> {
                    val bundle1 = Bundle()
                    bundle1.putBundle("bundle1", args)
                    findNavController().navigate(R.id.valuationFormatRestoClientesFragment, bundle1)
                }
                "21" -> {
                    val bundle1 = Bundle()
                    bundle1.putBundle("bundle1", args)
                    findNavController().navigate(R.id.valuationFormatRestoClientesFragment, bundle1)
                }
                "22" -> {
                    val bundle1 = Bundle()
                    bundle1.putBundle("bundle1", args)
                    findNavController().navigate(R.id.valuationFormatRestoClientesFragment, bundle1)
                }
                "23" -> {
                    val bundle1 = Bundle()
                    bundle1.putBundle("bundle1", args)
                    findNavController().navigate(R.id.valuationFormatRestoClientesFragment, bundle1)
                }
                "24" -> {
                    val bundle1 = Bundle()
                    bundle1.putBundle("bundle1", args)
                    findNavController().navigate(R.id.valuationFormatRestoClientesFragment, bundle1)
                }
                "25" -> {
                    val bundle1 = Bundle()
                    bundle1.putBundle("bundle1", args)
                    findNavController().navigate(R.id.valuationFormatRestoClientesFragment, bundle1)
                }
                "26" -> {
                    val bundle1 = Bundle()
                    bundle1.putBundle("bundle1", args)
                    findNavController().navigate(R.id.valuationFormatRestoClientesFragment, bundle1)
                }



            }

        }
    }
}
