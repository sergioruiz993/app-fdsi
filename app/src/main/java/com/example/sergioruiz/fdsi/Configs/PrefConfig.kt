package com.example.sergioruiz.fdsi.Configs

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.widget.Toast
import com.example.sergioruiz.fdsi.R

class PrefConfig(private var context: Context?) {
    private var sharedPreferences: SharedPreferences = context!!.getSharedPreferences(context!!.getString(R.string.UsersFile), Context.MODE_PRIVATE)


    @SuppressLint("CommitPrefEdits")
    fun writeLoginStatus(status: Boolean) {
        val editor: SharedPreferences.Editor = sharedPreferences.edit()
        editor.putBoolean(context?.getString(R.string.Status), status)
        editor.commit()
    }

    fun readLoginStatus(): Boolean? {
        return sharedPreferences.getBoolean(context?.getString(R.string.Status), false)
    }

    fun writeName(username: String){
        val editor: SharedPreferences.Editor = sharedPreferences.edit()
        editor.putString(context?.getString(R.string.UserName), username)
    }

    fun readName(): String? {
        return sharedPreferences.getString(context?.getString(R.string.UserName), "Usuarios")
    }

    fun displayToast(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }
}