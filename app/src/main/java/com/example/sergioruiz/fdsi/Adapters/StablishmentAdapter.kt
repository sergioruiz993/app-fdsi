package com.example.sergioruiz.fdsi.Adapters

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.os.bundleOf
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.sergioruiz.fdsi.Models.AdapterModel
import com.example.sergioruiz.fdsi.R

class StablishmentAdapter(private var context: Context?, val list: MutableList<AdapterModel>): RecyclerView.Adapter<StablishmentAdapter.StablishmentViewHolder>() {

    //var list: MutableList<AdapterModel> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StablishmentViewHolder {
        return StablishmentViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.rv_stablishment_item, parent, false))
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: StablishmentViewHolder, position: Int) {
        holder.imgStablishment.setImageResource(list.get(position).stablismentImage)
        holder.txvTitleStablishment.text = list.get(position).nameStablishment
        holder.itemView.setOnClickListener {v ->

            when(position) {
                0 -> {
                    //val SorianaVM = "Soriana Via Morelos"
                    val SE = "Selecciona un Establecimiento"
                    val SorianaSM = "Soriana Tienda 277 San Mateo"
                    val SorianaEchegaray = "Soriana Tienda 260 Echegaray"
                    val SorianaLomasVerdes = "Soriana Tienda 258 Lomas Verdes"
                    val SorianaPN = "Soriana tienda 281 Peri Norte"
                    val SorianaCuautitlan = "Soriana tienda 406 Cuautitlan"
                    val SorianaST = "Soriana Tienda 197 Sendero Toluca"
                    val SorianaIztapalapa = "Soriana Tienda 262 Iztapalapa"
                    val SorianaPD = "Soriana Tienda 397 Parque Delta"
                    val SorianaLF = "Soriana Tienda 009 La Fe"
                    val SorianaSantaM = "Soriana Tienda 027 Santa Maria"
                    val SorianaPC = "Soriana Tienda 419 Plaza Cantil"
                    val SorianaBuenavista = "Soriana Tienda 522 Buenavista"
                    val SorianaMixcoac = "Soriana Tienda 250 Mixcoac"
                    val SorianaZaragoza = "Soriana Tienda 269 Zaragoza"
                    val SorianaLV = "Soriana Tienda 255 La Villa"
                    val SorianaEugenia = "Soriana Tienda 279 Eugenia"
                    val SorianaTacubaya = "Soriana Tienda 257 Tacubaya"
                    val SorianaPA = "Soriana Tienda 682 Pabellón Azcapotzalco"
                    val SorianaER = "Soriana Tienda 261 El Rosario"
                    val SorianaCuitlahuac = "Soriana Tienda 253 Cuitláhuac"
                    val SorianaLaViga = "Soriana Tienda 251 La Viga"
                    val SorianaES = "Soriana Tienda 286 El Salado"
                    val SorianaConsulado = "Soriana Tienda 418 Consulado"
                    val SorianaVV = "Soriana Tienda 686 Vía Vallejo"
                    val SorianaMP = "Soriana Tienda 252 Miyana Polanco"
                    val SorianaLA = "Soriana Tienda 356 Los Angeles"
                    val SorianaMB = "Soriana Tienda 349 Montes Berneses"
                    val SorianaVH = "Soriana Tienda 345 Vista Hermosa"
                    val SorianaFundidora = "Soriana Tienda 341 Fundidora"
                    val SorianaTC = "Soriana Tienda 20 Terraza Coapa"
                    val ComercialMexicanaMix = "Comercial Mexicana Tienda 019 Mixcoac"
                    val CMPilares = "Comercial Mexicana Tienda Pilares"
                    val ComercialMexicanaNarv ="Comercial Mexicana Tienda  División del Norte"


                    val args = bundleOf(
                        "SE" to SE,
                        "SorianaSM" to SorianaSM,
                        "SorianaEchegaray" to SorianaEchegaray,
                        "SorianaLomasVerdes" to SorianaLomasVerdes,
                        "SorianaPN" to SorianaPN,
                        "SorianaCuautitlan" to SorianaCuautitlan,
                        "SorianaST" to SorianaST,
                        "SorianaIztapalapa" to SorianaIztapalapa,
                        "SorianaPD" to SorianaPD,
                        "SorianaLF" to SorianaLF,
                        "SorianaSantaM" to SorianaSantaM,
                        "SorianaPC" to SorianaPC,
                        "SorianaBuenavista" to SorianaBuenavista,
                        "SorianaMixcoac" to SorianaMixcoac,
                        "SorianaZaragoza" to SorianaZaragoza,
                        "SorianaLV" to SorianaLV,
                        "SorianaEugenia" to SorianaEugenia,
                        "SorianaTacubaya" to SorianaTacubaya,
                        "SorianaPA" to SorianaPA,
                        "SorianaER" to SorianaER,
                        "SorianaCuitlahuac" to SorianaCuitlahuac,
                        "SorianaLaViga" to SorianaLaViga,
                        "SorianaES" to SorianaES,
                        "SorianaConsulado" to SorianaConsulado,
                        "SorianaVV" to SorianaVV,
                        "SorianaMP" to SorianaMP,
                        "SorianaLA" to SorianaLA,
                        "SorianaMB" to SorianaMB,
                        "SorianaVH" to SorianaVH,
                        "SorianaFundidora" to SorianaFundidora,
                        "SorianaTC" to SorianaTC,
                        "ComercialMexicanaMix" to ComercialMexicanaMix,
                        "CMPilares" to CMPilares,
                        "ComercialMexicanaNarv" to ComercialMexicanaNarv,
                        "position" to position.toString()

                    )
                    holder.itemView.findNavController().navigate(R.id.statesFragment, args)
                }

                1 -> {
                    val SE = "Selecciona un Establecimiento"
                    val AeroMCentro = "Aeroméxico In plant PEMEX"
                    val AeroMCyoacan = "Inplant UNAM"
                    val AeroMC = "Aeroméxico In plant SAT"
                    val AeroMCoy = "Aeroméxico In plant SRÍA. MARINA"
                    val AeroMSL ="Aeroméxico In plant CÁMARA DIPUTADOS"


                    val args = bundleOf(
                        "SE" to SE,
                        "AeroMCentro" to  AeroMCentro,
                        "AeroMCyoacan" to AeroMCyoacan,
                        "AeroMC" to AeroMC,
                        "AeroMCoy" to  AeroMCoy,
                        "AeroMSL" to AeroMSL,
                        "position" to position.toString())


                    holder.itemView.findNavController().navigate(R.id.statesFragment, args)


                }
                2 -> {val SE = "Selecciona un Establecimiento"
                    val CECAPPDL = "CECAP - AM Formacion Interna"

                    val args = bundleOf(
                        "SE" to SE,
                        "CECAPPDL" to CECAPPDL,
                        "position" to position.toString())

                    holder.itemView.findNavController().navigate(R.id.statesFragment, args)

                }
                3 -> {val SE = "Selecciona un Establecimiento"
                    val UHCOYOACAN = "U.H. Cardiología"

                    val args = bundleOf(
                        "SE" to SE,
                        "UHCOYOACAN" to UHCOYOACAN,
                        "position" to position.toString())
                    holder.itemView.findNavController().navigate(R.id.statesFragment, args)

                }
                4 -> {val SE = "Selecciona un Establecimiento"
                    val clubPReforma = "Club Premier 250"

                    val args = bundleOf(
                        "SE" to SE,
                        "clubPReforma" to clubPReforma,
                        "position" to position.toString())

                    holder.itemView.findNavController().navigate(R.id.statesFragment, args)


                }
                5 -> { val SE = "Selecciona un Establecimiento"
                    val fdsiAsesores = "Oficinas Corporativas FDSI (Asesores)"
                    val fdsiAdministrativos = "Oficinas Corporativas FDSI (Administrativos)"
                    val fdsiSupervisores = "Oficinas Corporativas FDSI (Supervisores)"

                    val args = bundleOf(
                        "SE" to SE,
                        "fdsiAsesores" to fdsiAsesores,
                        "fdsiAdministrativos" to fdsiAdministrativos,
                        "fdsiSupervisores" to fdsiSupervisores,
                        "position" to position.toString())

                    holder.itemView.findNavController().navigate(R.id.statesFragment, args)


                }
                6 -> { val SE = "Selecciona un Establecimiento"
                    val SWISSSANANGEL = "Swiss Re"

                    val args = bundleOf(
                        "SE" to SE,
                        "SWISSSANANGEL" to SWISSSANANGEL,
                        "position" to position.toString())

                    holder.itemView.findNavController().navigate(R.id.statesFragment, args)

                }
                7 -> { val SE = "Selecciona un Establecimiento"
                    val BungeCuajimalpa = "Servicios Bunge"

                    val args = bundleOf(
                        "SE" to SE,
                        "BungeCuajimalpa" to BungeCuajimalpa,
                        "position" to position.toString())

                    holder.itemView.findNavController().navigate(R.id.statesFragment, args)

                }
                8 -> {val SE = "Selecciona un Establecimiento"
                    val PolymershapesN = "Polymershapes Naucalpan"
                    val PolymershapesTlahuac = "Polymershapes Tláhuac"
                    val PolymershapesLV= "Polymershapes La Viga"
                    val PolymershapesAPODACA ="Sabic Apodaca, Nuevo León"
                    val PolymershapesZAPOPAN = "Polymershapes Zapopan, Jalisco"
                    val PolymershapesCHIHUAHUA = "Polymershapes Chihuahua, Chihuahua"
                    val PolymershapesCelaya = "Polymershapes Celaya"
                    val PolymershapesIrapuato = "Polymershapes Irapuato"
                    val PolymershapesLeonCDC = "Polymershapes Leon Congreso de Chilpancingo"
                    val PolymershapesLeonIB = "Polymershapes Leon Islas Baliares"
                    val PolymershapesQueretaro = "Polymershapes Queretaro"

                    val args = bundleOf(
                        "SE" to SE,
                        "PolymershapesN" to PolymershapesN,
                        "PolymershapesTlahuac" to PolymershapesTlahuac,
                        "PolymershapesLV" to PolymershapesLV,
                        "PolymershapesAPODACA" to PolymershapesAPODACA,
                        "PolymershapesZAPOPAN" to PolymershapesZAPOPAN,
                        "PolymershapesCHIHUAHUA" to PolymershapesCHIHUAHUA,
                        "PolymershapesCelaya" to PolymershapesCelaya,
                        "PolymershapesIrapuato" to PolymershapesIrapuato,
                        "PolymershapesLeonCDC" to PolymershapesLeonCDC,
                        "PolymershapesLeonIB" to PolymershapesLeonIB,
                        "PolymershapesQueretaro" to PolymershapesQueretaro,
                        "position" to position.toString())

                    holder.itemView.findNavController().navigate(R.id.statesFragment, args)

                }

                9 -> {
                    val SE = "Selecciona un Establecimiento"
                    val BanorteMasaryk = "Banorte Suc. Banorte Masaryk"
                    val BanorteITG = "Banorte Suc. IXE Toriello Guerra"
                    val BanorteIFT = "Banorte Suc. Isidro Fabela Toluca"
                    val BanorteBSB = "Banorte Bahia de Santa Barbara"
                    val BanorteTroncoso = "Banorte Troncoso"
                    val BanorteJM = "Banorte Jamaica México"
                    val BanorteALLN = "Banorte Aeropuerto llegada Nacional"
                    val BanorteBAM = "Banorte Boulevard Aeropuerto México"
                    val BanorteAO = "Banorte Agricola Oriental"
                    val BanorteMA = "Banorte Milpa Alta"
                    val BanortePD = "Banorte Palacio de los Deportes"
                    val BanortePC = "Banorte Portal Churubusco"
                    val BanorteCH = "Banorte CAP Hipotecario (Jardín Balbuena)"
                    val BanorteSPP = "Banorte San Pedro de los Pinos"
                    val BanortePH = "Banorte Patriotismo Holbein"
                    val BanorteConstituyentes = "Banorte Constituyentes"
                    val BanorteRL = "Banorte Reforma Lomas"
                    val BanortePM = "Banorte Palmas México"
                    val BanorteMAI = "Banorte Multiplaza Aragón I"
                    val BanorteLVA = "Banorte Suc.  La Viga Apatlaco"
                    val BanortePSA = "Banorte Plaza San Juan de Aragón"
                    val BanorteCC = "Banorte Cuajimalpa Centro"
                    val BanorteVA = "Banorte Valle de Aragón"
                    val BanorteLF = "Banorte La Fontaine"
                    val BanorteTC = "Banorte Toluca Centro"
                    val BanorteMT = "Banorte Morelos Toluca"
                    val BanortePT = "Banorte Pilares Tolloacan"
                    val BanorteRD = "Banorte  Rancho Dolores"
                    val BanorteUT = "Banorte Urawa Tollocan"
                    val BanorteCIF = "Banorte Capacitacion Isidro Fabela"
                    val BanorteJV = "Banorte Julio Verne"
                    val BanorteFB = "Banorte Forum Buenavista"
                    val BanorteLVM = "Banorte La Villa México"
                    val BanorteLM = "Banorte Lindavista México"
                    val BanorteTL = "Banorte Torres Lindavista"
                    val BanorteDRL = "Banorte Direccion Regional Lindavista"
                    val BanorteCRR = "Banorte Cuautitlán Romero Rubio"
                    val BanortePIN = "Banorte Parque Industrial Naucalpan"
                    val BanorteSC = "Banorte Suc. Banorte San Cosme"
                    val BanorteVD = "Banorte Valle Dorado"
                    val BanorteCM = "Banorte Circuitos Medicos"
                    val BanorteEcatepec = "Banorte Ecatepec"
                    val BanorteCI = "Banorte Cuautitlán Izcalli"
                    val BanorteVHA = "Banorte Villas de la Hacienda Atizapán"
                    val BanorteLCN = "Banorte la Cúspide Naucalpan"
                    val BanorteHOA = "Banorte Hacienda Ojo de Agua"
                    val BanorteCPC = "Banorte Coacalco Power Center"
                    val BanorteAZE = "Banorte Suc. Banorte Atizapán Zona Esmeralda"
                    val BanortePJ = "Banorte Plaza Jardines"
                    val BanorteCAE = "Banorte Central de Abastos Ecatepec"
                    val BanorteZumpango = "Banorte Zumpango"
                    val BanorteLAT = "Banorte La Antigua Tlalnepantla"
                    val BanorteVCT = "Banorte Venustiano Carranza Toluca"
                    val BanortePST = "Banorte Pino Suarez Toluca"
                    val BanorteDRSMM = "Banorte Direccion Regional Sur México Manacar"
                    val BanorteNonoalco = "Banorte Suc. Banorte Nonoalco"
                    val BanorteAB = "Banorte Suc. Banorte Alce Blanco"
                    val BanorteAzcapotzalco = "Banorte Suc. Azcapotzalco"
                    val BanorteTacubaya = "Banorte Suc. Tacubaya"
                    val BanorteMontevideo = "Banorte Suc. Montevideo"
                    val BanorteCCSF = "Banorte Suc. Cto. Cmal. Santa Fe"
                    val BanorteAA = "Banorte Suc. Atizapán Alamedas"
                    val BanorteBGB = "Banorte Bellavista Gustavo Baz"
                    val BanorteCondesa = "Banorte  suc. Condesa"
                    val BanorteST = "Banorte suc. Pedregal Santa Teresa"
                    val BanorteCDD = "Banorte suc. Camara de Diputados"
                    val BanorteDROP = "Banorte Direccion Regional Optima Palmas"
                    val BanorteADP = "Banorte suc. Av. De la Paz"
                    val BanorteCT1T2 = "Banorte Cajeros T1 y T2"
                    val BanorteCCCS = "Banorte Circuito C. Comercial Satelite"
                    val BanorteWTC = "Banorte World Trade Center"
                    val BanorteSJ = "Banorte San Jerónimo"
                    val BanorteZA = "Banorte Zaragoza Agricola"
                    val BanorteLegaria = "Banorte Legaria"
                    val BanorteHuixquilucan = "Banorte Huixquilucan"
                    val BanorteTecamac = "Banorte Tecámac"
                    val BanorteADLI = "Banorte Angel de La Independencia"
                    val BanortePedregal = "Banorte Pedregal"
                    val BanorteFS = "Banorte Fuentes de Satelite"
                    val BanorteCopilco = "Banorte Copilco"
                    val BanorteTA = "Banorte Tlalpan Azteca"
                    val BanorteBEG = "Banorte bodega Eulalia Guzman"
                    val BanorteVenustianoC = "Banorte suc. Venustiano Carranza"
                    val BanorteOM = "Banorte Suc. Oficinas Masaryk"
                    val BanorteAT = "Banorte Avenida Toluca"
                    val BanorteTaxqueña = "Banorte Taxqueña"
                    val BanorteMS = "Banorte Mariscal Sucre"
                    val BanorteFBS = "Banorte Fuentes Brotantes"
                    val BanorteDDN = "Banorte División del Norte"
                    val BanorteBMS = "Banorte Medica Sur"
                    val BanorteVC = "Banorte Villa Coapa"
                    val BanorteCoaplaza = "Banorte Coaplaza"
                    val BanortePerisur = "Banorte Perisur"
                    val BanortePacifico = "Banorte Pacífico"
                    val BanorteBDM = "Banorte Baranca del Muerto"
                    val BanorteCPIC = "Banorte CPI Coyoacan"
                    val BanorteEugenia = "Banorte Eugenia"
                    val BanorteManacar = "Banorte Manacar"
                    val BanorteUM = "Banorte Universidad México"
                    val BanorteMerced = "Banorte Merced"
                    val BanorteSL = "Banorte San Lorenzo"
                    val BanorteCDM = "Banorte 5 de Mayo"
                    val BanorteC = "Banorte Condesa I"
                    val BanorteLC = "Banorte Suc. Banorte Lázaro Cárdenas"
                    val BanorteHET = "Banorte Suc. Banorte Heriberto Enriquez Toluca"
                    val BanorteIrrigacion = "Banorte Suc. Irrigación"


                    val args = bundleOf(
                        "SE" to SE,
                        "BanorteMasaryk" to BanorteMasaryk,
                        "BanorteITG" to BanorteITG,
                        "BanorteBSB" to BanorteBSB,
                        "BanorteTroncoso" to BanorteTroncoso,
                        "BanorteJM" to BanorteJM,
                        "BanorteALLN" to BanorteALLN,
                        "BanorteBAM" to BanorteBAM,
                        "BanorteIFT" to BanorteIFT,
                        "BanorteAO" to BanorteAO,
                        "BanorteMA" to BanorteMA,
                        "BanortePD" to BanortePD,
                        "BanortePC" to BanortePC,
                        "BanorteCH" to BanorteCH,
                        "BanorteVenustianoC" to BanorteVenustianoC,
                        "BanorteSPP" to BanorteSPP,
                        "BanortePH" to BanortePH,
                        "BanorteConstituyentes" to BanorteConstituyentes,
                        "BanorteRL" to BanorteRL,
                        "BanortePM" to BanortePM,
                        "BanorteMAI" to BanorteMAI,
                        "BanorteLVA" to BanorteLVA,
                        "BanortePSA" to BanortePSA,
                        "BanorteCC" to BanorteCC,
                        "BanorteVA" to BanorteVA,
                        "BanorteLF" to BanorteLF,
                        "BanorteTC" to BanorteTC,
                        "BanorteMT" to BanorteMT,
                        "BanortePT" to BanortePT,
                        "BanorteRD" to BanorteRD,
                        "BanorteUT" to BanorteUT,
                        "BanorteCIF" to BanorteCIF,
                        "BanorteJV" to BanorteJV,
                        "BanorteFB" to BanorteFB,
                        "BanorteLVM" to BanorteLVM,
                        "BanorteLM" to BanorteLM,
                        "BanorteTL" to BanorteTL,
                        "BanorteDRL" to BanorteDRL,
                        "BanorteCRR" to BanorteCRR,
                        "BanortePIN" to BanortePIN,
                        "BanorteSC" to BanorteSC,
                        "BanorteVD" to BanorteVD,
                        "BanorteCM" to BanorteCM,
                        "BanorteEcatepec" to BanorteEcatepec,
                        "BanorteCI" to BanorteCI,
                        "BanorteVHA" to BanorteVHA,
                        "BanorteLCN" to BanorteLCN,
                        "BanorteHOA" to BanorteHOA,
                        "BanorteCPC" to BanorteCPC,
                        "BanorteAZE" to BanorteAZE,
                        "BanortePJ" to BanortePJ,
                        "BanorteCAE" to BanorteCAE,
                        "BanorteZumpango" to BanorteZumpango,
                        "BanorteLAT" to BanorteLAT,
                        "BanorteVCT" to BanorteVCT,
                        "BanortePST" to BanortePST,
                        "BanorteDRSMM" to BanorteDRSMM,
                        "BanorteNonoalco" to BanorteNonoalco,
                        "BanorteAB" to BanorteAB,
                        "BanorteAzcapotzalco" to BanorteAzcapotzalco,
                        "BanorteTacubaya" to BanorteTacubaya,
                        "BanorteMontevideo" to BanorteMontevideo,
                        "BanorteCCSF" to BanorteCCSF,
                        "BanorteAA" to BanorteAA,
                        "BanorteBGB" to BanorteBGB,
                        "BanorteCondesa" to BanorteCondesa,
                        "BanorteST" to BanorteST,
                        "BanorteCDD" to BanorteCDD,
                        "BanorteDROP" to BanorteDROP,
                        "BanorteADP" to BanorteADP,
                        "BanorteCT1T2" to BanorteCT1T2,
                        "BanorteCCCS" to BanorteCCCS,
                        "BanorteWTC" to BanorteWTC,
                        "BanorteSJ" to BanorteSJ,
                        "BanorteZA" to BanorteZA,
                        "BanorteLegaria" to BanorteLegaria,
                        "BanorteHuixquilucan" to BanorteHuixquilucan,
                        "BanorteTecamac" to BanorteTecamac,
                        "BanorteADLI" to BanorteADLI,
                        "BanortePedregal" to BanortePedregal,
                        "BanorteFS" to BanorteFS,
                        "BanorteCopilco" to BanorteCopilco,
                        "BanorteTA" to BanorteTA,
                        "BanorteBEG" to BanorteBEG,
                        "BanorteSVC" to BanorteVC,
                        "BanorteOM" to BanorteOM,
                        "BanorteAT" to BanorteAT,
                        "BanorteTaxqueña" to BanorteTaxqueña,
                        "BanorteMS" to BanorteMS,
                        "BanorteFBS" to BanorteFBS,
                        "BanorteDDN" to BanorteDDN,
                        "BanorteBMS" to BanorteBMS,
                        "BanorteVC" to BanorteVC,
                        "BanorteCoaplaza" to BanorteCoaplaza,
                        "BanortePerisur" to BanortePerisur,
                        "BanortePacifico" to BanortePacifico,
                        "BanorteBDM" to BanorteBDM,
                        "BanorteCPIC" to BanorteCPIC,
                        "BanorteEugenia" to BanorteEugenia,
                        "BanorteManacar" to BanorteManacar,
                        "BanorteUM" to BanorteUM,
                        "BanorteMerced" to BanorteMerced,
                        "BanorteSL" to BanorteSL,
                        "BanorteCDM" to BanorteCDM,
                        "BanorteC" to BanorteC,
                        "BanorteLC" to BanorteLC,
                        "BanorteHET" to BanorteHET,
                        "BanorteIrrigacion" to BanorteIrrigacion,
                        "position" to position.toString()
                    )
                    holder.itemView.findNavController().navigate(R.id.statesFragment, args)
                }



                10 -> {
                    val SE = "Selecciona un Establecimiento"
                    val SIVALEREFORMA = "Sí Vale"
                    val SIVALETIJUANA = "Sí Vale Tijuana, Baja California"
                    val SIVALEQUERETARO = "Sí Vale Querétaro, Querétaro"
                    val SIVALEGUADALAJARA = "Sí Vale Guadalajara, Jalisco"
                    val SIVALETLALNE = "Si Vale Valle Dorado"

                    val args = bundleOf(
                        "SE" to SE,
                        "SIVALEREFORMA" to SIVALEREFORMA,
                        "SIVALETIJUANA" to SIVALETIJUANA,
                        "SIVALEQUERETARO" to SIVALEQUERETARO,
                        "SIVALEGUADALAJARA" to SIVALEGUADALAJARA,
                        "SIVALETLALNE" to SIVALETLALNE,
                        "position" to position.toString())

                    holder.itemView.findNavController().navigate(R.id.statesFragment, args)

                }

                11 -> {
                    val SE = "Selecciona un Establecimiento"
                    val ManufactureraVallejo = "Manufacturera Mexicana de Partes"

                    val args = bundleOf(
                        "SE" to SE,
                        "ManufactureraVallejo" to ManufactureraVallejo,
                        "position" to position.toString())

                    holder.itemView.findNavController().navigate(R.id.statesFragment, args)

                }



                12 -> {
                    val SE = "Selecciona un Establecimiento"
                    val MolinosBMerida = "Molinos Bunge Merida, Yucatán"

                    val args = bundleOf(
                        "SE" to SE,
                        "MolinosBMerida" to MolinosBMerida,
                        "position" to position.toString())

                    holder.itemView.findNavController().navigate(R.id.statesFragment, args)

                }

                13 -> {
                    val SE = "Selecciona un Establecimiento"
                    val TRAVELSANANGEL = "Travel  R US"

                    val args = bundleOf(
                        "SE" to SE,
                        "TRAVELSANANGEL" to TRAVELSANANGEL,
                        "position" to position.toString())

                    holder.itemView.findNavController().navigate(R.id.statesFragment, args)

                }


                14 -> {
                    val SE = "Selecciona un Establecimiento"
                    val Timex = "Tiempo  (Timex) 1 mayo Naucalpan"

                    val args = bundleOf(
                        "SE" to SE,
                        "Timex" to Timex,
                        "position" to position.toString())

                    holder.itemView.findNavController().navigate(R.id.statesFragment, args)

                }

                15 -> {
                    val SE = "Selecciona un Establecimiento"
                    val DistribuidoraALSEAT = "Distribuidora e Importadora Alsea (Tlahuac)"
                    val DistribuidoraALlseaC = "Distribuidora e Importadora Alsea (COA)"

                    val args = bundleOf(
                        "SE" to SE,
                        "DistribuidoraALSEAT" to DistribuidoraALSEAT,
                        "DistribuidoraALlseaC" to DistribuidoraALlseaC,
                        "position" to position.toString())

                    holder.itemView.findNavController().navigate(R.id.statesFragment, args)

                }

                16 -> {
                    val SE = "Selecciona un Establecimiento"
                    val CasaCerroLV ="Casa Cerro "

                    val args = bundleOf(
                        "SE" to SE,
                        "CasaCerroLV" to CasaCerroLV,
                        "position" to position.toString())

                    holder.itemView.findNavController().navigate(R.id.statesFragment, args)

                }

                17 -> {
                    val SE = "Selecciona un Establecimiento"
                    val PhillipsM = "Philips Mexico Comercial"

                    val args = bundleOf(
                        "SE" to SE,
                        "PhillipsM" to PhillipsM,
                        "position" to position.toString())

                    holder.itemView.findNavController().navigate(R.id.statesFragment, args)

                }

                18 -> {
                    val SE = "Selecciona un Establecimiento"
                    val FlexigripCentro ="Flexigrip de México"

                    val args = bundleOf(
                        "SE" to SE,
                        "FlexigripCentro" to FlexigripCentro,
                        "position" to position.toString())

                    holder.itemView.findNavController().navigate(R.id.statesFragment, args)

                }

                19 -> {
                    val SE = "Selecciona un Establecimiento"
                    val BufeteCoyoacan ="Bufete Diaz Miron y Asociados"

                    val args = bundleOf(
                        "SE" to SE,
                        "BufeteCoyoacan" to BufeteCoyoacan,
                        "position" to position.toString())

                    holder.itemView.findNavController().navigate(R.id.statesFragment, args)

                }

                20 -> {
                    val SE = "Selecciona un Establecimiento"
                    val TOURSREFORMA = "Villa Tours"

                    val args = bundleOf(
                        "SE" to SE,
                        "TOURSREFORMA" to TOURSREFORMA,
                        "position" to position.toString())
                    holder.itemView.findNavController().navigate(R.id.statesFragment, args)

                }
                21 -> {
                    val SE = "Selecciona un Establecimiento"
                    val WPSmERIDA = "WSP México"

                    val args = bundleOf(
                        "SE" to SE,
                        "WPSmERIDA" to WPSmERIDA,
                        "position" to position.toString())
                    holder.itemView.findNavController().navigate(R.id.statesFragment, args)

                }
                22 -> {
                    val SE = "Selecciona un Establecimiento"
                    val ComustentaEcatepec = "Comsustenta"

                    val args = bundleOf(
                        "SE" to SE,
                        "ComustentaEcatepec" to ComustentaEcatepec,
                        "position" to position.toString())

                    holder.itemView.findNavController().navigate(R.id.statesFragment, args)

                }
                23 -> {
                    val SE = "Selecciona un Establecimiento"
                    val ComerciaizadoraBex = "Comercializadora y Servicios Bexica"

                    val args = bundleOf(
                        "SE" to SE,
                        "ComerciaizadoraBex" to ComerciaizadoraBex,
                        "position" to position.toString())

                    holder.itemView.findNavController().navigate(R.id.statesFragment, args)

                }
                24 -> {
                    val SE = "Selecciona un Establecimiento"
                    val ElevenCP = "7-Eleven Corporativo"
                    val ElevenNYSM = "7-Eleven bodega Naucalpan y San Mateo"
                    val ElevenGenova = "7-Eleven  Genova"
                    val ElevenC= "7-Eleven  Ciclistas"
                    val ElevenCS = "7-Eleven Camara de Senadores"
                    val ElevenMoliere= "7-Eleven Moliere"
                    val ElevenSantaFe = "7-Eleven Santa Fe"
                    val ElevenFerreria = "7-Eleven Ferreria"
                    val ElevenGB = "7-Eleven Gustavo Baz"
                    val ElevenSC = "7-Eleven Santa Clara"

                    val args = bundleOf(
                        "SE" to SE,
                        "ElevenCP" to ElevenCP,
                        "ElevenNYSM" to ElevenNYSM,
                        "ElevenGenova" to ElevenGenova,
                        "ElevenC" to ElevenC,
                        "ElevenCS" to ElevenCS,
                        "ElevenMoliere" to ElevenMoliere,
                        "ElevenSantaFe" to ElevenSantaFe,
                        "ElevenFerreria" to ElevenFerreria,
                        "ElevenGB" to ElevenGB,
                        "ElevenSC" to ElevenSC,
                        "position" to position.toString())

                    holder.itemView.findNavController().navigate(R.id.statesFragment, args)

                }

                25 -> {
                    val SE = "Selecciona un Establecimiento"
                    val GrupoDescanso = "Grupo Internacional del Descanso"

                    val args = bundleOf(
                        "SE" to SE,
                        "GrupoDescanso" to GrupoDescanso,
                        "position" to position.toString())

                    holder.itemView.findNavController().navigate(R.id.statesFragment, args)

                }
                26 -> {
                    val SE = "Selecciona un Establecimiento"
                    val InmoviliariaF = "Inmobiliaria Vigas Fuentes de Satélite"

                    val args = bundleOf(
                        "SE" to SE,
                        "InmoviliariaF" to InmoviliariaF,
                        "position" to position.toString())

                    holder.itemView.findNavController().navigate(R.id.statesFragment, args)

                }

            }
            //holder.itemView.findNavController().navigate(R.id.listEvaluation)
        }
    }

    inner class StablishmentViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        var imgStablishment: ImageView = itemView.findViewById(R.id.imgStablishment)
        var txvTitleStablishment: TextView = itemView.findViewById(R.id.txvTitleStablishment)
    }
}