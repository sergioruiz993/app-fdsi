package com.example.sergioruiz.fdsi


import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.ProgressDialog
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.graphics.*
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.*
import android.provider.MediaStore
import android.text.Editable
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.core.os.bundleOf
import androidx.core.widget.NestedScrollView
import androidx.navigation.fragment.findNavController
import com.example.sergioruiz.fdsi.BaseDatosSQLite.SQLiteClass
import com.github.gcacace.signaturepad.views.SignaturePad
import android.util.Base64
import androidx.loader.content.CursorLoader
import com.example.sergioruiz.fdsi.ClientApi.RetrofitClient
import com.example.sergioruiz.fdsi.ClientApi.RetrofitClient2
import com.example.sergioruiz.fdsi.Interfaces.ApiFDSI
import com.example.sergioruiz.fdsi.Models.MSG
import com.example.sergioruiz.fdsi.Preferences.PreferenceHelper
import com.itextpdf.testutils.ITextTest
import com.itextpdf.text.*
import com.itextpdf.text.pdf.*
import com.nightonke.boommenu.BoomButtons.TextOutsideCircleButton
import kotlinx.android.synthetic.main.fragment_digital_signature.*
import kotlinx.android.synthetic.main.fragment_evidence.*
import kotlinx.android.synthetic.main.fragment_login.*
import kotlinx.android.synthetic.main.fragment_valuation_format.*
import kotlinx.android.synthetic.main.fragment_valuation_format.btnClean
//import kotlinx.android.synthetic.main.fragment_valuation_format.btnCreatePdf
import kotlinx.android.synthetic.main.fragment_valuation_format.btnEvaluacion
import kotlinx.android.synthetic.main.fragment_valuation_format.btnGoToSpinners
//import kotlinx.android.synthetic.main.fragment_valuation_format.btnSaveToSQLite
//import kotlinx.android.synthetic.main.fragment_valuation_format.btnUpdateInformation
//import kotlinx.android.synthetic.main.fragment_valuation_format.btnSaveSignature
import kotlinx.android.synthetic.main.fragment_valuation_format.etNameSignature
//import kotlinx.android.synthetic.main.fragment_valuation_format.fabVerListaReportes
import kotlinx.android.synthetic.main.fragment_valuation_format.imgEvidencia
import kotlinx.android.synthetic.main.fragment_valuation_format.imgEvidencia2
import kotlinx.android.synthetic.main.fragment_valuation_format.imgEvidencia3
import kotlinx.android.synthetic.main.fragment_valuation_format.imgEvidencia4
import kotlinx.android.synthetic.main.fragment_valuation_format.imgEvidencia5
/*import kotlinx.android.synthetic.main.fragment_valuation_format.imgEvidencia6
import kotlinx.android.synthetic.main.fragment_valuation_format.imgEvidencia7
import kotlinx.android.synthetic.main.fragment_valuation_format.imgEvidencia8
import kotlinx.android.synthetic.main.fragment_valuation_format.imgEvidencia9*/
import kotlinx.android.synthetic.main.fragment_valuation_format.rbCorregir1
import kotlinx.android.synthetic.main.fragment_valuation_format.rbCorregir10
import kotlinx.android.synthetic.main.fragment_valuation_format.rbCorregir11
import kotlinx.android.synthetic.main.fragment_valuation_format.rbCorregir12
import kotlinx.android.synthetic.main.fragment_valuation_format.rbCorregir13
import kotlinx.android.synthetic.main.fragment_valuation_format.rbCorregir14
import kotlinx.android.synthetic.main.fragment_valuation_format.rbCorregir15
import kotlinx.android.synthetic.main.fragment_valuation_format.rbCorregir16
import kotlinx.android.synthetic.main.fragment_valuation_format.rbCorregir17
import kotlinx.android.synthetic.main.fragment_valuation_format.rbCorregir18
import kotlinx.android.synthetic.main.fragment_valuation_format.rbCorregir19
import kotlinx.android.synthetic.main.fragment_valuation_format.rbCorregir2
import kotlinx.android.synthetic.main.fragment_valuation_format.rbCorregir20
import kotlinx.android.synthetic.main.fragment_valuation_format.rbCorregir21
import kotlinx.android.synthetic.main.fragment_valuation_format.rbCorregir22
import kotlinx.android.synthetic.main.fragment_valuation_format.rbCorregir23
import kotlinx.android.synthetic.main.fragment_valuation_format.rbCorregir24
import kotlinx.android.synthetic.main.fragment_valuation_format.rbCorregir3
import kotlinx.android.synthetic.main.fragment_valuation_format.rbCorregir4
import kotlinx.android.synthetic.main.fragment_valuation_format.rbCorregir5
import kotlinx.android.synthetic.main.fragment_valuation_format.rbCorregir6
import kotlinx.android.synthetic.main.fragment_valuation_format.rbCorregir7
import kotlinx.android.synthetic.main.fragment_valuation_format.rbCorregir8
import kotlinx.android.synthetic.main.fragment_valuation_format.rbCorregir9
import kotlinx.android.synthetic.main.fragment_valuation_format.rbDesempeño1
import kotlinx.android.synthetic.main.fragment_valuation_format.rbDesempeño10
import kotlinx.android.synthetic.main.fragment_valuation_format.rbDesempeño11
import kotlinx.android.synthetic.main.fragment_valuation_format.rbDesempeño12
import kotlinx.android.synthetic.main.fragment_valuation_format.rbDesempeño13
import kotlinx.android.synthetic.main.fragment_valuation_format.rbDesempeño14
import kotlinx.android.synthetic.main.fragment_valuation_format.rbDesempeño15
import kotlinx.android.synthetic.main.fragment_valuation_format.rbDesempeño16
import kotlinx.android.synthetic.main.fragment_valuation_format.rbDesempeño17
import kotlinx.android.synthetic.main.fragment_valuation_format.rbDesempeño18
import kotlinx.android.synthetic.main.fragment_valuation_format.rbDesempeño19
import kotlinx.android.synthetic.main.fragment_valuation_format.rbDesempeño2
import kotlinx.android.synthetic.main.fragment_valuation_format.rbDesempeño20
import kotlinx.android.synthetic.main.fragment_valuation_format.rbDesempeño21
import kotlinx.android.synthetic.main.fragment_valuation_format.rbDesempeño22
import kotlinx.android.synthetic.main.fragment_valuation_format.rbDesempeño23
import kotlinx.android.synthetic.main.fragment_valuation_format.rbDesempeño24
import kotlinx.android.synthetic.main.fragment_valuation_format.rbDesempeño3
import kotlinx.android.synthetic.main.fragment_valuation_format.rbDesempeño4
import kotlinx.android.synthetic.main.fragment_valuation_format.rbDesempeño5
import kotlinx.android.synthetic.main.fragment_valuation_format.rbDesempeño6
import kotlinx.android.synthetic.main.fragment_valuation_format.rbDesempeño7
import kotlinx.android.synthetic.main.fragment_valuation_format.rbDesempeño8
import kotlinx.android.synthetic.main.fragment_valuation_format.rbDesempeño9
import kotlinx.android.synthetic.main.fragment_valuation_format.rbOportunidad1
import kotlinx.android.synthetic.main.fragment_valuation_format.rbOportunidad10
import kotlinx.android.synthetic.main.fragment_valuation_format.rbOportunidad11
import kotlinx.android.synthetic.main.fragment_valuation_format.rbOportunidad12
import kotlinx.android.synthetic.main.fragment_valuation_format.rbOportunidad13
import kotlinx.android.synthetic.main.fragment_valuation_format.rbOportunidad14
import kotlinx.android.synthetic.main.fragment_valuation_format.rbOportunidad15
import kotlinx.android.synthetic.main.fragment_valuation_format.rbOportunidad16
import kotlinx.android.synthetic.main.fragment_valuation_format.rbOportunidad17
import kotlinx.android.synthetic.main.fragment_valuation_format.rbOportunidad18
import kotlinx.android.synthetic.main.fragment_valuation_format.rbOportunidad19
import kotlinx.android.synthetic.main.fragment_valuation_format.rbOportunidad2
import kotlinx.android.synthetic.main.fragment_valuation_format.rbOportunidad20
import kotlinx.android.synthetic.main.fragment_valuation_format.rbOportunidad21
import kotlinx.android.synthetic.main.fragment_valuation_format.rbOportunidad22
import kotlinx.android.synthetic.main.fragment_valuation_format.rbOportunidad23
import kotlinx.android.synthetic.main.fragment_valuation_format.rbOportunidad24
import kotlinx.android.synthetic.main.fragment_valuation_format.rbOportunidad3
import kotlinx.android.synthetic.main.fragment_valuation_format.rbOportunidad4
import kotlinx.android.synthetic.main.fragment_valuation_format.rbOportunidad5
import kotlinx.android.synthetic.main.fragment_valuation_format.rbOportunidad6
import kotlinx.android.synthetic.main.fragment_valuation_format.rbOportunidad7
import kotlinx.android.synthetic.main.fragment_valuation_format.rbOportunidad8
import kotlinx.android.synthetic.main.fragment_valuation_format.rbOportunidad9
import kotlinx.android.synthetic.main.fragment_valuation_format.scLayouts
import kotlinx.android.synthetic.main.fragment_valuation_format.signature_pad
import kotlinx.android.synthetic.main.fragment_valuation_format.txtAsistencias
import kotlinx.android.synthetic.main.fragment_valuation_format.txtEstablishment
import kotlinx.android.synthetic.main.fragment_valuation_format.txtEvaluacion
import kotlinx.android.synthetic.main.fragment_valuation_format.txtFaltasdMes
import kotlinx.android.synthetic.main.fragment_valuation_format.txtFechaElaboracion
import kotlinx.android.synthetic.main.fragment_valuation_format.txtPlantilla
import kotlinx.android.synthetic.main.fragment_valuation_format.txtPorcentajeAsistencias
import kotlinx.android.synthetic.main.fragment_valuation_format_resto_clientes.*
import okhttp3.MediaType
import okhttp3.MediaType.Companion.parse
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import org.json.JSONException
import org.json.JSONObject
import org.w3c.dom.Text
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.*
import java.lang.Exception
import java.text.ParseException
import java.text.SimpleDateFormat
import java.time.LocalTime
import java.util.*
import kotlin.collections.ArrayList

/**
 * A simple [Fragment] subclass.
 */
class ValuationFormatFragment : Fragment(), View.OnClickListener, SignaturePad.OnSignedListener {

    private var progressBar : ProgressDialog? = null
    private var progressHandler = Handler()
    private var statusBar : Int = 0
    private var file: File? = null
    private var file2: File? = null
    private var file3 : File? = null
    private var file4 : File? = null
    private var file5 : File? = null
    private var imageFile1 : String? = null
    private var imageFile2 : String? = null
    private var imageFile3 : String? = null
    private var imageFile4 : String? = null
    private var imageFile5 : String? = null
    /*private var file6 : File? = null
    private var file7 : File? = null
    private var file8 : File? = null
    private var file9 : File? = null*/
    private var directoryName = "Evidencias FDSI"
    private var deviceIdentifier: String? = null
    private var currentPhotoPath: String? = null
    private var REQUEST_PICTURE_CAPTURE: Int? = 1
    private var REQUEST_PICTURE_CAPTURE_2: Int? = 2
    private var REQUEST_PICTURE_CAPTURE_3: Int? = 3
    private var REQUEST_PICTURE_CAPTURE_4: Int? = 4
    private var REQUEST_PICTURE_CAPTURE_5: Int? = 5
    /*private var REQUEST_PICTURE_CAPTURE_6: Int? = 6
    private var REQUEST_PICTURE_CAPTURE_7: Int? = 7
    private var REQUEST_PICTURE_CAPTURE_8: Int? = 8
    private var REQUEST_PICTURE_CAPTURE_9: Int? = 9*/
    private var radioButton: RadioButton? = null
    lateinit var views: View
    private var pdfFile: File? = null
    private var baseFont: BaseFont? = null
    private val filePath = "MisPdf"
    private var apiFDSI: ApiFDSI? = null
    private var image: File? = null
    private lateinit var preferenceHelper: PreferenceHelper
    //private lateinit var radioGroup: RadioGroup

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_valuation_format, container, false)
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        getInstallationIndentifier()
        //getInformation()
        apiFDSI = RetrofitClient2().getClient2()?.create(ApiFDSI::class.java)
        preferenceHelper = PreferenceHelper(activity?.applicationContext!!)
        //verifyStoragePermissions(activity)

        if (ContextCompat.checkSelfPermission(activity?.applicationContext!!, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity!!, arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE), 0)
            if (!activity?.packageManager!!.hasSystemFeature(PackageManager.FEATURE_CAMERA)){
                imgEvidencia.isEnabled = false
            }
        }

        val today = Calendar.getInstance().time
        val formatDate = SimpleDateFormat("dd/MM/yyyy", Locale("es", "MX"))
        try {
            val date = formatDate.format(today)
            txtFaltasdMes.setText(date)
        } catch (ex: ParseException) {
            ex.printStackTrace()
        }

        for (i in 0 until boom.piecePlaceEnum.pieceNumber()) {
            val position = i
            when (position) {
                0 -> {
                    val builder = TextOutsideCircleButton.Builder()
                        .normalImageRes(R.drawable.ic_save)
                        .normalText("Crear, guardar y enviar")
                        .normalColor(Color.parseColor("#FFFFFF"))
                        .listener {
                            onCustomClick()
                            //saveInformation()
                            try {
                                postInformation("$imageFile1",
                                    "$imageFile2",
                                    "$imageFile3",
                                    "$imageFile4",
                                    "$imageFile5",
                                    directoryName)
                            } catch (e: Exception) {
                                Toast.makeText(activity, "No se ha completado el formulario", Toast.LENGTH_SHORT).show()
                            }
                            createPDFClick()

                        }
                    boom.addBuilder(builder)
                }
                /*1 -> {
                    val builder = TextOutsideCircleButton.Builder()
                        .normalImageRes(R.drawable.ic_update)
                        .normalText("Actualizar")
                        .normalColor(Color.parseColor("#FFFFFF"))
                        .listener {
                            updateInformation()
                        }
                    boom.addBuilder(builder)
                }
                2 -> {
                    val builder = TextOutsideCircleButton.Builder()
                        .normalImageRes(R.drawable.ic_list)
                        .normalText("Ver Listado")
                        .normalColor(Color.parseColor("#FFFFFF"))
                        .listener {
                            goListReports()
                        }
                    boom.addBuilder(builder)
                }*/
            }
        }

        /*if (ContextCompat.checkSelfPermission(activity?.applicationContext!!, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity!!, arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE), 0)
            if (!activity?.packageManager!!.hasSystemFeature(PackageManager.FEATURE_CAMERA)){
                imgEvidencia.isEnabled = false
                //imgEvidencia2.isEnabled = false
            }
        }*/

        val myDrawable = activity?.resources?.getDrawable(R.drawable.camara)
        imgEvidencia.setImageDrawable(myDrawable)
        imgEvidencia2.setImageDrawable(myDrawable)
        imgEvidencia3.setImageDrawable(myDrawable)
        imgEvidencia4.setImageDrawable(myDrawable)
        imgEvidencia5.setImageDrawable(myDrawable)
        /*imgEvidencia6.setImageDrawable(myDrawable)
        imgEvidencia7.setImageDrawable(myDrawable)
        imgEvidencia8.setImageDrawable(myDrawable)
        imgEvidencia9.setImageDrawable(myDrawable)*/
        radioButton = RadioButton(activity?.applicationContext)
        /*scLayouts.setOnScrollChangeListener(NestedScrollView.OnScrollChangeListener { v, scrollX, scrollY, oldScrollX, oldScrollY ->
            if (scrollY > oldScrollY) {
                fabVerListaReportes.collapse()
            } else {
                fabVerListaReportes.expand()
            }
        })*/
        //fabVerListaReportes.setOnClickListener(this)
        //imgEvidencia2.setImageDrawable(myDrawable)

        /*scLayouts.setOnTouchListener { v, event ->
            val imm = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(activity?.currentFocus?.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
            true
        }*/
        imgEvidencia.setOnClickListener(this)
        imgEvidencia2.setOnClickListener(this)
        imgEvidencia3.setOnClickListener(this)
        imgEvidencia4.setOnClickListener(this)
        imgEvidencia5.setOnClickListener(this)
        /*imgEvidencia6.setOnClickListener(this)
        imgEvidencia7.setOnClickListener(this)
        imgEvidencia8.setOnClickListener(this)
        imgEvidencia9.setOnClickListener(this)*/
        signature_pad.setOnSignedListener(this)
        btnClean.setOnClickListener(this)
        //btnUpdateInformation.setOnClickListener(this)
        //btnCreatePdf.setOnClickListener(this)
        //btnSaveSignature.setOnClickListener(this)
        //btnEvaluacion.setOnClickListener(this)
        //btnSaveToSQLite.setOnClickListener(this)
        //btnGetInformation.setOnClickListener(this)
        btnGoToSpinners.setOnClickListener(this)
        //btnSaveInformation.setOnClickListener(this)
        //radioButton?.setOnClickListener(this)

        rbDesempeño1.setOnClickListener(this)
        rbDesempeño2.setOnClickListener(this)
        rbDesempeño3.setOnClickListener(this)
        rbDesempeño4.setOnClickListener(this)
        rbDesempeño5.setOnClickListener(this)
        rbDesempeño6.setOnClickListener(this)
        rbDesempeño7.setOnClickListener(this)
        rbDesempeño8.setOnClickListener(this)
        rbDesempeño9.setOnClickListener(this)
        rbDesempeño10.setOnClickListener(this)
        rbDesempeño11.setOnClickListener(this)
        rbDesempeño12.setOnClickListener(this)
        rbDesempeño13.setOnClickListener(this)
        rbDesempeño14.setOnClickListener(this)
        rbDesempeño15.setOnClickListener(this)
        rbDesempeño16.setOnClickListener(this)
        rbDesempeño17.setOnClickListener(this)
        rbDesempeño18.setOnClickListener(this)
        rbDesempeño19.setOnClickListener(this)
        rbDesempeño20.setOnClickListener(this)
        rbDesempeño21.setOnClickListener(this)
        rbDesempeño22.setOnClickListener(this)
        //rbDesempeño23.setOnClickListener(this)
        //rbDesempeño24.setOnClickListener(this)



        rbOportunidad1.setOnClickListener(this)
        rbOportunidad2.setOnClickListener(this)
        rbOportunidad3.setOnClickListener(this)
        rbOportunidad4.setOnClickListener(this)
        rbOportunidad5.setOnClickListener(this)
        rbOportunidad6.setOnClickListener(this)
        rbOportunidad7.setOnClickListener(this)
        rbOportunidad8.setOnClickListener(this)
        rbOportunidad9.setOnClickListener(this)
        rbOportunidad10.setOnClickListener(this)
        rbOportunidad11.setOnClickListener(this)
        rbOportunidad12.setOnClickListener(this)
        rbOportunidad13.setOnClickListener(this)
        rbOportunidad14.setOnClickListener(this)
        rbOportunidad15.setOnClickListener(this)
        rbOportunidad16.setOnClickListener(this)
        rbOportunidad17.setOnClickListener(this)
        rbOportunidad18.setOnClickListener(this)
        rbOportunidad19.setOnClickListener(this)
        rbOportunidad20.setOnClickListener(this)
        rbOportunidad21.setOnClickListener(this)
        rbOportunidad22.setOnClickListener(this)
        rbOportunidad23.setOnClickListener(this)
        rbOportunidad24.setOnClickListener(this)
        //rbOportunidad25.setOnClickListener(this)
        //rbOportunidad26.setOnClickListener(this)


        rbCorregir1.setOnClickListener(this)
        rbCorregir2.setOnClickListener(this)
        rbCorregir3.setOnClickListener(this)
        rbCorregir4.setOnClickListener(this)
        rbCorregir5.setOnClickListener(this)
        rbCorregir6.setOnClickListener(this)
        rbCorregir7.setOnClickListener(this)
        rbCorregir8.setOnClickListener(this)
        rbCorregir9.setOnClickListener(this)
        rbCorregir10.setOnClickListener(this)
        rbCorregir11.setOnClickListener(this)
        rbCorregir12.setOnClickListener(this)
        rbCorregir13.setOnClickListener(this)
        rbCorregir14.setOnClickListener(this)
        rbCorregir15.setOnClickListener(this)
        rbCorregir16.setOnClickListener(this)
        rbCorregir17.setOnClickListener(this)
        rbCorregir18.setOnClickListener(this)
        rbCorregir19.setOnClickListener(this)
        rbCorregir20.setOnClickListener(this)
        rbCorregir21.setOnClickListener(this)
        rbCorregir22.setOnClickListener(this)
        rbCorregir23.setOnClickListener(this)
        rbCorregir24.setOnClickListener(this)
        //rbCorregir25.setOnClickListener(this)
        //rbCorregir26.setOnClickListener(this)

        val bundle1 = arguments?.getBundle("bundle1")
        if (bundle1 != null) {
            val sp1 = arguments?.getBundle("bundle1")?.getString("sp1")
            val sp2 = arguments?.getBundle("bundle1")?.getString("sp2")
            val sp3 = arguments?.getBundle("bundle1")?.getString("sp3")

            txtEstablishment.keyListener = null
            txtPlantilla.keyListener = null
            txtAsistencias.keyListener = null
            txtEvaluacion.keyListener = null

            txtEstablishment.setText(sp3)
            txtPlantilla.setText(sp2)
            txtAsistencias.setText(sp1)

            when(sp3) {
                "Soriana Tienda 277 San Mateo" -> {
                    txtPorcentajeAsistencias.setText("4")
                }
                "Soriana Tienda 260 Echegaray" -> {
                    txtPorcentajeAsistencias.setText("7")
                }
                "Soriana Tienda 258 Lomas Verdes" -> {
                    txtPorcentajeAsistencias.setText("6")
                }
                "Soriana tienda 281 Peri Norte" -> {
                    txtPorcentajeAsistencias.setText("5")
                }
                "Soriana tienda 406 Cuautitlan" -> {
                    txtPorcentajeAsistencias.setText("4")
                }
                "Soriana Tienda 197 Sendero Toluca" -> {
                    txtPorcentajeAsistencias.setText("6")
                }
                "Soriana Tienda 262 Iztapalapa" -> {
                    txtPorcentajeAsistencias.setText("11")
                }
                "Soriana Tienda 397 Parque Delta" -> {
                    txtPorcentajeAsistencias.setText("13")
                }
                "Soriana Tienda 009 La Fe" -> {
                    txtPorcentajeAsistencias.setText("4")
                }
                "Soriana Tienda 027 Santa Maria" -> {
                    txtPorcentajeAsistencias.setText("4")
                }
                "Soriana Tienda 419 Plaza Cantil" -> {
                    txtPorcentajeAsistencias.setText("7")
                }
                "Soriana Tienda 522 Buenavista" -> {
                    txtPorcentajeAsistencias.setText("6")
                }
                "Soriana Tienda 250 Mixcoac" -> {
                    txtPorcentajeAsistencias.setText("10")
                }
                "Soriana Tienda 269 Zaragoza" -> {
                    txtPorcentajeAsistencias.setText("7")
                }
                "Soriana Tienda 255 La Villa" -> {
                    txtPorcentajeAsistencias.setText("11")
                }
                "Soriana Tienda 279 Eugenia" -> {
                    txtPorcentajeAsistencias.setText("9")
                }
                "Soriana Tienda 257 Tacubaya" -> {
                    txtPorcentajeAsistencias.setText("11")
                }
                "Soriana Tienda 682 Pabellón Azcapotzalco" -> {
                    txtPorcentajeAsistencias.setText("5")
                }
                "Soriana Tienda 261 El Rosario" -> {
                    txtPorcentajeAsistencias.setText("6")
                }
                "Soriana Tienda 253 Cuitláhuac" -> {
                    txtPorcentajeAsistencias.setText("8")
                }
                "Soriana Tienda 251 La Viga" -> {
                    txtPorcentajeAsistencias.setText("9")
                }
                "Soriana Tienda 286 El Salado" -> {
                    txtPorcentajeAsistencias.setText("7")
                }
                "Soriana Tienda 418 Consulado" -> {
                    txtPorcentajeAsistencias.setText("6")
                }
                "Soriana Tienda 686 Vía Vallejo" -> {
                    txtPorcentajeAsistencias.setText("8")
                }
                "Soriana Tienda 252 Miyana Polanco" -> {
                    txtPorcentajeAsistencias.setText("16")
                }
                "Soriana Tienda 356 Los Angeles" -> {
                    txtPorcentajeAsistencias.setText("5")
                }
                "Soriana Tienda 349 Montes Berneses" -> {
                    txtPorcentajeAsistencias.setText("5")
                }
                "Soriana Tienda 345 Vista Hermosa" -> {
                    txtPorcentajeAsistencias.setText("3")
                }
                "Soriana Tienda 341 Fundidora" -> {
                    txtPorcentajeAsistencias.setText("3")
                }
                "Soriana Tienda 20 Terraza Coapa" -> {
                    txtPorcentajeAsistencias.setText("18")
                }
                "Comercial Mexicana Tienda 019 Mixcoac" -> {
                    txtPorcentajeAsistencias.setText("26")
                }
                "Comercial Mexicana Tienda Pilares" -> {
                    txtPorcentajeAsistencias.setText("19")
                }
                "Comercial Mexicana Tienda  División del Norte" -> {
                    txtPorcentajeAsistencias.setText("5")
                }
            }
        }
    }

    override fun onClick(v: View?) {
        when(v) {
            imgEvidencia -> capture()
            imgEvidencia2 -> capture_2()
            imgEvidencia3 -> capture_3()
            imgEvidencia4 -> capture_4()
            imgEvidencia5 -> capture_5()
           /* imgEvidencia6 -> capture_6()
            imgEvidencia7 -> capture_7()
            imgEvidencia8 -> capture_8()
            imgEvidencia9 -> capture_9()*/
            btnClean -> cleanSignature()
            //btnEvaluacion -> onCustomClick()
            //btnSaveSignature -> saveSignatures()
            //btnCreatePdf -> createPDFClick()
            /*btnSaveToSQLite -> {
                onCustomClick()
                saveInformation()
                createPDFClick()
                //saveSignatures()
                //sendImageToDb()
            }*/
            //btnGetInformation -> getInformation()
            //btnUpdateInformation -> updateInformation()
            btnGoToSpinners -> goToSpiners()
            //fabVerListaReportes -> goListReports()
        }
    }

    /*override fun onCheckedChanged(group: RadioGroup?, checkedId: Int) {
        when(group) {
            radioButton -> onCustomClick()
        }
    }*/

    /**---------------------------------------------------------- METODO QUE REALIZA LA EVALUACION --------------------------------------------------------------------------------------------------------------------*/

    @SuppressLint("SetTextI18n")
    fun onCustomClick() {

        val number1 = 1
        val number2 = 0
        var sum = 0
        var desempeño = arrayOf(
            if (rbDesempeño1.isChecked) number1 else number2, if (rbDesempeño2.isChecked) number1 else number2, if (rbDesempeño3.isChecked) number1 else number2, if (rbDesempeño4.isChecked) number1 else number2,
            if (rbDesempeño5.isChecked) number1 else number2, if (rbDesempeño6.isChecked) number1 else number2, if (rbDesempeño7.isChecked) number1 else number2, if (rbDesempeño8.isChecked) number1 else number2,
            if (rbDesempeño9.isChecked) number1 else number2, if (rbDesempeño10.isChecked) number1 else number2, if (rbDesempeño11.isChecked) number1 else number2, if (rbDesempeño12.isChecked) number1 else number2,
            if (rbDesempeño13.isChecked) number1 else number2, if (rbDesempeño14.isChecked) number1 else number2, if (rbDesempeño15.isChecked) number1 else number2, if (rbDesempeño16.isChecked) number1 else number2,
            if (rbDesempeño17.isChecked) number1 else number2, if (rbDesempeño18.isChecked) number1 else number2, if (rbDesempeño19.isChecked) number1 else number2, if (rbDesempeño20.isChecked) number1 else number2,
            if (rbDesempeño21.isChecked) number1 else number2, if (rbDesempeño22.isChecked) number1 else number2, if (rbDesempeño23.isChecked) number1 else number2, if (rbDesempeño24.isChecked) number1 else number2)

        for (num in desempeño) sum += num

        when {
            sum <= 17 -> {
                txtEvaluacion.setText("75%")
            }
            sum in 18..19 -> {
                txtEvaluacion.setText("80%")
            }
            sum in 20..24 -> {
                txtEvaluacion.setText("100%")
            }
        }
        Toast.makeText(activity?.baseContext, "Su evaluación es: ${txtEvaluacion.text.toString()}", Toast.LENGTH_SHORT).show()
    }

    /*---------------------------------- FOTOS DE EVIDENCIA --------------------------------------*/

    private fun capture() {
        if (activity?.packageManager!!.hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            sendTakePictureIntent()
        }
    }

   private fun capture_2() {
        if (activity?.packageManager!!.hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            sendTakePictureIntent_2()
        }
    }

    private fun capture_3() {
        if (activity?.packageManager!!.hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            sendTakePictureIntent_3()
        }
    }

    private fun capture_4() {
        if (activity?.packageManager!!.hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            sendTakePictureIntent_4()
        }
    }

    private fun capture_5() {
        if (activity?.packageManager!!.hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            sendTakePictureIntent_5()
        }
    }

   /* private fun capture_6() {
        if (activity?.packageManager!!.hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            sendTakePictureIntent_6()
        }
    }

    private fun capture_7() {
        if (activity?.packageManager!!.hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            sendTakePictureIntent_7()
        }
    }

    private fun capture_8() {
        if (activity?.packageManager!!.hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            sendTakePictureIntent_8()
        }
    }

    private fun capture_9() {
        if (activity?.packageManager!!.hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            sendTakePictureIntent_9()
        }
    }*/

    private fun sendTakePictureIntent() {
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        cameraIntent.putExtra(MediaStore.EXTRA_FINISH_ON_COMPLETION, true)

        if (cameraIntent.resolveActivity(activity?.packageManager!!) != null) {
            try {
                file = getPictureFile()
            } catch (ex: IOException) {
                displayMessage(activity?.baseContext!!, "El archivo de foto no puede ser creada, por favor intentelo de nuevo")
                return
            }

            if (file != null) {
                val photoUri = FileProvider.getUriForFile(activity?.applicationContext!!, "com.example.sergioruiz.fdsi.fileprovider", file!!)
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri)
                startActivityForResult(cameraIntent, REQUEST_PICTURE_CAPTURE!!)
            }
        }
    }

    private fun sendTakePictureIntent_2() {
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        cameraIntent.putExtra(MediaStore.EXTRA_FINISH_ON_COMPLETION, true)

        if (cameraIntent.resolveActivity(activity?.packageManager!!) != null) {
            try {
                file2 = getPictureFile()
            } catch (ex: IOException) {
                displayMessage(activity?.baseContext!!, "El archivo de foto no puede ser creada, por favor intentelo de nuevo")
                return
            }

            if (file2 != null) {
                val photoUri = FileProvider.getUriForFile(activity?.applicationContext!!, "com.example.sergioruiz.fdsi.fileprovider", file2!!)
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri)
                startActivityForResult(cameraIntent, REQUEST_PICTURE_CAPTURE_2!!)
            }
        }
    }

    private fun sendTakePictureIntent_3() {
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        cameraIntent.putExtra(MediaStore.EXTRA_FINISH_ON_COMPLETION, true)

        if (cameraIntent.resolveActivity(activity?.packageManager!!) != null) {
            try {
                file3 = getPictureFile()
            } catch (ex: IOException) {
                displayMessage(activity?.baseContext!!, "El archivo de foto no puede ser creada, por favor intentelo de nuevo")
                return
            }

            if (file3 != null) {
                val photoUri = FileProvider.getUriForFile(activity?.applicationContext!!, "com.example.sergioruiz.fdsi.fileprovider", file3!!)
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri)
                startActivityForResult(cameraIntent, REQUEST_PICTURE_CAPTURE_3!!)
            }
        }
    }

    private fun sendTakePictureIntent_4() {
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        cameraIntent.putExtra(MediaStore.EXTRA_FINISH_ON_COMPLETION, true)

        if (cameraIntent.resolveActivity(activity?.packageManager!!) != null) {
            try {
                file4 = getPictureFile()
            } catch (ex: IOException) {
                displayMessage(activity?.baseContext!!, "El archivo de foto no puede ser creada, por favor intentelo de nuevo")
                return
            }

            if (file4 != null) {
                val photoUri = FileProvider.getUriForFile(activity?.applicationContext!!, "com.example.sergioruiz.fdsi.fileprovider", file4!!)
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri)
                startActivityForResult(cameraIntent, REQUEST_PICTURE_CAPTURE_4!!)
            }
        }
    }

    private fun sendTakePictureIntent_5() {
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        cameraIntent.putExtra(MediaStore.EXTRA_FINISH_ON_COMPLETION, true)

        if (cameraIntent.resolveActivity(activity?.packageManager!!) != null) {
            try {
                file5 = getPictureFile()
            } catch (ex: IOException) {
                displayMessage(activity?.baseContext!!, "El archivo de foto no puede ser creada, por favor intentelo de nuevo")
                return
            }

            if (file5 != null) {
                val photoUri = FileProvider.getUriForFile(activity?.applicationContext!!, "com.example.sergioruiz.fdsi.fileprovider", file5!!)
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri)
                startActivityForResult(cameraIntent, REQUEST_PICTURE_CAPTURE_5!!)
            }
        }
    }

   /* private fun sendTakePictureIntent_6() {
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        cameraIntent.putExtra(MediaStore.EXTRA_FINISH_ON_COMPLETION, true)

        if (cameraIntent.resolveActivity(activity?.packageManager!!) != null) {
            try {
                file6 = getPictureFile()
            } catch (ex: IOException) {
                displayMessage(activity?.baseContext!!, "El archivo de foto no puede ser creada, por favor intentelo de nuevo")
                return
            }

            if (file6 != null) {
                val photoUri = FileProvider.getUriForFile(activity?.applicationContext!!, "com.example.sergioruiz.fdsi.fileprovider", file6!!)
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri)
                startActivityForResult(cameraIntent, REQUEST_PICTURE_CAPTURE_6!!)
            }
        }
    }

    private fun sendTakePictureIntent_7() {
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        cameraIntent.putExtra(MediaStore.EXTRA_FINISH_ON_COMPLETION, true)

        if (cameraIntent.resolveActivity(activity?.packageManager!!) != null) {
            try {
                file7 = getPictureFile()
            } catch (ex: IOException) {
                displayMessage(activity?.baseContext!!, "El archivo de foto no puede ser creada, por favor intentelo de nuevo")
                return
            }

            if (file7 != null) {
                val photoUri = FileProvider.getUriForFile(activity?.applicationContext!!, "com.example.sergioruiz.fdsi.fileprovider", file7!!)
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri)
                startActivityForResult(cameraIntent, REQUEST_PICTURE_CAPTURE_7!!)
            }
        }
    }

    private fun sendTakePictureIntent_8() {
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        cameraIntent.putExtra(MediaStore.EXTRA_FINISH_ON_COMPLETION, true)

        if (cameraIntent.resolveActivity(activity?.packageManager!!) != null) {
            try {
                file8 = getPictureFile()
            } catch (ex: IOException) {
                displayMessage(activity?.baseContext!!, "El archivo de foto no puede ser creada, por favor intentelo de nuevo")
                return
            }

            if (file8 != null) {
                val photoUri = FileProvider.getUriForFile(activity?.applicationContext!!, "com.example.sergioruiz.fdsi.fileprovider", file8!!)
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri)
                startActivityForResult(cameraIntent, REQUEST_PICTURE_CAPTURE_8!!)
            }
        }
    }

    private fun sendTakePictureIntent_9() {
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        cameraIntent.putExtra(MediaStore.EXTRA_FINISH_ON_COMPLETION, true)

        if (cameraIntent.resolveActivity(activity?.packageManager!!) != null) {
            try {
                file9 = getPictureFile()
            } catch (ex: IOException) {
                displayMessage(activity?.baseContext!!, "El archivo de foto no puede ser creada, por favor intentelo de nuevo")
                return
            }

            if (file9 != null) {
                val photoUri = FileProvider.getUriForFile(activity?.applicationContext!!, "com.example.sergioruiz.fdsi.fileprovider", file9!!)
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri)
                startActivityForResult(cameraIntent, REQUEST_PICTURE_CAPTURE_9!!)
            }
        }
    }*/

    @SuppressLint("SimpleDateFormat")
    @Throws(IOException::class)
    private fun getPictureFile(): File? {
        val timeStamp = SimpleDateFormat("yyyyMMddHHmmss").format(Date())
        val pictureFile = "IMG_${txtPlantilla.text}"
        //val storageDir = activity?.getExternalFilesDir(Environment.DIRECTORY_PICTURES)!!
        val storageDir = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), directoryName)
        if (!storageDir.exists()) {
            if (!storageDir.mkdirs()) {
                Log.d("FDSI", "failed to create directory")
                return null
            }
        }
        image = File.createTempFile(pictureFile, ".jpg", storageDir)
        currentPhotoPath = image?.absolutePath
        return image
    }

    private fun addToGallery() {
        val galleryIntent = Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE)
        val f = File(currentPhotoPath!!)
        val picUri = Uri.fromFile(f)
        galleryIntent.data = picUri
        activity?.sendBroadcast(galleryIntent)
        displayMessage(activity?.baseContext!!, "Se ha guardado la foto exitosamente")
    }

    @SuppressLint("CommitPrefEdits")
    @Synchronized
    protected fun getInstallationIndentifier(): String? {
        if (deviceIdentifier == null) {
            val sharedPref = activity?.getSharedPreferences("DEVICE_ID", Context.MODE_PRIVATE)
            deviceIdentifier = sharedPref?.getString("DEVICE_ID", null)
            if (deviceIdentifier == null) {
                deviceIdentifier = UUID.randomUUID().toString()
                val editor = sharedPref?.edit()
                editor?.putString("DEVICE_ID", deviceIdentifier)
                editor?.apply()
            }
        }
        return deviceIdentifier
    }

    private fun displayMessage(context: Context, message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when(requestCode) {
            REQUEST_PICTURE_CAPTURE -> {
                val imgFile = File(currentPhotoPath!!)
                if (imgFile.exists()) {
                    imgEvidencia.setImageDrawable(null)
                    imgEvidencia.setImageURI(Uri.fromFile(imgFile))
                }
                imageFile1 = imgFile.name
                addToGallery()
            }
            REQUEST_PICTURE_CAPTURE_2 -> {
                val imgFile = File(currentPhotoPath!!)
                if (imgFile.exists()) {
                    imgEvidencia2.setImageDrawable(null)
                    imgEvidencia2.setImageURI(Uri.fromFile(imgFile))
                }
                imageFile2 = imgFile.name
                addToGallery()
            }
            REQUEST_PICTURE_CAPTURE_3 -> {
                val imgFile = File(currentPhotoPath!!)
                if (imgFile.exists()) {
                    imgEvidencia3.setImageDrawable(null)
                    imgEvidencia3.setImageURI(Uri.fromFile(imgFile))
                }
                imageFile3 = imgFile.name
                addToGallery()
            }
            REQUEST_PICTURE_CAPTURE_4 -> {
                val imgFile = File(currentPhotoPath!!)
                if (imgFile.exists()) {
                    imgEvidencia4.setImageDrawable(null)
                    imgEvidencia4.setImageURI(Uri.fromFile(imgFile))
                }
                imageFile4 = imgFile.name
                addToGallery()
            }
            REQUEST_PICTURE_CAPTURE_5 -> {
                val imgFile = File(currentPhotoPath!!)
                if (imgFile.exists()) {
                    imgEvidencia5.setImageDrawable(null)
                    imgEvidencia5.setImageURI(Uri.fromFile(imgFile))
                }
                imageFile5 = imgFile.name
                addToGallery()
            }
          /*  REQUEST_PICTURE_CAPTURE_6 -> {
                val imgFile = File(currentPhotoPath!!)
                if (imgFile.exists()) {
                    imgEvidencia6.setImageDrawable(null)
                    imgEvidencia6.setImageURI(Uri.fromFile(imgFile))
                }
                addToGallery()
            }
            REQUEST_PICTURE_CAPTURE_7 -> {
                val imgFile = File(currentPhotoPath!!)
                if (imgFile.exists()) {
                    imgEvidencia7.setImageDrawable(null)
                    imgEvidencia7.setImageURI(Uri.fromFile(imgFile))
                }
                addToGallery()
            }
            REQUEST_PICTURE_CAPTURE_8 -> {
                val imgFile = File(currentPhotoPath!!)
                if (imgFile.exists()) {
                    imgEvidencia8.setImageDrawable(null)
                    imgEvidencia8.setImageURI(Uri.fromFile(imgFile))
                }
                addToGallery()
            }
            REQUEST_PICTURE_CAPTURE_9 -> {
                val imgFile = File(currentPhotoPath!!)
                if (imgFile.exists()) {
                    imgEvidencia9.setImageDrawable(null)
                    imgEvidencia9.setImageURI(Uri.fromFile(imgFile))
                }
                addToGallery()
            }*/
        }
        /**if (requestCode == REQUEST_PICTURE_CAPTURE && resultCode == Activity.RESULT_OK) {
            val imgFile = File(currentPhotoPath!!)
            if (imgFile.exists()) {
                imgEvidencia.setImageDrawable(null)
                imgEvidencia.setImageURI(Uri.fromFile(imgFile))
            }
            addToGallery()
        }*/
    }

    /*--------------------------------------- FIRMA DIGITAL -------------------------------*/

    override fun onStartSigning() {
        //Toast.makeText(activity, "Ingrese su firma", Toast.LENGTH_SHORT).show()
    }

    override fun onClear() {
        btnClean.isEnabled = false
        //btnSaveSignature.isEnabled = false
    }

    override fun onSigned() {
        btnClean.isEnabled = true
        //btnSaveSignature.isEnabled = true
    }

    fun cleanSignature() {
        signature_pad.clear()
        etNameSignature.text.clear()
    }

    fun saveSignatures() {
        val signatureBitmap = signature_pad.signatureBitmap
        val signatureSvgs = signature_pad.signatureSvg
        if (addJPGSignatureToGallery(signatureBitmap)!!) {
            Toast.makeText(activity, "Firma guardada dentro de la galeria", Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(activity, "No se puede guardar la firma", Toast.LENGTH_SHORT).show()
        }

        if (addSvgSignatureToGallery(signatureSvgs)!!) {
            Toast.makeText(activity, "La firma SVG se guardo dentro de la galeria", Toast.LENGTH_SHORT).show()
        } else {
            //Toast.makeText(activity, "No se puede guardar la firma SVG", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        //super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when(requestCode) {
            1 -> {
                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(activity, "No se puede escribir en una galeria externa", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    fun getAlbumStorageDir(albumName: String): File? {
        val file = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), albumName)
        if (!file.mkdirs()) {
            Log.e("SignaturePad", "Directorio no creado")
        }
        return file
    }

    @Throws(IOException::class)
    private fun saveBitmapToJPG(bitmap: Bitmap?, photo: File?) {
        val newBitmap = Bitmap.createBitmap(bitmap!!.width, bitmap.height, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(newBitmap)
        canvas.drawColor(Color.parseColor("#FFFFFF"))
        canvas.drawBitmap(bitmap, 0f, 0f, null)
        val stream = FileOutputStream(photo)
        newBitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream)
        stream.close()
    }

    fun addJPGSignatureToGallery(signature: Bitmap): Boolean? {
        var result = false
        try {
            val nombreFirma = etNameSignature.text.toString()
            //val photo = File(getAlbumStorageDir("FirmasFDSI"), String.format("Firma_%d.jpg", System.currentTimeMillis()))
            val photo = File(getAlbumStorageDir("FirmasFDSI"), String.format("Firma_${nombreFirma}.jpg", System.currentTimeMillis()))
            saveBitmapToJPG(signature, photo)
            //postImage("Firma_${nombreFirma}.jpg", "FirmasFDSI")
            scanMediaFile(photo)
            result = true
        }catch (e: IOException) {
            e.printStackTrace()
        }
        return result
    }

    private fun scanMediaFile(photo: File?) {
        val mediaScanIntent = Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE)
        val contentUri = Uri.fromFile(photo)
        mediaScanIntent.setData(contentUri)
        activity?.sendBroadcast(mediaScanIntent)
    }

    fun addSvgSignatureToGallery(signatureSvg: String): Boolean? {
        var result = false
        try {
            val nombreFirma = etNameSignature.text.toString()
            val svgFile = File("Firmas FDSI", String.format("Firma_${nombreFirma}.svg", System.currentTimeMillis()))
            val stream = FileOutputStream(svgFile)
            val writer = OutputStreamWriter(stream)
            writer.write(signatureSvg)
            writer.close()
            stream.flush()
            stream.close()
            scanMediaFile(svgFile)
            result = true
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return result
    }

    /**--------------------------------------- Guardar, consultar y actualizar informacion de SQLite -------------------------------*/

    fun saveInformation() {
        val db = SQLiteClass(activity?.applicationContext)
        var id = db.agregar(txtEstablishment.text.toString(),
            txtPlantilla.text.toString(),
            txtAsistencias.text.toString(),
            txtPorcentajeAsistencias.text.toString(),
            txtFechaElaboracion.text.toString(),
            txtFaltasdMes.text.toString(),
            txtEvaluacion.text.toString(),
            txvPersonal_Uniformado.text.toString(),
            rbDesempeño1.isChecked.toString(),
            //rbOportunidad1.isChecked.toString(),
            //rbCorregir1.isChecked.toString(),
            txvMaterial_Completo.text.toString(),
            rbDesempeño2.isChecked.toString(),
            //rbOportunidad2.isChecked.toString(),
            //rbCorregir2.isChecked.toString(),
            txvEquipoCompleto.text.toString(),
            rbDesempeño3.isChecked.toString(),
            //rbOportunidad3.isChecked.toString(),
            //rbCorregir3.isChecked.toString(),
            txvPasilloIntExtCajas.text.toString(),
            rbDesempeño4.isChecked.toString(),
            //rbOportunidad4.isChecked.toString(),
            //rbCorregir4.isChecked.toString(),
            txvFrutasVerduras.text.toString(),
            rbDesempeño5.isChecked.toString(),
            //rbOportunidad5.isChecked.toString(),
            //rbCorregir5.isChecked.toString(),
            txvAreaAlimentosComensales.text.toString(),
            rbDesempeño6.isChecked.toString(),
            //rbOportunidad6.isChecked.toString(),
            //rbCorregir6.isChecked.toString(),
            txvCongeladosLacteos.text.toString(),
            rbDesempeño7.isChecked.toString(),

            txvVinosLicores.text.toString(),
            rbDesempeño8.isChecked.toString(),

            txvDeportes.text.toString(),
            rbDesempeño9.isChecked.toString(),

            txvJugueteria.text.toString(),
            rbDesempeño10.isChecked.toString(),

            txvMueblesElectronica.text.toString(),
            rbDesempeño11.isChecked.toString(),

            txvFerreteriayJardineria.text.toString(),
            rbDesempeño12.isChecked.toString(),

            txvRopa.text.toString(),
            rbDesempeño13.isChecked.toString(),

            txvLinea_Blanca.text.toString(),
            rbDesempeño14.isChecked.toString(),

            txvFarmaciaPerfumeria.text.toString(),
            rbDesempeño15.isChecked.toString(),

            txvPasillosPrincipales.text.toString(),
            rbDesempeño16.isChecked.toString(),

            txvAbarrotes.text.toString(),
            rbDesempeño17.isChecked.toString(),

            txvBaños_de_Clientes.text.toString(),
            rbDesempeño18.isChecked.toString(),

            txvBaños_de_Oficina.text.toString(),
            rbDesempeño19.isChecked.toString(),

            txvRetiro_de_Basura_General.text.toString(),
            rbDesempeño20.isChecked.toString(),

            txvMopeado_de_Pisos.text.toString(),
            rbDesempeño21.isChecked.toString(),

            txvDesmanchado_de_Piso.text.toString(),
            rbDesempeño22.isChecked.toString(),

            txvLimpieza_de_OficinasComedor.text.toString(),
            rbDesempeño23.isChecked.toString(),

            txvDesorillados.text.toString(),
            rbDesempeño24.isChecked.toString()

            /*txvMATENIMIENTO_DE_ALFOMBRAS.text.toString(),
            rbDesempeño25.isChecked.toString(),

            txvCOMEDOR.text.toString(),
            rbDesempeño26.isChecked.toString()*/)
        Log.e("SQLITE", id.toString())
        Toast.makeText(activity?.baseContext, "Se ha guardado la informacion en la db", Toast.LENGTH_SHORT).show()
        //db.obtener(1)
    }

    fun getInformation() {
        val sql = SQLiteClass(activity)
        val database = sql.readableDatabase
        val bundle2 = arguments?.getBundle("bundle2")
        if (bundle2 != null) {
            val cliente = arguments?.getBundle("bundle2")?.getString("cliente")
            val fecha = arguments?.getBundle("bundle2")?.getString("fecha")
            val sucursal = arguments?.getBundle("bundle2")?.getString("sucursal")
            txtEstablishment.setText(cliente)
            txtFaltasdMes.setText(fecha)
            txtPlantilla.setText(sucursal)
        }
        val cursor = sql.obtener(txtFaltasdMes.text.toString(), txtEstablishment.text.toString(),database)
        Log.e("COUNT", cursor.count.toString())
        if (cursor.count > 0) {
            txtEstablishment.setText(cursor.getString(1))
            txtPlantilla.setText(cursor.getString(2))
            txtAsistencias.setText(cursor.getString(3))
            txtPorcentajeAsistencias.setText(cursor.getString(4))
            txtFechaElaboracion.setText(cursor.getString(5))
            txtFaltasdMes.setText(cursor.getString(6))
            txtEvaluacion.setText(cursor.getString(7))
            rbDesempeño1.isChecked = cursor.getString(9)!!.toBoolean()
            //rbOportunidad1.isChecked = cursor.getString(9)!!.toBoolean()
            //rbCorregir1.isChecked = cursor.getString(10)!!.toBoolean()
            rbDesempeño2.isChecked = cursor.getString(11)!!.toBoolean()
            //rbOportunidad2.isChecked = cursor.getString(12)!!.toBoolean()
            //rbCorregir2.isChecked = cursor.getString(13)!!.toBoolean()
            rbDesempeño3.isChecked = cursor.getString(13)!!.toBoolean()
            //rbOportunidad3.isChecked = cursor.getString(15)!!.toBoolean()
            //rbCorregir3.isChecked = cursor.getString(16)!!.toBoolean()
            rbDesempeño4.isChecked = cursor.getString(15)!!.toBoolean()
            //rbOportunidad4.isChecked = cursor.getString(18)!!.toBoolean()
            //rbCorregir4.isChecked = cursor.getString(19)!!.toBoolean()
            rbDesempeño5.isChecked = cursor.getString(17)!!.toBoolean()
            //rbOportunidad5.isChecked = cursor.getString(21)!!.toBoolean()
            //rbCorregir5.isChecked = cursor.getString(22)!!.toBoolean()
            rbDesempeño6.isChecked = cursor.getString(19)!!.toBoolean()
            //rbOportunidad6.isChecked = cursor.getString(24)!!.toBoolean()
            //rbCorregir6.isChecked = cursor.getString(25)!!.toBoolean()
            rbDesempeño7.isChecked = cursor.getString(21)!!.toBoolean()
            //rbOportunidad7.isChecked = cursor.getString(27)!!.toBoolean()
            //rbCorregir7.isChecked = cursor.getString(28)!!.toBoolean()
            rbDesempeño8.isChecked = cursor.getString(23)!!.toBoolean()
            rbDesempeño9.isChecked = cursor.getString(25)!!.toBoolean()
            rbDesempeño10.isChecked = cursor.getString(27)!!.toBoolean()
            rbDesempeño11.isChecked = cursor.getString(29)!!.toBoolean()
            rbDesempeño12.isChecked = cursor.getString(31)!!.toBoolean()
            rbDesempeño13.isChecked = cursor.getString(33)!!.toBoolean()
            rbDesempeño14.isChecked = cursor.getString(35)!!.toBoolean()
            rbDesempeño15.isChecked = cursor.getString(37)!!.toBoolean()
            rbDesempeño16.isChecked = cursor.getString(39)!!.toBoolean()
            rbDesempeño17.isChecked = cursor.getString(41)!!.toBoolean()
            rbDesempeño18.isChecked = cursor.getString(43)!!.toBoolean()
            rbDesempeño19.isChecked = cursor.getString(45)!!.toBoolean()
            rbDesempeño20.isChecked = cursor.getString(47)!!.toBoolean()
            rbDesempeño21.isChecked = cursor.getString(49)!!.toBoolean()
            rbDesempeño22.isChecked = cursor.getString(51)!!.toBoolean()
            rbDesempeño23.isChecked = cursor.getString(53)!!.toBoolean()
            rbDesempeño24.isChecked = cursor.getString(55)!!.toBoolean()
        } else {
            //Toast.makeText(activity?.applicationContext!!, "No hay datos", Toast.LENGTH_SHORT).show()
        }
        /*if (cursor.moveToFirst()) {

        }*/
    }

    fun updateInformation() {
        val db = SQLiteClass(activity)
        val update = db.actualizar(txtFaltasdMes.text.toString(),
            txtEvaluacion.text.toString(),
            rbDesempeño1.isChecked.toString(),
            rbDesempeño2.isChecked.toString(),
            rbDesempeño3.isChecked.toString(),
            rbDesempeño4.isChecked.toString(),
            rbDesempeño5.isChecked.toString(),
            rbDesempeño6.isChecked.toString(),
            rbDesempeño7.isChecked.toString(),
            rbDesempeño8.isChecked.toString(),
            rbDesempeño9.isChecked.toString(),
            rbDesempeño10.isChecked.toString(),
            rbDesempeño11.isChecked.toString(),
            rbDesempeño12.isChecked.toString(),
            rbDesempeño13.isChecked.toString(),
            rbDesempeño14.isChecked.toString(),
            rbDesempeño15.isChecked.toString(),
            rbDesempeño16.isChecked.toString(),
            rbDesempeño17.isChecked.toString(),
            rbDesempeño18.isChecked.toString(),
            rbDesempeño19.isChecked.toString(),
            rbDesempeño20.isChecked.toString(),
            rbDesempeño21.isChecked.toString(),
            rbDesempeño22.isChecked.toString(),
            rbDesempeño23.isChecked.toString(),
            rbDesempeño24.isChecked.toString())
        //rbDesempeño25.isChecked.toString(),
        //rbDesempeño26.isChecked.toString())
        Log.e("SQLITE", update.toString())
        Toast.makeText(activity?.baseContext!!, "Se ha actualizado la informacion", Toast.LENGTH_SHORT).show()
    }

    /**---------------------------- CREACION DEL PDF ----------------------------------------*/

    private fun createPDFClick() {
        val token = Binder.clearCallingIdentity()
        try {
            val personName = txtEstablishment.text.toString()
            pdfFile = File(getAlbumStorageDirPdf(filePath), "Archivo_${txtEstablishment.text.toString()}.pdf")
            generatePDF(personName)
        } finally {
            Binder.restoreCallingIdentity(token)
        }

    }

    fun generatePDF(personName: String) {
        val document = Document()
        //val number1 = "1"
        //val number2 = ""
        try {
            val docWriter = PdfWriter.getInstance(document, FileOutputStream(pdfFile))
            document.open()

            val cb = docWriter.directContent
            initializeFonts()
            val inputStream = activity?.assets!!.open("fdsiimagen.png")
            val bitmap = BitmapFactory.decodeStream(inputStream)
            val stream = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream)
            val companyLogo = Image.getInstance(stream.toByteArray())
            companyLogo.setAbsolutePosition(25f, 770f)
            companyLogo.scalePercent(25f)
            document.add(companyLogo)
            createHeadings(cb, 450f, 815f, "EMPRESA CERTIFICADA")
            createHeadingsBasics(cb, 450f, 800f, "ISO   9001 - 2015")
            createHeadingsBasics(cb, 450f, 785f, "ISO 14001 - 2015")

            var inputStream2 = activity?.assets!!.open("barra.png")
            var bitmap2 = BitmapFactory.decodeStream(inputStream2)
            var stream2 = ByteArrayOutputStream()
            bitmap2.compress(Bitmap.CompressFormat.PNG, 100, stream2)
            var barra = Image.getInstance(stream2.toByteArray())
            barra.setAbsolutePosition(25f, 765f)
            barra.scalePercent(57f, 10f)
            document.add(barra)

            var barra2 = Image.getInstance(stream2.toByteArray())
            barra2.setAbsolutePosition(25f, 65f)
            barra2.scalePercent(57f, 10f)
            document.add(barra2)


            var inputStream3 = activity?.assets!!.open("lpimg.png")
            var bitmap3 = BitmapFactory.decodeStream(inputStream3)
            var stream3 = ByteArrayOutputStream()
            bitmap3.compress(Bitmap.CompressFormat.PNG, 100, stream3)
            var imgact = Image.getInstance(stream3.toByteArray())
            imgact.setAbsolutePosition(25f, 10f)
            imgact.scalePercent(40f)
            document.add(imgact)

            var inputStream4 = activity?.assets!!.open("dbimg.png")
            var bitmap4 = BitmapFactory.decodeStream(inputStream4)
            var stream4 = ByteArrayOutputStream()
            bitmap4.compress(Bitmap.CompressFormat.PNG, 100, stream4)
            var imgact2 = Image.getInstance(stream4.toByteArray())
            imgact2.setAbsolutePosition(400f, 13f)
            imgact2.scalePercent(40f)
            document.add(imgact2)

            var inputStream5 = activity?.assets!!.open("jfimg.png")
            var bitmap5 = BitmapFactory.decodeStream(inputStream5)
            var stream5 = ByteArrayOutputStream()
            bitmap5.compress(Bitmap.CompressFormat.PNG, 100, stream5)
            var imgact3 = Image.getInstance(stream5.toByteArray())
            imgact3.setAbsolutePosition(205f, 13f)
            imgact3.scalePercent(40f)
            document.add(imgact3)

            createHeadingsBasics(cb, 165f, 750f, "INFORME DE EVALUACIÓN DINÁMICA")
            createHeadingsinf(cb, 50f, 730f, "Cliente: ${txtEstablishment.text}")
            createHeadingsinf(cb, 50f, 710f, "Sucursal: ${txtPlantilla.text}" )
            createHeadingsinf(cb, 50f, 690f, "Localidad: ${txtAsistencias.text}")
            createHeadingsinf(cb, 50f, 670f, "Calificación actual: ${txtEvaluacion.text}")

            createHeadingsinf(cb, 370f, 730f, "Personal Contratado: ${txtPorcentajeAsistencias.text}")
            createHeadingsinf(cb, 370f, 710f, "Responsable: ${txtFechaElaboracion.text}")
            createHeadingsinf(cb, 370f, 690f, "Firma:")
            createHeadingsinf(cb, 370f, 670f, "Fecha de Elaboración: ${txtFaltasdMes.text}")

            val signatureBitmap = signature_pad.signatureBitmap
            var streamFirma = ByteArrayOutputStream()
            signatureBitmap.compress(Bitmap.CompressFormat.JPEG, 100, streamFirma)
            var imgFirma = Image.getInstance(streamFirma.toByteArray())
            imgFirma.setAbsolutePosition(400f, 680f)
            imgFirma.scalePercent(10f, 5f)
            document.add(imgFirma)

            createHeadingsinf(cb, 25f, 55f, "Calz. de Camarones No. 695 Of. 1, Col. El Recreo")
            createHeadingsinf(cb, 25f, 40f, "Alcaldia Azcapotzalco, C.P. 02070, CDMX")
            createHeadingsinf(cb, 315f, 40f, "Tel:(55)26143757")
            createHeadingsinf(cb, 490f, 40f, "www.fdsi.com.mx")
            createHeadingsinf(cb, 50f, 320f, "Comentarios:")
            createHeadingsinf(cb, 50f, 250f, "Imagenes")



            val columnWidths = floatArrayOf(3f, 2f, 2f, 2f)
            val table = PdfPTable(columnWidths)
            table.totalWidth = 525f

            val bold = Font(Font.FontFamily.HELVETICA, 7f ,Font.BOLD)
            bold.color = BaseColor(Color.WHITE)

            var cell = PdfPCell(Phrase("AREA", bold))
            cell.horizontalAlignment = Element.ALIGN_CENTER
            cell.backgroundColor = BaseColor(Color.parseColor(getString(R.string.colorBlueLogo)))
            table.addCell(cell)

            cell = PdfPCell(Phrase("DESEMPEÑO OPTIMO", bold))
            cell.horizontalAlignment = Element.ALIGN_CENTER
            cell.backgroundColor = BaseColor(Color.parseColor(getString(R.string.colorBlueLogo)))
            table.addCell(cell)

            cell = PdfPCell(Phrase("OPORTUNIDAD DE MEJORA", bold))
            cell.horizontalAlignment = Element.ALIGN_CENTER
            cell.backgroundColor = BaseColor(Color.parseColor(getString(R.string.colorBlueLogo)))
            table.addCell(cell)

            cell = PdfPCell(Phrase("POR CORREGIR", bold))
            cell.horizontalAlignment = Element.ALIGN_CENTER
            cell.backgroundColor = BaseColor(Color.parseColor(getString(R.string.colorBlueLogo)))
            table.addCell(cell)
            table.headerRows = 1

            val table2 = PdfPTable(1)
            table2.totalWidth = 525f

            var cell2 = PdfPCell(Phrase(et_comentarios.text.toString()))
            cell2.horizontalAlignment = Element.ALIGN_LEFT
            cell2.fixedHeight = 50f
            table2.addCell(cell2)

            var inputStream6 = activity?.assets!!.open("pal.png")
            var bitmap6 = BitmapFactory.decodeStream(inputStream6)
            var stream6 = ByteArrayOutputStream()
            bitmap6.compress(Bitmap.CompressFormat.PNG, 100, stream6)
            var number1 = Image.getInstance(stream6.toByteArray())
            number1.scalePercent(20f, 20f)


            var inputStream7 = activity?.assets!!.open("vac.png")
            var bitmap7 = BitmapFactory.decodeStream(inputStream7)
            var stream7 = ByteArrayOutputStream()
            bitmap7.compress(Bitmap.CompressFormat.PNG, 100, stream7)
            var number2 = Image.getInstance(stream7.toByteArray())
            number2.scalePercent(30f, 30f)

            var arrayQuestion = arrayOf("PERSONAL UNIFORMADO","MATERIAL COMPLETO", "EQUIPO COMPLETO",
                    "PASILLO EXTERIOR Y EXTERIOR DE CAJAS", "FRUTAS Y VERDURAS", "AREA DE ALIMENTOS, COMENSALES",
                    "CONGELADOS Y LACTEOS", "VINOS Y LICORES","DEPORTES", "JUGUETERÍA", "MUEBLES Y ELECTRONICA",
                    "FERRETERIA Y JARDINERÍA", "ROPA", "LINEA BLANCA", "FARMACÍA Y PERFUMERIA", "PASILLOS PRINCIPALES",
                    "ABARROTES", "BAÑOS DE CLIENTES", "BAÑOS DE OFICINA", "RETIRO DE BASURA GENERAL", "MOPEADO DE PISOS",
                    "DESMANCHADO DE PISO", "LIMPIEZA DE OFICINAS Y COMEDOR", "DESORILLADOS")

            var desempeño = arrayOf(
                if (rbDesempeño1.isChecked) number1 else number2, if (rbDesempeño2.isChecked) number1 else number2, if (rbDesempeño3.isChecked) number1 else number2, if (rbDesempeño4.isChecked) number1 else number2,
                if (rbDesempeño5.isChecked) number1 else number2, if (rbDesempeño6.isChecked) number1 else number2, if (rbDesempeño7.isChecked) number1 else number2, if (rbDesempeño8.isChecked) number1 else number2,
                if (rbDesempeño9.isChecked) number1 else number2, if (rbDesempeño10.isChecked) number1 else number2, if (rbDesempeño11.isChecked) number1 else number2, if (rbDesempeño12.isChecked) number1 else number2,
                if (rbDesempeño13.isChecked) number1 else number2, if (rbDesempeño14.isChecked) number1 else number2, if (rbDesempeño15.isChecked) number1 else number2, if (rbDesempeño16.isChecked) number1 else number2,
                if (rbDesempeño17.isChecked) number1 else number2, if (rbDesempeño18.isChecked) number1 else number2, if (rbDesempeño19.isChecked) number1 else number2, if (rbDesempeño20.isChecked) number1 else number2,
                if (rbDesempeño21.isChecked) number1 else number2, if (rbDesempeño22.isChecked) number1 else number2, if (rbDesempeño23.isChecked) number1 else number2, if (rbDesempeño24.isChecked) number1 else number2)

                /*if (rbDesempeño25.isChecked) number1 else number2, if (rbDesempeño26.isChecked) number1 else number2, if (rbDesempeño27.isChecked) number1 else number2, if (rbDesempeño28.isChecked) number1 else number2,
                if (rbDesempeño29.isChecked) number1 else number2, if (rbDesempeño30.isChecked) number1 else number2, if (rbDesempeño31.isChecked) number1 else number2, if (rbDesempeño32.isChecked) number1 else number2,
                if (rbDesempeño33.isChecked) number1 else number2, if (rbDesempeño34.isChecked) number1 else number2, if (rbDesempeño35.isChecked) number1 else number2, if (rbDesempeño36.isChecked) number1 else number2,
                if (rbDesempeño37.isChecked) number1 else number2)*/

            var oportunidad = arrayOf(
                if (rbOportunidad1.isChecked) number1 else number2, if (rbOportunidad2.isChecked) number1 else number2, if (rbOportunidad3.isChecked) number1 else number2, if (rbOportunidad4.isChecked) number1 else number2,
                if (rbOportunidad5.isChecked) number1 else number2, if (rbOportunidad6.isChecked) number1 else number2, if (rbOportunidad7.isChecked) number1 else number2, if (rbOportunidad8.isChecked) number1 else number2,
                if (rbOportunidad9.isChecked) number1 else number2, if (rbOportunidad10.isChecked) number1 else number2, if (rbOportunidad11.isChecked) number1 else number2, if (rbOportunidad12.isChecked) number1 else number2,
                if (rbOportunidad13.isChecked) number1 else number2, if (rbOportunidad14.isChecked) number1 else number2, if (rbOportunidad15.isChecked) number1 else number2, if (rbOportunidad16.isChecked) number1 else number2,
                if (rbOportunidad17.isChecked) number1 else number2, if (rbOportunidad18.isChecked) number1 else number2, if (rbOportunidad19.isChecked) number1 else number2, if (rbOportunidad20.isChecked) number1 else number2,
                if (rbOportunidad21.isChecked) number1 else number2, if (rbOportunidad22.isChecked) number1 else number2, if (rbOportunidad23.isChecked) number1 else number2, if (rbOportunidad24.isChecked) number1 else number2)
                /*if (rbOportunidad25.isChecked) number1 else number2, if (rbOportunidad26.isChecked) number1 else number2, if (rbOportunidad27.isChecked) number1 else number2, if (rbOportunidad28.isChecked) number1 else number2,
                if (rbOportunidad29.isChecked) number1 else number2, if (rbOportunidad30.isChecked) number1 else number2, if (rbOportunidad31.isChecked) number1 else number2, if (rbOportunidad32.isChecked) number1 else number2,
                if (rbOportunidad33.isChecked) number1 else number2, if (rbOportunidad34.isChecked) number1 else number2, if (rbOportunidad35.isChecked) number1 else number2, if (rbOportunidad36.isChecked) number1 else number2,
                if (rbOportunidad37.isChecked) number1 else number2)*/

            var corregir = arrayOf(
                if (rbCorregir1.isChecked) number1 else number2, if (rbCorregir2.isChecked) number1 else number2, if (rbCorregir3.isChecked) number1 else number2, if (rbCorregir4.isChecked) number1 else number2,
                if (rbCorregir5.isChecked) number1 else number2, if (rbCorregir6.isChecked) number1 else number2, if (rbCorregir7.isChecked) number1 else number2, if (rbCorregir8.isChecked) number1 else number2,
                if (rbCorregir9.isChecked) number1 else number2, if (rbCorregir10.isChecked) number1 else number2, if (rbCorregir11.isChecked) number1 else number2, if (rbCorregir12.isChecked) number1 else number2,
                if (rbCorregir13.isChecked) number1 else number2, if (rbCorregir14.isChecked) number1 else number2, if (rbCorregir15.isChecked) number1 else number2, if (rbCorregir16.isChecked) number1 else number2,
                if (rbCorregir17.isChecked) number1 else number2, if (rbCorregir18.isChecked) number1 else number2, if (rbCorregir19.isChecked) number1 else number2, if (rbCorregir20.isChecked) number1 else number2,
                if (rbCorregir21.isChecked) number1 else number2, if (rbCorregir22.isChecked) number1 else number2, if (rbCorregir23.isChecked) number1 else number2, if (rbCorregir24.isChecked) number1 else number2)
               /* if (rbCorregir25.isChecked) number1 else number2, if (rbCorregir26.isChecked) number1 else number2, if (rbCorregir27.isChecked) number1 else number2, if (rbCorregir28.isChecked) number1 else number2,
                if (rbCorregir29.isChecked) number1 else number2, if (rbCorregir30.isChecked) number1 else number2, if (rbCorregir31.isChecked) number1 else number2, if (rbCorregir32.isChecked) number1 else number2,
                if (rbCorregir33.isChecked) number1 else number2, if (rbCorregir34.isChecked) number1 else number2, if (rbCorregir35.isChecked) number1 else number2, if (rbCorregir36.isChecked) number1 else number2,
                if (rbCorregir37.isChecked) number1 else number2)*/

            var i1 = arrayQuestion.iterator()
            var i2 = desempeño.iterator()
            var i3 = oportunidad.iterator()
            var i4 = corregir.iterator()

            while (i1.hasNext() && i2.hasNext() && i3.hasNext()) {
                val normal = Font(Font.FontFamily.HELVETICA, 8.5f)
                cell = PdfPCell(Phrase(i1.next(), normal))
                cell.backgroundColor = BaseColor(Color.parseColor(getString(R.string.colorGrayCell)))
                table.addCell(cell)

                //cell = PdfPCell(Phrase(i2.next(), normal))
                cell = PdfPCell(i2.next())
                cell.horizontalAlignment = Element.ALIGN_CENTER
                table.addCell(cell)

                //cell = PdfPCell(Phrase(i3.next(), normal))
                cell = PdfPCell(i3.next())
                cell.horizontalAlignment = Element.ALIGN_CENTER
                table.addCell(cell)

                //cell = PdfPCell(Phrase(i4.next(), normal))
                cell = PdfPCell(i4.next())
                cell.horizontalAlignment = Element.ALIGN_CENTER
                table.addCell(cell)
            }

            /*for (i in arrayQuestion) {
                val normal = Font(Font.FontFamily.HELVETICA, 8.5f)
                cell = PdfPCell(Phrase(i, normal))
                cell.backgroundColor = BaseColor(Color.parseColor(getString(R.string.colorGrayCell)))
                val empty1 = ""
                val empty2 = ""
                val empty3 = ""
                table.addCell(cell)
                table.addCell(empty1)
                table.addCell(empty2)
                table.addCell(empty3)
            }*/

            //val df = DecimalFormat("0.00")
            /*for (i in 0..14) {
            }*/
            table.writeSelectedRows(0, -1, document.leftMargin(), 650f, docWriter.directContent)
            table2.writeSelectedRows(0, -1, document.leftMargin(), 315f, docWriter.directContent)



            /*document.newPage()

            val inputStream8 = activity?.assets!!.open("fdsiimagen.png")
            val bitmap8 = BitmapFactory.decodeStream(inputStream8)
            val stream8 = ByteArrayOutputStream()
            bitmap8.compress(Bitmap.CompressFormat.PNG, 100, stream8)
            val companyLogo2 = Image.getInstance(stream.toByteArray())
            companyLogo2.setAbsolutePosition(25f, 770f)
            companyLogo2.scalePercent(25f)
            document.add(companyLogo2)
            createHeadings(cb, 450f, 815f, "EMPRESA CERTIFICADA")
            createHeadingsBasics(cb, 450f, 800f, "ISO   9001 - 2015")
            createHeadingsBasics(cb, 450f, 785f, "ISO 14001 - 2015")

            var barra3 = Image.getInstance(stream2.toByteArray())
            barra3.setAbsolutePosition(25f, 65f)
            barra3.scalePercent(57f, 10f)
            document.add(barra3)

            var barra4 = Image.getInstance(stream2.toByteArray())
            barra4.setAbsolutePosition(25f, 765f)
            barra4.scalePercent(57f, 10f)
            document.add(barra4)

            var inputStream9 = activity?.assets!!.open("lpimg.png")
            var bitmap9 = BitmapFactory.decodeStream(inputStream9)
            var stream9 = ByteArrayOutputStream()
            bitmap9.compress(Bitmap.CompressFormat.PNG, 100, stream9)
            var imgact4 = Image.getInstance(stream9.toByteArray())
            imgact4.setAbsolutePosition(25f, 10f)
            imgact4.scalePercent(40f)
            document.add(imgact4)

            var inputStream10 = activity?.assets!!.open("dbimg.png")
            var bitmap10 = BitmapFactory.decodeStream(inputStream10)
            var stream10 = ByteArrayOutputStream()
            bitmap10.compress(Bitmap.CompressFormat.PNG, 100, stream10)
            var imgact5 = Image.getInstance(stream10.toByteArray())
            imgact5.setAbsolutePosition(400f, 13f)
            imgact5.scalePercent(40f)
            document.add(imgact5)

            var inputStream11 = activity?.assets!!.open("jfimg.png")
            var bitmap11 = BitmapFactory.decodeStream(inputStream11)
            var stream11 = ByteArrayOutputStream()
            bitmap11.compress(Bitmap.CompressFormat.PNG, 100, stream11)
            var imgact6 = Image.getInstance(stream11.toByteArray())
            imgact6.setAbsolutePosition(205f, 13f)
            imgact6.scalePercent(40f)
            document.add(imgact6)

            createHeadingsBasics(cb, 200f, 750f, "EVIDENCIA FOTOGRAFICA")

            createHeadingsinf(cb, 25f, 55f, "Calz. de Camarones No. 695 Of. 1, Col. El Recreo")
            createHeadingsinf(cb, 25f, 40f, "Deleg. Azcapotzalco, C.P. 02070, CDMX")
            createHeadingsinf(cb, 315f, 40f, "Tel:(55)26143757")
            createHeadingsinf(cb, 490f, 40f, "www.fdsi.com.mx")*/


            val evidenciaDrawable = imgEvidencia.drawable as BitmapDrawable
            val bmp = evidenciaDrawable.bitmap
            var streamEvidencia = ByteArrayOutputStream()
            bmp.compress(Bitmap.CompressFormat.JPEG, 10, streamEvidencia)
            var evidenciaImg = Image.getInstance(streamEvidencia.toByteArray())
            evidenciaImg.setAbsolutePosition(40f, 160f)
            evidenciaImg.scalePercent(2f, 2f)
            document.add(evidenciaImg)

            val evidenciaDrawable2 = imgEvidencia2.drawable as BitmapDrawable
            val bmp2 = evidenciaDrawable2.bitmap
            var streamEvidencia2 = ByteArrayOutputStream()
            bmp2.compress(Bitmap.CompressFormat.JPEG, 10, streamEvidencia2)
            var evidenciaImg2 = Image.getInstance(streamEvidencia2.toByteArray())
            evidenciaImg2.setAbsolutePosition(120f, 160f)
            evidenciaImg2.scalePercent(2f, 2f)
            document.add(evidenciaImg2)

            val evidenciaDrawable3 = imgEvidencia3.drawable as BitmapDrawable
            val bmp3 = evidenciaDrawable3.bitmap
            var streamEvidencia3 = ByteArrayOutputStream()
            bmp3.compress(Bitmap.CompressFormat.JPEG, 10, streamEvidencia3)
            var evidenciaImg3 = Image.getInstance(streamEvidencia3.toByteArray())
            evidenciaImg3.setAbsolutePosition(200f, 160f)
            evidenciaImg3.scalePercent(2f, 2f)
            document.add(evidenciaImg3)

            val evidenciaDrawable4 = imgEvidencia4.drawable as BitmapDrawable
            val bmp4 = evidenciaDrawable4.bitmap
            var streamEvidencia4 = ByteArrayOutputStream()
            bmp4.compress(Bitmap.CompressFormat.JPEG, 10, streamEvidencia4)
            var evidenciaImg4 = Image.getInstance(streamEvidencia4.toByteArray())
            evidenciaImg4.setAbsolutePosition(280f, 160f)
            evidenciaImg4.scalePercent(2f, 2f)
            document.add(evidenciaImg4)

            val evidenciaDrawable5 = imgEvidencia5.drawable as BitmapDrawable
            val bmp5 = evidenciaDrawable5.bitmap
            var streamEvidencia5 = ByteArrayOutputStream()
            bmp5.compress(Bitmap.CompressFormat.JPEG, 10, streamEvidencia5)
            var evidenciaImg5 = Image.getInstance(streamEvidencia5.toByteArray())
            evidenciaImg5.setAbsolutePosition(360f, 160f)
            evidenciaImg5.scalePercent(2f, 2f)
            document.add(evidenciaImg5)

            /*val evidenciaDrawable6 = imgEvidencia6.drawable as BitmapDrawable
            val bmp6 = evidenciaDrawable6.bitmap
            var streamEvidencia6 = ByteArrayOutputStream()
            bmp6.compress(Bitmap.CompressFormat.JPEG, 4, streamEvidencia6)
            var evidenciaImg6 = Image.getInstance(streamEvidencia6.toByteArray())
            evidenciaImg6.setAbsolutePosition(440f, 160f)
            evidenciaImg6.scalePercent(2f, 2f)
            document.add(evidenciaImg6)

            val evidenciaDrawable7 = imgEvidencia7.drawable as BitmapDrawable
            val bmp7 = evidenciaDrawable7.bitmap
            var streamEvidencia7 = ByteArrayOutputStream()
            bmp7.compress(Bitmap.CompressFormat.JPEG, 4, streamEvidencia7)
            var evidenciaImg7 = Image.getInstance(streamEvidencia7.toByteArray())
            evidenciaImg7.setAbsolutePosition(40f, 70f)
            evidenciaImg7.scalePercent(2f, 2f)
            document.add(evidenciaImg7)

            val evidenciaDrawable8 = imgEvidencia8.drawable as BitmapDrawable
            val bmp8 = evidenciaDrawable8.bitmap
            var streamEvidencia8 = ByteArrayOutputStream()
            bmp8.compress(Bitmap.CompressFormat.JPEG, 4, streamEvidencia8)
            var evidenciaImg8 = Image.getInstance(streamEvidencia8.toByteArray())
            evidenciaImg8.setAbsolutePosition(120f, 70f)
            evidenciaImg8.scalePercent(2f, 2f)
            document.add(evidenciaImg8)

            val evidenciaDrawable9 = imgEvidencia9.drawable as BitmapDrawable
            val bmp9 = evidenciaDrawable9.bitmap
            var streamEvidencia9 = ByteArrayOutputStream()
            bmp9.compress(Bitmap.CompressFormat.JPEG, 4, streamEvidencia9)
            var evidenciaImg9 = Image.getInstance(streamEvidencia9.toByteArray())
            evidenciaImg9.setAbsolutePosition(200f, 70f)
            evidenciaImg9.scalePercent(2f, 2f)
            document.add(evidenciaImg9)*/

            /*createHeadingsinf(cb, 43f, 140f, "Evidencia 1")
            createHeadingsinf(cb, 123f, 140f, "Evidencia 2")
            createHeadingsinf(cb, 203f, 140f, "Evidencia 3")
            createHeadingsinf(cb, 283f, 140f, "Evidencia 4")
            createHeadingsinf(cb, 363f, 140f, "Evidencia 5")
            createHeadingsinf(cb, 443f, 140f, "Evidencia 6")*/
           // createHeadingsinf(cb, 450f, 485f, "Evidencia 2")
            //createHeadingsinf(cb, 100f, 235f, "Evidencia 3")

            document.close()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        sendPdfToEmail("Archivo_${txtEstablishment.text.toString()}.pdf", filePath)
    }

    private fun createHeadings(cb: PdfContentByte, x: Float, y: Float, text: String) {
        cb.beginText()
        cb.setFontAndSize(baseFont, 10f)
        cb.setTextMatrix(x, y)
        cb.setColorFill(BaseColor(Color.parseColor(getString(R.string.colorBlueLogo))))
        cb.showText(text.trim())
        cb.endText()
    }

    private fun createHeadingsBasics(cb: PdfContentByte, x: Float, y: Float, text: String) {
        cb.beginText()
        cb.setFontAndSize(baseFont, 15f)
        cb.setTextMatrix(x, y)
        cb.showText(text.trim())
        cb.endText()
    }

    private fun createHeadingsinf(cb: PdfContentByte, x: Float, y: Float, text: String) {
        cb.beginText()
        cb.setFontAndSize(baseFont, 10f)
        cb.setTextMatrix(x, y)
        cb.setColorFill(BaseColor(Color.parseColor(getString(R.string.colorBlueLogo))))
        cb.showText(text.trim())
        cb.endText()
    }

    private fun viewPdf(file: String, directory: String) {
        val files = File(getStorageDir(directory), file)
        val path = FileProvider.getUriForFile(activity?.applicationContext!!, activity?.applicationContext!!.packageName+".fileprovider", files)

        val intentPdf = Intent(Intent.ACTION_VIEW)
        intentPdf.setDataAndType(path, "application/pdf")
        intentPdf.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        try {
            startActivity(intentPdf)
        } catch (e: ActivityNotFoundException) {
            e.printStackTrace()
        }
    }

    private fun sendPdfToEmail (file: String, directory: String) {
        val sp3 = arguments?.getBundle("bundle1")?.getString("sp3")
        when(sp3) {
            "Soriana Tienda 277 San Mateo" -> {
                val files = File(getStorageDir(directory), file)
                val path = FileProvider.getUriForFile(activity?.applicationContext!!, "${activity?.applicationContext!!.packageName}.fileprovider", files)
                val emailIntent = Intent(Intent.ACTION_SEND)
                emailIntent.type = "vnd.android.cursor.dir/email"
                val to = arrayOf("henrycaz@soriana.com", "almalpg@soriana.com", "victorsc@soriana.com", "felipejia@soriana.com", "gerencia260@soriana.com")
                val cc = arrayOf("jorge.lozada@fdsi.com.mx", "guadalupe.ocampo@fdsi.com.mx", "alejandro.becerril@fdsi.com.mx")
                //val to = arrayOf("guadalupe.ocampo@fdsi.com.com", "javier_martinez9604@hotmail.com", "alejandro.becerril@fdsi.com.mx", "cesar.segundo@iib-consulting.com", "ingsergio93@hotmail.com")
                emailIntent.putExtra(Intent.EXTRA_EMAIL, to)
                emailIntent.putExtra(Intent.EXTRA_STREAM, path)
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Reporte de evaluación")
                emailIntent.putExtra(Intent.EXTRA_CC, cc)
                try {
                    startActivity(Intent.createChooser(emailIntent, "Enviando correo..."))
                } catch (e: ActivityNotFoundException) {
                    e.printStackTrace()
                }
            }
            "Soriana Tienda 260 Echegaray" -> {
                val files = File(getStorageDir(directory), file)
                val path = FileProvider.getUriForFile(activity?.applicationContext!!, "${activity?.applicationContext!!.packageName}.fileprovider", files)
                val emailIntent = Intent(Intent.ACTION_SEND)
                emailIntent.type = "vnd.android.cursor.dir/email"
                val to = arrayOf("henrycaz@soriana.com", "almalpg@soriana.com", "victorsc@soriana.com", "felipejia@soriana.com", "gerencia260@soriana.com")
                val cc = arrayOf("jorge.lozada@fdsi.com.mx", "guadalupe.ocampo@fdsi.com.mx", "alejandro.becerril@fdsi.com.mx")
                emailIntent.putExtra(Intent.EXTRA_EMAIL, to)
                emailIntent.putExtra(Intent.EXTRA_STREAM, path)
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Reporte de evaluación")
                emailIntent.putExtra(Intent.EXTRA_CC, cc)
                try {
                    startActivity(Intent.createChooser(emailIntent, "Enviando correo..."))
                } catch (e: ActivityNotFoundException) {
                    e.printStackTrace()
                }
            }
            "Soriana Tienda 258 Lomas Verdes" -> {
                val files = File(getStorageDir(directory), file)
                val path = FileProvider.getUriForFile(activity?.applicationContext!!, "${activity?.applicationContext!!.packageName}.fileprovider", files)
                val emailIntent = Intent(Intent.ACTION_SEND)
                emailIntent.type = "vnd.android.cursor.dir/email"
                val to = arrayOf("henrycaz@soriana.com", "almalpg@soriana.com", "victorsc@soriana.com", "felipejia@soriana.com", "gerencia260@soriana.com")
                val cc = arrayOf("jorge.lozada@fdsi.com.mx", "guadalupe.ocampo@fdsi.com.mx", "alejandro.becerril@fdsi.com.mx")
                emailIntent.putExtra(Intent.EXTRA_EMAIL, to)
                emailIntent.putExtra(Intent.EXTRA_STREAM, path)
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Reporte de evaluación")
                emailIntent.putExtra(Intent.EXTRA_CC, cc)
                try {
                    startActivity(Intent.createChooser(emailIntent, "Enviando correo..."))
                } catch (e: ActivityNotFoundException) {
                    e.printStackTrace()
                }
            }
            "Soriana tienda 281 Peri Norte" -> {
                val files = File(getStorageDir(directory), file)
                val path = FileProvider.getUriForFile(activity?.applicationContext!!, "${activity?.applicationContext!!.packageName}.fileprovider", files)
                val emailIntent = Intent(Intent.ACTION_SEND)
                emailIntent.type = "vnd.android.cursor.dir/email"
                val to = arrayOf("henrycaz@soriana.com", "almalpg@soriana.com", "victorsc@soriana.com", "joaquinabc@soriana.com", "gerencia281@soriana.com")
                val cc = arrayOf("jorge.lozada@fdsi.com.mx", "guadalupe.ocampo@fdsi.com.mx", "alejandro.becerril@fdsi.com.mx")
                emailIntent.putExtra(Intent.EXTRA_EMAIL, to)
                emailIntent.putExtra(Intent.EXTRA_STREAM, path)
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Reporte de evaluación")
                emailIntent.putExtra(Intent.EXTRA_CC, cc)
                try {
                    startActivity(Intent.createChooser(emailIntent, "Enviando correo..."))
                } catch (e: ActivityNotFoundException) {
                    e.printStackTrace()
                }


            }
            "Soriana tienda 406 Cuautitlan" -> {
                val files = File(getStorageDir(directory), file)
                val path = FileProvider.getUriForFile(activity?.applicationContext!!, "${activity?.applicationContext!!.packageName}.fileprovider", files)
                val emailIntent = Intent(Intent.ACTION_SEND)
                emailIntent.type = "vnd.android.cursor.dir/email"
                val to = arrayOf("henrycaz@soriana.com", "almalpg@soriana.com", "victorsc@soriana.com", "joaquinabc@soriana.com", "gerencia281@soriana.com")
                val cc = arrayOf("jorge.lozada@fdsi.com.mx", "guadalupe.ocampo@fdsi.com.mx", "alejandro.becerril@fdsi.com.mx")
                emailIntent.putExtra(Intent.EXTRA_EMAIL, to)
                emailIntent.putExtra(Intent.EXTRA_STREAM, path)
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Reporte de evaluación")
                emailIntent.putExtra(Intent.EXTRA_CC, cc)
                try {
                    startActivity(Intent.createChooser(emailIntent, "Enviando correo..."))
                } catch (e: ActivityNotFoundException) {
                    e.printStackTrace()
                }
            }
            "Soriana Tienda 197 Sendero Toluca" -> {
                val files = File(getStorageDir(directory), file)
                val path = FileProvider.getUriForFile(activity?.applicationContext!!, "${activity?.applicationContext!!.packageName}.fileprovider", files)
                val emailIntent = Intent(Intent.ACTION_SEND)
                emailIntent.type = "vnd.android.cursor.dir/email"
                val to = arrayOf("henrycaz@soriana.com", "almalpg@soriana.com", "victorsc@soriana.com", "hectorfo@soriana.com", "gerencia197@soriana.com")
                val cc = arrayOf("jorge.lozada@fdsi.com.mx", "guadalupe.ocampo@fdsi.com.mx", "alejandro.becerril@fdsi.com.mx")
                emailIntent.putExtra(Intent.EXTRA_EMAIL, to)
                emailIntent.putExtra(Intent.EXTRA_STREAM, path)
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Reporte de evaluación")
                emailIntent.putExtra(Intent.EXTRA_CC, cc)
                try {
                    startActivity(Intent.createChooser(emailIntent, "Enviando correo..."))
                } catch (e: ActivityNotFoundException) {
                    e.printStackTrace()
                }
            }
            "Soriana Tienda 262 Iztapalapa" -> {
                val files = File(getStorageDir(directory), file)
                val path = FileProvider.getUriForFile(activity?.applicationContext!!, "${activity?.applicationContext!!.packageName}.fileprovider", files)
                val emailIntent = Intent(Intent.ACTION_SEND)
                emailIntent.type = "vnd.android.cursor.dir/email"
                val to = arrayOf("henrycaz@soriana.com", "almalpg@soriana.com", "victorsc@soriana.com", "juanfoa@soriana.com", "gerencia262@soriana.com")
                val cc = arrayOf("jorge.lozada@fdsi.com.mx", "guadalupe.ocampo@fdsi.com.mx", "alejandro.becerril@fdsi.com.mx")
                emailIntent.putExtra(Intent.EXTRA_EMAIL, to)
                emailIntent.putExtra(Intent.EXTRA_STREAM, path)
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Reporte de evaluación")
                emailIntent.putExtra(Intent.EXTRA_CC, cc)
                try {
                    startActivity(Intent.createChooser(emailIntent, "Enviando correo..."))
                } catch (e: ActivityNotFoundException) {
                    e.printStackTrace()
                }
            }
            "Soriana Tienda 397 Parque Delta" -> {
                val files = File(getStorageDir(directory), file)
                val path = FileProvider.getUriForFile(activity?.applicationContext!!, "${activity?.applicationContext!!.packageName}.fileprovider", files)
                val emailIntent = Intent(Intent.ACTION_SEND)
                emailIntent.type = "vnd.android.cursor.dir/email"
                val to = arrayOf("henrycaz@soriana.com", "almalpg@soriana.com", "victorsc@soriana.com", "hectorfo@soriana.com", "gerencia397@soriana.com")
                val cc = arrayOf("jorge.lozada@fdsi.com.mx", "guadalupe.ocampo@fdsi.com.mx", "alejandro.becerril@fdsi.com.mx")
                emailIntent.putExtra(Intent.EXTRA_EMAIL, to)
                emailIntent.putExtra(Intent.EXTRA_STREAM, path)
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Reporte de evaluacón")
                emailIntent.putExtra(Intent.EXTRA_CC, cc)
                try {
                    startActivity(Intent.createChooser(emailIntent, "Enviando correo..."))
                } catch (e: ActivityNotFoundException) {
                    e.printStackTrace()
                }
            }
            "Soriana Tienda 009 La Fe" -> {
                val files = File(getStorageDir(directory), file)
                val path = FileProvider.getUriForFile(activity?.applicationContext!!, "${activity?.applicationContext!!.packageName}.fileprovider", files)
                val emailIntent = Intent(Intent.ACTION_SEND)
                emailIntent.type = "vnd.android.cursor.dir/email"
                val to = arrayOf("henrycaz@soriana.com", "almalpg@soriana.com", "victorsc@soriana.com", "gerencia09@soriana.com")
                val cc = arrayOf("jorge.lozada@fdsi.com.mx", "guadalupe.ocampo@fdsi.com.mx", "alejandro.becerril@fdsi.com.mx")
                emailIntent.putExtra(Intent.EXTRA_EMAIL, to)
                emailIntent.putExtra(Intent.EXTRA_STREAM, path)
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Reporte de evaluación")
                emailIntent.putExtra(Intent.EXTRA_CC, cc)
                try {
                    startActivity(Intent.createChooser(emailIntent, "Enviando correo..."))
                } catch (e: ActivityNotFoundException) {
                    e.printStackTrace()
                }
            }
            "Soriana Tienda 027 Santa Maria" -> {
                val files = File(getStorageDir(directory), file)
                val path = FileProvider.getUriForFile(activity?.applicationContext!!, "${activity?.applicationContext!!.packageName}.fileprovider", files)
                val emailIntent = Intent(Intent.ACTION_SEND)
                emailIntent.type = "vnd.android.cursor.dir/email"
                val to = arrayOf("henrycaz@soriana.com", "almalpg@soriana.com", "victorsc@soriana.com", "gerencia027@soriana.com")
                val cc = arrayOf("jorge.lozada@fdsi.com.mx", "guadalupe.ocampo@fdsi.com.mx", "alejandro.becerril@fdsi.com.mx")
                emailIntent.putExtra(Intent.EXTRA_EMAIL, to)
                emailIntent.putExtra(Intent.EXTRA_STREAM, path)
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Reporte de evaluación")
                emailIntent.putExtra(Intent.EXTRA_CC, cc)
                try {
                    startActivity(Intent.createChooser(emailIntent, "Enviando correo..."))
                } catch (e: ActivityNotFoundException) {
                    e.printStackTrace()
                }
            }
            "Soriana Tienda 419 Plaza Cantil" -> {
                val files = File(getStorageDir(directory), file)
                val path = FileProvider.getUriForFile(activity?.applicationContext!!, "${activity?.applicationContext!!.packageName}.fileprovider", files)
                val emailIntent = Intent(Intent.ACTION_SEND)
                emailIntent.type = "vnd.android.cursor.dir/email"
                val to = arrayOf("henrycaz@soriana.com", "almalpg@soriana.com", "victorsc@soriana.com", "hectorfo@soriana.com", "gerencia419@soriana.com")
                val cc = arrayOf("jorge.lozada@fdsi.com.mx", "guadalupe.ocampo@fdsi.com.mx", "alejandro.becerril@fdsi.com.mx")
                emailIntent.putExtra(Intent.EXTRA_EMAIL, to)
                emailIntent.putExtra(Intent.EXTRA_STREAM, path)
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Reporte de evaluación")
                emailIntent.putExtra(Intent.EXTRA_CC, cc)
                try {
                    startActivity(Intent.createChooser(emailIntent, "Enviando correo..."))
                } catch (e: ActivityNotFoundException) {
                    e.printStackTrace()
                }
            }
            "Soriana Tienda 522 Buenavista" -> {
                val files = File(getStorageDir(directory), file)
                val path = FileProvider.getUriForFile(activity?.applicationContext!!, "${activity?.applicationContext!!.packageName}.fileprovider", files)
                val emailIntent = Intent(Intent.ACTION_SEND)
                emailIntent.type = "vnd.android.cursor.dir/email"
                val to = arrayOf("henrycaz@soriana.com", "almalpg@soriana.com", "victorsc@soriana.com", "juaquinabc@soriana.com", "gerencia522@soriana.com")
                val cc = arrayOf("jorge.lozada@fdsi.com.mx", "guadalupe.ocampo@fdsi.com.mx", "alejandro.becerril@fdsi.com.mx")
                emailIntent.putExtra(Intent.EXTRA_EMAIL, to)
                emailIntent.putExtra(Intent.EXTRA_STREAM, path)
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Reporte de evaluación")
                emailIntent.putExtra(Intent.EXTRA_CC, cc)
                try {
                    startActivity(Intent.createChooser(emailIntent, "Enviando correo..."))
                } catch (e: ActivityNotFoundException) {
                    e.printStackTrace()
                }
            }
            "Soriana Tienda 250 Mixcoac" -> {
                val files = File(getStorageDir(directory), file)
                val path = FileProvider.getUriForFile(activity?.applicationContext!!, "${activity?.applicationContext!!.packageName}.fileprovider", files)
                val emailIntent = Intent(Intent.ACTION_SEND)
                emailIntent.type = "vnd.android.cursor.dir/email"
                val to = arrayOf("henrycaz@soriana.com", "almalpg@soriana.com", "victorsc@soriana.com", "hectorfo@soriana.com", "gerencia250@soriana.com")
                val cc = arrayOf("jorge.lozada@fdsi.com.mx", "guadalupe.ocampo@fdsi.com.mx", "alejandro.becerril@fdsi.com.mx")
                emailIntent.putExtra(Intent.EXTRA_EMAIL, to)
                emailIntent.putExtra(Intent.EXTRA_STREAM, path)
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Reporte de evaluacón")
                emailIntent.putExtra(Intent.EXTRA_CC, cc)
                try {
                    startActivity(Intent.createChooser(emailIntent, "Enviando correo..."))
                } catch (e: ActivityNotFoundException) {
                    e.printStackTrace()
                }
            }
            "Soriana Tienda 269 Zaragoza" -> {
                val files = File(getStorageDir(directory), file)
                val path = FileProvider.getUriForFile(activity?.applicationContext!!, "${activity?.applicationContext!!.packageName}.fileprovider", files)
                val emailIntent = Intent(Intent.ACTION_SEND)
                emailIntent.type = "vnd.android.cursor.dir/email"
                val to = arrayOf("henrycaz@soriana.com", "almalpg@soriana.com", "victorsc@soriana.com", "juanfoa@soriana.com", "gerencia269@soriana.com")
                val cc = arrayOf("jorge.lozada@fdsi.com.mx", "guadalupe.ocampo@fdsi.com.mx", "alejandro.becerril@fdsi.com.mx")
                emailIntent.putExtra(Intent.EXTRA_EMAIL, to)
                emailIntent.putExtra(Intent.EXTRA_STREAM, path)
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Reporte de evaluación")
                emailIntent.putExtra(Intent.EXTRA_CC, cc)
                try {
                    startActivity(Intent.createChooser(emailIntent, "Enviando correo..."))
                } catch (e: ActivityNotFoundException) {
                    e.printStackTrace()
                }
            }
            "Soriana Tienda 255 La Villa" -> {
                val files = File(getStorageDir(directory), file)
                val path = FileProvider.getUriForFile(activity?.applicationContext!!, "${activity?.applicationContext!!.packageName}.fileprovider", files)
                val emailIntent = Intent(Intent.ACTION_SEND)
                emailIntent.type = "vnd.android.cursor.dir/email"
                val to = arrayOf("henrycaz@soriana.com", "almalpg@soriana.com", "victorsc@soriana.com", "gerencia255@soriana.com")
                val cc = arrayOf("jorge.lozada@fdsi.com.mx", "guadalupe.ocampo@fdsi.com.mx", "alejandro.becerril@fdsi.com.mx")
                emailIntent.putExtra(Intent.EXTRA_EMAIL, to)
                emailIntent.putExtra(Intent.EXTRA_STREAM, path)
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Reporte de evaluación")
                emailIntent.putExtra(Intent.EXTRA_CC, cc)
                try {
                    startActivity(Intent.createChooser(emailIntent, "Enviando correo..."))
                } catch (e: ActivityNotFoundException) {
                    e.printStackTrace()
                }
            }
            "Soriana Tienda 279 Eugenia" -> {
                val files = File(getStorageDir(directory), file)
                val path = FileProvider.getUriForFile(activity?.applicationContext!!, "${activity?.applicationContext!!.packageName}.fileprovider", files)
                val emailIntent = Intent(Intent.ACTION_SEND)
                emailIntent.type = "vnd.android.cursor.dir/email"
                val to = arrayOf("henrycaz@soriana.com", "almalpg@soriana.com", "victorsc@soriana.com", "gerencia279@soriana.com")
                val cc = arrayOf("jorge.lozada@fdsi.com.mx", "guadalupe.ocampo@fdsi.com.mx", "alejandro.becerril@fdsi.com.mx")
                emailIntent.putExtra(Intent.EXTRA_EMAIL, to)
                emailIntent.putExtra(Intent.EXTRA_STREAM, path)
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Reporte de evaluación")
                emailIntent.putExtra(Intent.EXTRA_CC, cc)
                try {
                    startActivity(Intent.createChooser(emailIntent, "Enviando correo..."))
                } catch (e: ActivityNotFoundException) {
                    e.printStackTrace()
                }
            }
            "Soriana Tienda 257 Tacubaya" -> {
                val files = File(getStorageDir(directory), file)
                val path = FileProvider.getUriForFile(activity?.applicationContext!!, "${activity?.applicationContext!!.packageName}.fileprovider", files)
                val emailIntent = Intent(Intent.ACTION_SEND)
                emailIntent.type = "vnd.android.cursor.dir/email"
                val to = arrayOf("henrycaz@soriana.com", "almalpg@soriana.com", "victorsc@soriana.com", "felipejia@soriana.com", "gerencia257@soriana.com")
                val cc = arrayOf("jorge.lozada@fdsi.com.mx", "guadalupe.ocampo@fdsi.com.mx", "alejandro.becerril@fdsi.com.mx")
                emailIntent.putExtra(Intent.EXTRA_EMAIL, to)
                emailIntent.putExtra(Intent.EXTRA_STREAM, path)
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Reporte de evaluación")
                emailIntent.putExtra(Intent.EXTRA_CC, cc)
                try {
                    startActivity(Intent.createChooser(emailIntent, "Enviando correo..."))
                } catch (e: ActivityNotFoundException) {
                    e.printStackTrace()
                }
            }
            "Soriana Tienda 682 Pabellón Azcapotzalco" -> {
                val files = File(getStorageDir(directory), file)
                val path = FileProvider.getUriForFile(activity?.applicationContext!!, "${activity?.applicationContext!!.packageName}.fileprovider", files)
                val emailIntent = Intent(Intent.ACTION_SEND)
                emailIntent.type = "vnd.android.cursor.dir/email"
                val to = arrayOf("henrycaz@soriana.com", "almalpg@soriana.com", "victorsc@soriana.com", "joaquinabc@soriana.com", "gerencia682@sorian.com")
                val cc = arrayOf("jorge.lozada@fdsi.com.mx", "guadalupe.ocampo@fdsi.com.mx", "alejandro.becerril@fdsi.com.mx")
                emailIntent.putExtra(Intent.EXTRA_EMAIL, to)
                emailIntent.putExtra(Intent.EXTRA_STREAM, path)
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Reporte de evaluación")
                emailIntent.putExtra(Intent.EXTRA_CC, cc)
                try {
                    startActivity(Intent.createChooser(emailIntent, "Enviando correo..."))
                } catch (e: ActivityNotFoundException) {
                    e.printStackTrace()
                }
            }
            "Soriana Tienda 261 El Rosario" -> {
                val files = File(getStorageDir(directory), file)
                val path = FileProvider.getUriForFile(activity?.applicationContext!!, "${activity?.applicationContext!!.packageName}.fileprovider", files)
                val emailIntent = Intent(Intent.ACTION_SEND)
                emailIntent.type = "vnd.android.cursor.dir/email"
                val to = arrayOf("henrycaz@soriana.com", "almalpg@soriana.com", "victorsc@soriana.com", "joaquinabc@soriana.com", "gerencia261@soriana.com")
                val cc = arrayOf("jorge.lozada@fdsi.com.mx", "guadalupe.ocampo@fdsi.com.mx", "alejandro.becerril@fdsi.com.mx")
                emailIntent.putExtra(Intent.EXTRA_EMAIL, to)
                emailIntent.putExtra(Intent.EXTRA_STREAM, path)
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Reporte de evaluación")
                emailIntent.putExtra(Intent.EXTRA_CC, cc)
                try {
                    startActivity(Intent.createChooser(emailIntent, "Enviando correo..."))
                } catch (e: ActivityNotFoundException) {
                    e.printStackTrace()
                }
            }
            "Soriana Tienda 253 Cuitláhuac" -> {
                val files = File(getStorageDir(directory), file)
                val path = FileProvider.getUriForFile(activity?.applicationContext!!, "${activity?.applicationContext!!.packageName}.fileprovider", files)
                val emailIntent = Intent(Intent.ACTION_SEND)
                emailIntent.type = "vnd.android.cursor.dir/email"
                val to = arrayOf("henrycaz@soriana.com", "almalpg@soriana.com", "victorsc@soriana.com", "joaquinabc@soriana.com", "gerencia253@soriana.com")
                val cc = arrayOf("jorge.lozada@fdsi.com.mx", "guadalupe.ocampo@fdsi.com.mx", "alejandro.becerril@fdsi.com.mx")
                emailIntent.putExtra(Intent.EXTRA_EMAIL, to)
                emailIntent.putExtra(Intent.EXTRA_STREAM, path)
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Reporte de evaluación")
                emailIntent.putExtra(Intent.EXTRA_CC, cc)
                try {
                    startActivity(Intent.createChooser(emailIntent, "Enviando correo..."))
                } catch (e: ActivityNotFoundException) {
                    e.printStackTrace()
                }
            }
            "Soriana Tienda 251 La Viga" -> {
                val files = File(getStorageDir(directory), file)
                val path = FileProvider.getUriForFile(activity?.applicationContext!!, "${activity?.applicationContext!!.packageName}.fileprovider", files)
                val emailIntent = Intent(Intent.ACTION_SEND)
                emailIntent.type = "vnd.android.cursor.dir/email"
                val to = arrayOf("henrycaz@soriana.com", "almalpg@soriana.com", "victorsc@soriana.com", "juanfoa@soriana.com", "gerencia251@soriana.com")
                val cc = arrayOf("jorge.lozada@fdsi.com.mx", "guadalupe.ocampo@fdsi.com.mx", "alejandro.becerril@fdsi.com.mx")
                emailIntent.putExtra(Intent.EXTRA_EMAIL, to)
                emailIntent.putExtra(Intent.EXTRA_STREAM, path)
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Reporte de evaluación")
                emailIntent.putExtra(Intent.EXTRA_CC, cc)
                try {
                    startActivity(Intent.createChooser(emailIntent, "Enviando correo..."))
                } catch (e: ActivityNotFoundException) {
                    e.printStackTrace()
                }
            }
            "Soriana Tienda 286 El Salado" -> {
                val files = File(getStorageDir(directory), file)
                val path = FileProvider.getUriForFile(activity?.applicationContext!!, "${activity?.applicationContext!!.packageName}.fileprovider", files)
                val emailIntent = Intent(Intent.ACTION_SEND)
                emailIntent.type = "vnd.android.cursor.dir/email"
                val to = arrayOf("henrycaz@soriana.com", "almalpg@soriana.com", "victorsc@soriana.com", "juanfoa@soriana.com", "gerencia286@soriana.com")
                val cc = arrayOf("jorge.lozada@fdsi.com.mx", "guadalupe.ocampo@fdsi.com.mx", "alejandro.becerril@fdsi.com.mx")
                emailIntent.putExtra(Intent.EXTRA_EMAIL, to)
                emailIntent.putExtra(Intent.EXTRA_STREAM, path)
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Reporte de evaluación")
                emailIntent.putExtra(Intent.EXTRA_CC, cc)
                try {
                    startActivity(Intent.createChooser(emailIntent, "Enviando correo..."))
                } catch (e: ActivityNotFoundException) {
                    e.printStackTrace()
                }
            }
            "Soriana Tienda 418 Consulado" -> {
                val files = File(getStorageDir(directory), file)
                val path = FileProvider.getUriForFile(activity?.applicationContext!!, "${activity?.applicationContext!!.packageName}.fileprovider", files)
                val emailIntent = Intent(Intent.ACTION_SEND)
                emailIntent.type = "vnd.android.cursor.dir/email"
                val to = arrayOf("henrycaz@soriana.com", "almalpg@soriana.com", "victorsc@soriana.com", "felipejia@soriana.com", "gerencia418@soriana.com")
                val cc = arrayOf("jorge.lozada@fdsi.com.mx", "guadalupe.ocampo@fdsi.com.mx", "alejandro.becerril@fdsi.com.mx")
                emailIntent.putExtra(Intent.EXTRA_EMAIL, to)
                emailIntent.putExtra(Intent.EXTRA_STREAM, path)
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Reporte de evaluación")
                emailIntent.putExtra(Intent.EXTRA_CC, cc)
                try {
                    startActivity(Intent.createChooser(emailIntent, "Enviando correo..."))
                } catch (e: ActivityNotFoundException) {
                    e.printStackTrace()
                }
            }
            "Soriana Tienda 686 Vía Vallejo" -> {
                val files = File(getStorageDir(directory), file)
                val path = FileProvider.getUriForFile(activity?.applicationContext!!, "${activity?.applicationContext!!.packageName}.fileprovider", files)
                val emailIntent = Intent(Intent.ACTION_SEND)
                emailIntent.type = "vnd.android.cursor.dir/email"
                val to = arrayOf("henrycaz@soriana.com", "almalpg@soriana.com", "victorsc@soriana.com", "joaquinabc@soriana.com", "gerencia683@soriana.com")
                val cc = arrayOf("jorge.lozada@fdsi.com.mx", "guadalupe.ocampo@fdsi.com.mx", "alejandro.becerril@fdsi.com.mx")
                emailIntent.putExtra(Intent.EXTRA_EMAIL, to)
                emailIntent.putExtra(Intent.EXTRA_STREAM, path)
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Reporte de evaluación")
                emailIntent.putExtra(Intent.EXTRA_CC, cc)
                try {
                    startActivity(Intent.createChooser(emailIntent, "Enviando correo..."))
                } catch (e: ActivityNotFoundException) {
                    e.printStackTrace()
                }
            }
            "Soriana Tienda 252 Miyana Polanco" -> {
                val files = File(getStorageDir(directory), file)
                val path = FileProvider.getUriForFile(activity?.applicationContext!!, "${activity?.applicationContext!!.packageName}.fileprovider", files)
                val emailIntent = Intent(Intent.ACTION_SEND)
                emailIntent.type = "vnd.android.cursor.dir/email"
                val to = arrayOf("henrycaz@soriana.com", "almalpg@soriana.com", "victorsc@soriana.com", "felipejia@soriana.com", "gerencia252@soriana.com")
                val cc = arrayOf("jorge.lozada@fdsi.com.mx", "guadalupe.ocampo@fdsi.com.mx", "alejandro.becerril@fdsi.com.mx")
                emailIntent.putExtra(Intent.EXTRA_EMAIL, to)
                emailIntent.putExtra(Intent.EXTRA_STREAM, path)
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Reporte de evaluación")
                emailIntent.putExtra(Intent.EXTRA_CC, cc)
                try {
                    startActivity(Intent.createChooser(emailIntent, "Enviando correo..."))
                } catch (e: ActivityNotFoundException) {
                    e.printStackTrace()
                }
            }
            "Soriana Tienda 356 Los Angeles" -> {
                val files = File(getStorageDir(directory), file)
                val path = FileProvider.getUriForFile(activity?.applicationContext!!, "${activity?.applicationContext!!.packageName}.fileprovider", files)
                val emailIntent = Intent(Intent.ACTION_SEND)
                emailIntent.type = "vnd.android.cursor.dir/email"
                val to = arrayOf("henrycaz@soriana.com", "almalpg@soriana.com", "victorsc@soriana.com", "gerencia356@soriana.com")
                val cc = arrayOf("jorge.lozada@fdsi.com.mx", "guadalupe.ocampo@fdsi.com.mx", "alejandro.becerril@fdsi.com.mx")
                emailIntent.putExtra(Intent.EXTRA_EMAIL, to)
                emailIntent.putExtra(Intent.EXTRA_STREAM, path)
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Reporte de evaluación")
                emailIntent.putExtra(Intent.EXTRA_CC, cc)
                try {
                    startActivity(Intent.createChooser(emailIntent, "Enviando correo..."))
                } catch (e: ActivityNotFoundException) {
                    e.printStackTrace()
                }
            }
            "Soriana Tienda 349 Montes Berneses" -> {
                val files = File(getStorageDir(directory), file)
                val path = FileProvider.getUriForFile(activity?.applicationContext!!, "${activity?.applicationContext!!.packageName}.fileprovider", files)
                val emailIntent = Intent(Intent.ACTION_SEND)
                emailIntent.type = "vnd.android.cursor.dir/email"
                val to = arrayOf("henrycaz@soriana.com", "almalpg@soriana.com", "victorsc@soriana.com", "gerencia349@soriana.com")
                val cc = arrayOf("jorge.lozada@fdsi.com.mx", "guadalupe.ocampo@fdsi.com.mx", "alejandro.becerril@fdsi.com.mx")
                emailIntent.putExtra(Intent.EXTRA_EMAIL, to)
                emailIntent.putExtra(Intent.EXTRA_STREAM, path)
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Reporte de evaluación")
                emailIntent.putExtra(Intent.EXTRA_CC, cc)
                try {
                    startActivity(Intent.createChooser(emailIntent, "Enviando correo..."))
                } catch (e: ActivityNotFoundException) {
                    e.printStackTrace()
                }
            }
            "Soriana Tienda 345 Vista Hermosa" -> {
                val files = File(getStorageDir(directory), file)
                val path = FileProvider.getUriForFile(activity?.applicationContext!!, "${activity?.applicationContext!!.packageName}.fileprovider", files)
                val emailIntent = Intent(Intent.ACTION_SEND)
                emailIntent.type = "vnd.android.cursor.dir/email"
                val to = arrayOf("henrycaz@soriana.com", "almalpg@soriana.com", "victorsc@soriana.com", "gerencia345@soriana.com")
                val cc = arrayOf("jorge.lozada@fdsi.com.mx", "guadalupe.ocampo@fdsi.com.mx", "alejandro.becerril@fdsi.com.mx")
                emailIntent.putExtra(Intent.EXTRA_EMAIL, to)
                emailIntent.putExtra(Intent.EXTRA_STREAM, path)
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Reporte de evaluación")
                emailIntent.putExtra(Intent.EXTRA_CC, cc)
                try {
                    startActivity(Intent.createChooser(emailIntent, "Enviando correo..."))
                } catch (e: ActivityNotFoundException) {
                    e.printStackTrace()
                }
            }
            "Comercial Mexicana Tienda 019 Mixcoac" -> {
                val files = File(getStorageDir(directory), file)
                val path = FileProvider.getUriForFile(activity?.applicationContext!!, "${activity?.applicationContext!!.packageName}.fileprovider", files)
                val emailIntent = Intent(Intent.ACTION_SEND)
                emailIntent.type = "vnd.android.cursor.dir/email"
                val to = arrayOf("henrycaz@soriana.com", "almalpg@soriana.com", "victorsc@soriana.com", "hectorfo@soriana.com", "gerencia857@soriana.com")
                val cc = arrayOf("jorge.lozada@fdsi.com.mx", "guadalupe.ocampo@fdsi.com.mx", "alejandro.becerril@fdsi.com.mx")
                emailIntent.putExtra(Intent.EXTRA_EMAIL, to)
                emailIntent.putExtra(Intent.EXTRA_STREAM, path)
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Reporte de evaluación")
                emailIntent.putExtra(Intent.EXTRA_CC, cc)
                try {
                    startActivity(Intent.createChooser(emailIntent, "Enviando correo..."))
                } catch (e: ActivityNotFoundException) {
                    e.printStackTrace()
                }
            }
            "Comercial Mexicana Tienda Pilares" -> {
                val files = File(getStorageDir(directory), file)
                val path = FileProvider.getUriForFile(activity?.applicationContext!!, "${activity?.applicationContext!!.packageName}.fileprovider", files)
                val emailIntent = Intent(Intent.ACTION_SEND)
                emailIntent.type = "vnd.android.cursor.dir/email"
                val to = arrayOf("henrycaz@soriana.com", "almalpg@soriana.com", "victorsc@soriana.com", "g005@intraccm.com")
                val cc = arrayOf("jorge.lozada@fdsi.com.mx", "guadalupe.ocampo@fdsi.com.mx", "alejandro.becerril@fdsi.com.mx")
                emailIntent.putExtra(Intent.EXTRA_EMAIL, to)
                emailIntent.putExtra(Intent.EXTRA_STREAM, path)
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Reporte de evaluación")
                emailIntent.putExtra(Intent.EXTRA_CC, cc)
                try {
                    startActivity(Intent.createChooser(emailIntent, "Enviando correo..."))
                } catch (e: ActivityNotFoundException) {
                    e.printStackTrace()
                }
            }
            "Comercial Mexicana Tienda  División del Norte" -> {
                val files = File(getStorageDir(directory), file)
                val path = FileProvider.getUriForFile(activity?.applicationContext!!, "${activity?.applicationContext!!.packageName}.fileprovider", files)
                val emailIntent = Intent(Intent.ACTION_SEND)
                emailIntent.type = "vnd.android.cursor.dir/email"
                val to = arrayOf("henrycaz@soriana.com", "almalpg@soriana.com", "victorsc@soriana.com", "gerencia690@soriana.com")
                val cc = arrayOf("jorge.lozada@fdsi.com.mx", "guadalupe.ocampo@fdsi.com.mx", "alejandro.becerril@fdsi.com.mx")
                emailIntent.putExtra(Intent.EXTRA_EMAIL, to)
                emailIntent.putExtra(Intent.EXTRA_STREAM, path)
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Reporte de evaluación")
                emailIntent.putExtra(Intent.EXTRA_CC, cc)
                try {
                    startActivity(Intent.createChooser(emailIntent, "Enviando correo..."))
                } catch (e: ActivityNotFoundException) {
                    e.printStackTrace()
                }
            }
            "Soriana Tienda 341 Fundidora" -> {
                val files = File(getStorageDir(directory), file)
                val path = FileProvider.getUriForFile(activity?.applicationContext!!, "${activity?.applicationContext!!.packageName}.fileprovider", files)
                val emailIntent = Intent(Intent.ACTION_SEND)
                emailIntent.type = "vnd.android.cursor.dir/email"
                val to = arrayOf("henrycaz@soriana.com", "almalpg@soriana.com", "victorsc@soriana.com", "gerencia341@soriana.com")
                val cc = arrayOf("jorge.lozada@fdsi.com.mx", "guadalupe.ocampo@fdsi.com.mx", "alejandro.becerril@fdsi.com.mx")
                emailIntent.putExtra(Intent.EXTRA_EMAIL, to)
                emailIntent.putExtra(Intent.EXTRA_STREAM, path)
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Reporte de evaluación")
                emailIntent.putExtra(Intent.EXTRA_CC, cc)
                try {
                    startActivity(Intent.createChooser(emailIntent, "Enviando correo..."))
                } catch (e: ActivityNotFoundException) {
                    e.printStackTrace()
                }
            }
            "Soriana Tienda 20 Terraza Coapa" -> {
                val files = File(getStorageDir(directory), file)
                val path = FileProvider.getUriForFile(activity?.applicationContext!!, "${activity?.applicationContext!!.packageName}.fileprovider", files)
                val emailIntent = Intent(Intent.ACTION_SEND)
                emailIntent.type = "vnd.android.cursor.dir/email"
                val to = arrayOf("henrycaz@soriana.com", "almalpg@soriana.com", "victorsc@soriana.com", "gerencia272@soriana.com")
                val cc = arrayOf("jorge.lozada@fdsi.com.mx", "guadalupe.ocampo@fdsi.com.mx", "alejandro.becerril@fdsi.com.mx")
                emailIntent.putExtra(Intent.EXTRA_EMAIL, to)
                emailIntent.putExtra(Intent.EXTRA_STREAM, path)
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Reporte de evaluación")
                emailIntent.putExtra(Intent.EXTRA_CC, cc)
                try {
                    startActivity(Intent.createChooser(emailIntent, "Enviando correo..."))
                } catch (e: ActivityNotFoundException) {
                    e.printStackTrace()
                }
            }
        }
    }

    private fun isExternalStorageReadOnly(): Boolean {
        val extStorageState = Environment.getExternalStorageState()
        if (Environment.MEDIA_MOUNTED_READ_ONLY == extStorageState) {
            return true
        }
        return false
    }

    private fun isExternalStorageAvailable(): Boolean {
        val extStorageState = Environment.getExternalStorageState()
        if (Environment.MEDIA_MOUNTED == extStorageState) {
            return true
        }
        return false
    }

    private fun initializeFonts() {
        try {
            baseFont = BaseFont.createFont(BaseFont.HELVETICA_BOLD, BaseFont.CP1252, BaseFont.EMBEDDED)
        } catch (e: DocumentException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    fun getAlbumStorageDirPdf(albumName: String): File? {
        val file = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), albumName)
        if (!file.mkdirs()) {
            Log.e("SignaturePad", "Directorio no creado")
        }
        return file
    }

    fun getStorageDir(albumName: String): File? {
        val file = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), albumName)
        return file
    }

    fun goListReports() {
        val cliente = txtEstablishment.text.toString()
        val fecha = txtFaltasdMes.text.toString()
        val args1 = bundleOf("clientes" to cliente, "fechas" to fecha)
        val bundle1 = Bundle()
        bundle1.putBundle("bundle1", args1)
        findNavController().navigate(R.id.listEvaluation, bundle1)
    }

    fun goToSpiners() {
        findNavController().navigate(R.id.stablishmentFragment)
    }

    /*private fun sendImageToDb() {
        val timeStamp = SimpleDateFormat("yyyyMMddHHmmss").format(Date())
        val pictureFile = "IMG_$timeStamp"
        postImage("${pictureFile}.jpg", directoryName)
    }*/

    private fun postInformation(files1: String, files2: String, files3: String, files4: String, files5: String, directory: String) {
        val file1 = File(getDataDirectory(directory), files1)
        val file2 = File(getDataDirectory(directory), files2)
        val file3 = File(getDataDirectory(directory), files3)
        val file4 = File(getDataDirectory(directory), files4)
        val file5 = File(getDataDirectory(directory), files5)

        /*uploadImage(file1,
            file2,
            file3,
            file4,
            file5,
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25",
            "26",
            "27",
            "28",
            "29",
            "30",
            "31",
            "32",
            "33",
            "34",
            "35",
            "36",
            "37",
            "38",
            "39",
            "40",
            "41",
            "42",
            "43",
            "44",
            "45",
            "46",
            "47",
            "48",
            "49",
            "50",
            "51",
            "52",
            "53",
            "54",
            "55",
            "56",
            "57",
            "58",
            "59",
            "60",
            "61",
            "62",
            "63",
            "64",
            "65",
            "66",
            "67",
            "68",
            "69",
            "70",
            "71",
            "72",
            "73",
            "74",
            "75",
            "76",
            "77",
            "78",
            "79",
            "80")*/

        try {
            uploadImage(file1,
                file2,
                file3,
                file4,
                file5,
                txtEstablishment.text.toString(),
                txtPlantilla.text.toString(),
                txtAsistencias.text.toString(),
                txtPorcentajeAsistencias.text.toString(),
                txtFaltasdMes.text.toString(),
                txtEvaluacion.text.toString(),
                if(rbDesempeño1.isChecked) true.toString() else false.toString(),
                if(rbOportunidad1.isChecked) true.toString() else false.toString(),
                if(rbCorregir1.isChecked) true.toString() else false.toString(),
                if(rbDesempeño2.isChecked) true.toString() else false.toString(),
                if(rbOportunidad2.isChecked) true.toString() else false.toString(),
                if(rbCorregir2.isChecked) true.toString() else false.toString(),
                if(rbDesempeño3.isChecked) true.toString() else false.toString(),
                if(rbOportunidad3.isChecked) true.toString() else false.toString(),
                if(rbCorregir3.isChecked) true.toString() else false.toString(),
                if(rbDesempeño4.isChecked) true.toString() else false.toString(),
                if(rbOportunidad4.isChecked) true.toString() else false.toString(),
                if(rbCorregir4.isChecked) true.toString() else false.toString(),
                if(rbDesempeño5.isChecked) true.toString() else false.toString(),
                if(rbOportunidad5.isChecked) true.toString() else false.toString(),
                if(rbCorregir5.isChecked) true.toString() else false.toString(),
                if(rbDesempeño6.isChecked) true.toString() else false.toString(),
                if(rbOportunidad6.isChecked) true.toString() else false.toString(),
                if(rbCorregir6.isChecked) true.toString() else false.toString(),
                if(rbDesempeño7.isChecked) true.toString() else false.toString(),
                if(rbOportunidad7.isChecked) true.toString() else false.toString(),
                if(rbCorregir7.isChecked) true.toString() else false.toString(),
                if(rbDesempeño8.isChecked) true.toString() else false.toString(),
                if(rbOportunidad8.isChecked) true.toString() else false.toString(),
                if(rbCorregir8.isChecked) true.toString() else false.toString(),
                if(rbDesempeño9.isChecked) true.toString() else false.toString(),
                if(rbOportunidad9.isChecked) true.toString() else false.toString(),
                if(rbCorregir9.isChecked) true.toString() else false.toString(),
                if(rbDesempeño10.isChecked) true.toString() else false.toString(),
                if(rbOportunidad10.isChecked) true.toString() else false.toString(),
                if(rbCorregir10.isChecked) true.toString() else false.toString(),
                if(rbDesempeño11.isChecked) true.toString() else false.toString(),
                if(rbOportunidad11.isChecked) true.toString() else false.toString(),
                if(rbCorregir11.isChecked) true.toString() else false.toString(),
                if(rbDesempeño12.isChecked) true.toString() else false.toString(),
                if(rbOportunidad12.isChecked) true.toString() else false.toString(),
                if(rbCorregir12.isChecked) true.toString() else false.toString(),
                if(rbDesempeño13.isChecked) true.toString() else false.toString(),
                if(rbOportunidad13.isChecked) true.toString() else false.toString(),
                if(rbCorregir13.isChecked) true.toString() else false.toString(),
                if(rbDesempeño14.isChecked) true.toString() else false.toString(),
                if(rbOportunidad14.isChecked) true.toString() else false.toString(),
                if(rbCorregir14.isChecked) true.toString() else false.toString(),
                if(rbDesempeño15.isChecked) true.toString() else false.toString(),
                if(rbOportunidad15.isChecked) true.toString() else false.toString(),
                if(rbCorregir15.isChecked) true.toString() else false.toString(),
                if(rbDesempeño16.isChecked) true.toString() else false.toString(),
                if(rbOportunidad16.isChecked) true.toString() else false.toString(),
                if(rbCorregir16.isChecked) true.toString() else false.toString(),
                if(rbDesempeño17.isChecked) true.toString() else false.toString(),
                if(rbOportunidad17.isChecked) true.toString() else false.toString(),
                if(rbCorregir17.isChecked) true.toString() else false.toString(),
                if(rbDesempeño18.isChecked) true.toString() else false.toString(),
                if(rbOportunidad18.isChecked) true.toString() else false.toString(),
                if(rbCorregir18.isChecked) true.toString() else false.toString(),
                if(rbDesempeño19.isChecked) true.toString() else false.toString(),
                if(rbOportunidad19.isChecked) true.toString() else false.toString(),
                if(rbCorregir19.isChecked) true.toString() else false.toString(),
                if(rbDesempeño20.isChecked) true.toString() else false.toString(),
                if(rbOportunidad20.isChecked) true.toString() else false.toString(),
                if(rbCorregir20.isChecked) true.toString() else false.toString(),
                if(rbDesempeño21.isChecked) true.toString() else false.toString(),
                if(rbOportunidad21.isChecked) true.toString() else false.toString(),
                if(rbCorregir21.isChecked) true.toString() else false.toString(),
                if(rbDesempeño22.isChecked) true.toString() else false.toString(),
                if(rbOportunidad22.isChecked) true.toString() else false.toString(),
                if(rbCorregir22.isChecked) true.toString() else false.toString(),
                if(rbDesempeño23.isChecked) true.toString() else false.toString(),
                if(rbOportunidad23.isChecked) true.toString() else false.toString(),
                if(rbCorregir23.isChecked) true.toString() else false.toString(),
                if(rbDesempeño24.isChecked) true.toString() else false.toString(),
                if(rbOportunidad24.isChecked) true.toString() else false.toString(),
                if(rbCorregir24.isChecked) true.toString() else false.toString(),
                et_comentarios.text.toString(),
                txtFechaElaboracion.text.toString())
        } catch (ex: Exception) {
            Toast.makeText(activity?.baseContext, "No se han llenado faltan fotos o no se han llenado todos los campos", Toast.LENGTH_SHORT).show()
        }

        /*uploadImage(file1,
            file2,
            file3,
            file4,
            file5,
            txtEstablishment.text.toString(),
            txtPlantilla.text.toString(),
            txtAsistencias.text.toString(),
            txtPorcentajeAsistencias.text.toString(),
            txtFaltasdMes.text.toString(),
            txtEvaluacion.text.toString(),
            if(rbDesempeño1.isChecked) true.toString() else false.toString(),
            if(rbOportunidad1.isChecked) true.toString() else false.toString(),
            if(rbCorregir1.isChecked) true.toString() else false.toString(),
            if(rbDesempeño2.isChecked) true.toString() else false.toString(),
            if(rbOportunidad2.isChecked) true.toString() else false.toString(),
            if(rbCorregir2.isChecked) true.toString() else false.toString(),
            if(rbDesempeño3.isChecked) true.toString() else false.toString(),
            if(rbOportunidad3.isChecked) true.toString() else false.toString(),
            if(rbCorregir3.isChecked) true.toString() else false.toString(),
            if(rbDesempeño4.isChecked) true.toString() else false.toString(),
            if(rbOportunidad4.isChecked) true.toString() else false.toString(),
            if(rbCorregir4.isChecked) true.toString() else false.toString(),
            if(rbDesempeño5.isChecked) true.toString() else false.toString(),
            if(rbOportunidad5.isChecked) true.toString() else false.toString(),
            if(rbCorregir5.isChecked) true.toString() else false.toString(),
            if(rbDesempeño6.isChecked) true.toString() else false.toString(),
            if(rbOportunidad6.isChecked) true.toString() else false.toString(),
            if(rbCorregir6.isChecked) true.toString() else false.toString(),
            if(rbDesempeño7.isChecked) true.toString() else false.toString(),
            if(rbOportunidad7.isChecked) true.toString() else false.toString(),
            if(rbCorregir7.isChecked) true.toString() else false.toString(),
            if(rbDesempeño8.isChecked) true.toString() else false.toString(),
            if(rbOportunidad8.isChecked) true.toString() else false.toString(),
            if(rbCorregir8.isChecked) true.toString() else false.toString(),
            if(rbDesempeño9.isChecked) true.toString() else false.toString(),
            if(rbOportunidad9.isChecked) true.toString() else false.toString(),
            if(rbCorregir9.isChecked) true.toString() else false.toString(),
            if(rbDesempeño10.isChecked) true.toString() else false.toString(),
            if(rbOportunidad10.isChecked) true.toString() else false.toString(),
            if(rbCorregir10.isChecked) true.toString() else false.toString(),
            if(rbDesempeño11.isChecked) true.toString() else false.toString(),
            if(rbOportunidad11.isChecked) true.toString() else false.toString(),
            if(rbCorregir11.isChecked) true.toString() else false.toString(),
            if(rbDesempeño12.isChecked) true.toString() else false.toString(),
            if(rbOportunidad12.isChecked) true.toString() else false.toString(),
            if(rbCorregir12.isChecked) true.toString() else false.toString(),
            if(rbDesempeño13.isChecked) true.toString() else false.toString(),
            if(rbOportunidad13.isChecked) true.toString() else false.toString(),
            if(rbCorregir13.isChecked) true.toString() else false.toString(),
            if(rbDesempeño14.isChecked) true.toString() else false.toString(),
            if(rbOportunidad14.isChecked) true.toString() else false.toString(),
            if(rbCorregir14.isChecked) true.toString() else false.toString(),
            if(rbDesempeño15.isChecked) true.toString() else false.toString(),
            if(rbOportunidad15.isChecked) true.toString() else false.toString(),
            if(rbCorregir15.isChecked) true.toString() else false.toString(),
            if(rbDesempeño16.isChecked) true.toString() else false.toString(),
            if(rbOportunidad16.isChecked) true.toString() else false.toString(),
            if(rbCorregir16.isChecked) true.toString() else false.toString(),
            if(rbDesempeño17.isChecked) true.toString() else false.toString(),
            if(rbOportunidad17.isChecked) true.toString() else false.toString(),
            if(rbCorregir17.isChecked) true.toString() else false.toString(),
            if(rbDesempeño18.isChecked) true.toString() else false.toString(),
            if(rbOportunidad18.isChecked) true.toString() else false.toString(),
            if(rbCorregir18.isChecked) true.toString() else false.toString(),
            if(rbDesempeño19.isChecked) true.toString() else false.toString(),
            if(rbOportunidad19.isChecked) true.toString() else false.toString(),
            if(rbCorregir19.isChecked) true.toString() else false.toString(),
            if(rbDesempeño20.isChecked) true.toString() else false.toString(),
            if(rbOportunidad20.isChecked) true.toString() else false.toString(),
            if(rbCorregir20.isChecked) true.toString() else false.toString(),
            if(rbDesempeño21.isChecked) true.toString() else false.toString(),
            if(rbOportunidad21.isChecked) true.toString() else false.toString(),
            if(rbCorregir21.isChecked) true.toString() else false.toString(),
            if(rbDesempeño22.isChecked) true.toString() else false.toString(),
            if(rbOportunidad22.isChecked) true.toString() else false.toString(),
            if(rbCorregir22.isChecked) true.toString() else false.toString(),
            if(rbDesempeño23.isChecked) true.toString() else false.toString(),
            if(rbOportunidad23.isChecked) true.toString() else false.toString(),
            if(rbCorregir23.isChecked) true.toString() else false.toString(),
            if(rbDesempeño24.isChecked) true.toString() else false.toString(),
            if(rbOportunidad24.isChecked) true.toString() else false.toString(),
            if(rbCorregir24.isChecked) true.toString() else false.toString(),
            et_comentarios.text.toString(),
            txtFechaElaboracion.text.toString())*/

        Log.e("FORMAT", rbDesempeño24.isChecked.toString())
        Log.e("FORMAT2", rbOportunidad24.isChecked.toString())
        Log.e("FORMAT3", rbCorregir24.isChecked.toString())
    }

    private fun uploadImage(file1: File, file2: File, file3: File, file4: File, file5: File, cliente: String, sucur: String, local: String,
                            perscon: String, felab: String, califica: String, desem1: String, mejora1: String, correg1: String, desem2: String, mejora2: String, correg2: String,
                            desem3: String, mejora3: String, correg3: String, desem4: String, mejora4: String, correg4: String, desem5: String, mejora5: String, correg5: String,
                            desem6: String, mejora6: String, correg6: String, desem7: String, mejora7: String, correg7: String, desem8: String, mejora8: String, correg8: String,
                            desem9: String, mejora9: String, correg9: String, desem10: String, mejora10: String, correg10: String, desem11: String, mejora11: String, correg11: String,
                            desem12: String, mejora12: String, correg12: String, desem13: String, mejora13: String, correg13: String, desem14: String, mejora14: String, correg14: String,
                            desem15: String, mejora15: String, correg15: String, desem16: String, mejora16: String, correg16: String, desem17: String, mejora17: String, correg17: String,
                            desem18: String, mejora18: String, correg18: String, desem19: String, mejora19: String, correg19: String, desem20: String, mejora20: String, correg20: String,
                            desem21: String, mejora21: String, correg21: String, desem22: String, mejora22: String, correg22: String, desem23: String, mejora23: String, correg23: String,
                            desem24: String, mejora24: String, correg24: String, comenta: String, respon: String) {
        //val imgFile = File(getRealPathFromUri(uri))
        /*val uri = FileProvider.getUriForFile(activity?.applicationContext!!, activity?.applicationContext!!.packageName+".fileprovider", file)
        val mediaType = activity?.contentResolver!!.getType(uri).toString()
        val mediaType2 = "text/plain"
        val requestFile = RequestBody.create(mediaType.toMediaTypeOrNull(), file)
        val nameBody = RequestBody.create(mediaType2.toMediaTypeOrNull(), nombre)

        val responsePostCall = apiFDSI?.uploadImage(requestFile, nameBody)
        responsePostCall?.enqueue(object : Callback<String> {
            override fun onFailure(call: Call<String>, t: Throwable) {
                Toast.makeText(activity?.baseContext, "Error: ${t.toString()}", Toast.LENGTH_SHORT).show()
            }

            override fun onResponse(call: Call<String>, response: Response<String>) {
                if (response.isSuccessful) {
                    if (response.body() != null) {
                        val jsonResponse = response.body().toString()
                        parseRegData(jsonResponse)
                        Toast.makeText(activity?.baseContext, "Image updated successfuly", Toast.LENGTH_SHORT).show()
                    } else {
                        Toast.makeText(activity?.baseContext, "Some error ocurred...", Toast.LENGTH_SHORT).show()
                    }
                }
            }

        })*/

        val uri1 = FileProvider.getUriForFile(activity?.applicationContext!!, activity?.applicationContext!!.packageName+".fileprovider", file1)
        val uri2 = FileProvider.getUriForFile(activity?.applicationContext!!, activity?.applicationContext!!.packageName+".fileprovider", file2)
        val uri3 = FileProvider.getUriForFile(activity?.applicationContext!!, activity?.applicationContext!!.packageName+".fileprovider", file3)
        val uri4 = FileProvider.getUriForFile(activity?.applicationContext!!, activity?.applicationContext!!.packageName+".fileprovider", file4)
        val uri5 = FileProvider.getUriForFile(activity?.applicationContext!!, activity?.applicationContext!!.packageName+".fileprovider", file5)
        val mediaType1 = activity?.contentResolver!!.getType(uri1).toString()
        val mediaType2 = "application/json"
        val mediaType3 = activity?.contentResolver!!.getType(uri2).toString()
        val mediaType4 = activity?.contentResolver!!.getType(uri3).toString()
        val mediaType5 = activity?.contentResolver!!.getType(uri4).toString()
        val mediaType6 = activity?.contentResolver!!.getType(uri5).toString()
        val requestFile1 = RequestBody.create(mediaType1.toMediaTypeOrNull(), file1)
        val requestFile2 = RequestBody.create(mediaType3.toMediaTypeOrNull(), file2)
        val requestFile3 = RequestBody.create(mediaType4.toMediaTypeOrNull(), file3)
        val requestFile4 = RequestBody.create(mediaType5.toMediaTypeOrNull(), file4)
        val requestFile5 = RequestBody.create(mediaType6.toMediaTypeOrNull(), file5)
        val clienteBody = RequestBody.create(mediaType2.toMediaTypeOrNull(), cliente)
        val sucurBody = RequestBody.create(mediaType2.toMediaTypeOrNull(), sucur)
        val localBody = RequestBody.create(mediaType2.toMediaTypeOrNull(), local)
        val persconBody = RequestBody.create(mediaType2.toMediaTypeOrNull(), perscon)
        val felabBody = RequestBody.create(mediaType2.toMediaTypeOrNull(), felab)
        val calificaBody = RequestBody.create(mediaType2.toMediaTypeOrNull(), califica)
        val desem1Body = RequestBody.create(mediaType2.toMediaTypeOrNull(), desem1)
        val mejora1Body = RequestBody.create(mediaType2.toMediaTypeOrNull(), mejora1)
        val correg1Body = RequestBody.create(mediaType2.toMediaTypeOrNull(), correg1)
        val desem2Body = RequestBody.create(mediaType2.toMediaTypeOrNull(), desem2)
        val mejora2Body = RequestBody.create(mediaType2.toMediaTypeOrNull(), mejora2)
        val correg2Body = RequestBody.create(mediaType2.toMediaTypeOrNull(), correg2)
        val desem3Body = RequestBody.create(mediaType2.toMediaTypeOrNull(), desem3)
        val mejora3Body = RequestBody.create(mediaType2.toMediaTypeOrNull(), mejora3)
        val correg3Body = RequestBody.create(mediaType2.toMediaTypeOrNull(), correg3)
        val desem4Body = RequestBody.create(mediaType2.toMediaTypeOrNull(), desem4)
        val mejora4Body = RequestBody.create(mediaType2.toMediaTypeOrNull(), mejora4)
        val correg4Body = RequestBody.create(mediaType2.toMediaTypeOrNull(), correg4)
        val desem5Body = RequestBody.create(mediaType2.toMediaTypeOrNull(), desem5)
        val mejora5Body = RequestBody.create(mediaType2.toMediaTypeOrNull(), mejora5)
        val correg5Body = RequestBody.create(mediaType2.toMediaTypeOrNull(), correg5)
        val desem6Body = RequestBody.create(mediaType2.toMediaTypeOrNull(), desem6)
        val mejora6Body = RequestBody.create(mediaType2.toMediaTypeOrNull(), mejora6)
        val correg6Body = RequestBody.create(mediaType2.toMediaTypeOrNull(), correg6)
        val desem7Body = RequestBody.create(mediaType2.toMediaTypeOrNull(), desem7)
        val mejora7Body = RequestBody.create(mediaType2.toMediaTypeOrNull(), mejora7)
        val correg7Body = RequestBody.create(mediaType2.toMediaTypeOrNull(), correg7)
        val desem8Body = RequestBody.create(mediaType2.toMediaTypeOrNull(), desem8)
        val mejora8Body = RequestBody.create(mediaType2.toMediaTypeOrNull(), mejora8)
        val correg8Body = RequestBody.create(mediaType2.toMediaTypeOrNull(), correg8)
        val desem9Body = RequestBody.create(mediaType2.toMediaTypeOrNull(), desem9)
        val mejora9Body = RequestBody.create(mediaType2.toMediaTypeOrNull(), mejora9)
        val correg9Body = RequestBody.create(mediaType2.toMediaTypeOrNull(), correg9)
        val desem10Body = RequestBody.create(mediaType2.toMediaTypeOrNull(), desem10)
        val mejora10Body = RequestBody.create(mediaType2.toMediaTypeOrNull(), mejora10)
        val correg10Body = RequestBody.create(mediaType2.toMediaTypeOrNull(), correg10)
        val desem11Body = RequestBody.create(mediaType2.toMediaTypeOrNull(), desem11)
        val mejora11Body = RequestBody.create(mediaType2.toMediaTypeOrNull(), mejora11)
        val correg11Body = RequestBody.create(mediaType2.toMediaTypeOrNull(), correg11)
        val desem12Body = RequestBody.create(mediaType2.toMediaTypeOrNull(), desem12)
        val mejora12Body = RequestBody.create(mediaType2.toMediaTypeOrNull(), mejora12)
        val correg12Body = RequestBody.create(mediaType2.toMediaTypeOrNull(), correg12)
        val desem13Body = RequestBody.create(mediaType2.toMediaTypeOrNull(), desem13)
        val mejora13Body = RequestBody.create(mediaType2.toMediaTypeOrNull(), mejora13)
        val correg13Body = RequestBody.create(mediaType2.toMediaTypeOrNull(), correg13)
        val desem14Body = RequestBody.create(mediaType2.toMediaTypeOrNull(), desem14)
        val mejora14Body = RequestBody.create(mediaType2.toMediaTypeOrNull(), mejora14)
        val correg14Body = RequestBody.create(mediaType2.toMediaTypeOrNull(), correg14)
        val desem15Body = RequestBody.create(mediaType2.toMediaTypeOrNull(), desem15)
        val mejora15Body = RequestBody.create(mediaType2.toMediaTypeOrNull(), mejora15)
        val correg15Body = RequestBody.create(mediaType2.toMediaTypeOrNull(), correg15)
        val desem16Body = RequestBody.create(mediaType2.toMediaTypeOrNull(), desem16)
        val mejora16Body = RequestBody.create(mediaType2.toMediaTypeOrNull(), mejora16)
        val correg16Body = RequestBody.create(mediaType2.toMediaTypeOrNull(), correg16)
        val desem17Body = RequestBody.create(mediaType2.toMediaTypeOrNull(), desem17)
        val mejora17Body = RequestBody.create(mediaType2.toMediaTypeOrNull(), mejora17)
        val correg17Body = RequestBody.create(mediaType2.toMediaTypeOrNull(), correg17)
        val desem18Body = RequestBody.create(mediaType2.toMediaTypeOrNull(), desem18)
        val mejora18Body = RequestBody.create(mediaType2.toMediaTypeOrNull(), mejora18)
        val correg18Body = RequestBody.create(mediaType2.toMediaTypeOrNull(), correg18)
        val desem19Body = RequestBody.create(mediaType2.toMediaTypeOrNull(), desem19)
        val mejora19Body = RequestBody.create(mediaType2.toMediaTypeOrNull(), mejora19)
        val correg19Body = RequestBody.create(mediaType2.toMediaTypeOrNull(), correg19)
        val desem20Body = RequestBody.create(mediaType2.toMediaTypeOrNull(), desem20)
        val mejora20Body = RequestBody.create(mediaType2.toMediaTypeOrNull(), mejora20)
        val correg20Body = RequestBody.create(mediaType2.toMediaTypeOrNull(), correg20)
        val desem21Body = RequestBody.create(mediaType2.toMediaTypeOrNull(), desem21)
        val mejora21Body = RequestBody.create(mediaType2.toMediaTypeOrNull(), mejora21)
        val correg21Body = RequestBody.create(mediaType2.toMediaTypeOrNull(), correg21)
        val desem22Body = RequestBody.create(mediaType2.toMediaTypeOrNull(), desem22)
        val mejora22Body = RequestBody.create(mediaType2.toMediaTypeOrNull(), mejora22)
        val correg22Body = RequestBody.create(mediaType2.toMediaTypeOrNull(), correg22)
        val desem23Body = RequestBody.create(mediaType2.toMediaTypeOrNull(), desem23)
        val mejora23Body = RequestBody.create(mediaType2.toMediaTypeOrNull(), mejora23)
        val correg23Body = RequestBody.create(mediaType2.toMediaTypeOrNull(), correg23)
        val desem24Body = RequestBody.create(mediaType2.toMediaTypeOrNull(), desem24)
        val mejora24Body = RequestBody.create(mediaType2.toMediaTypeOrNull(), mejora24)
        val correg24Body = RequestBody.create(mediaType2.toMediaTypeOrNull(), correg24)
        val comentaBody = RequestBody.create(mediaType2.toMediaTypeOrNull(), comenta)
        val respondBody = RequestBody.create(mediaType2.toMediaTypeOrNull(), respon)
        val responseCall = apiFDSI?.uploadInformationSoriana(requestFile1, requestFile2, requestFile3, requestFile4, requestFile5, clienteBody,
            sucurBody, localBody, persconBody, felabBody, calificaBody, desem1Body, mejora1Body, correg1Body, desem2Body, mejora2Body, correg2Body,
            desem3Body, mejora3Body, correg3Body, desem4Body, mejora4Body, correg4Body, desem5Body, mejora5Body, correg5Body, desem6Body, mejora6Body,
            correg6Body, desem7Body, mejora7Body, correg7Body, desem8Body, mejora8Body, correg8Body, desem9Body, mejora9Body, correg9Body, desem10Body,
            mejora10Body, correg10Body, desem11Body, mejora11Body, correg11Body, desem12Body, mejora12Body, correg12Body, desem13Body, mejora13Body,
            correg13Body, desem14Body, mejora14Body, correg14Body, desem15Body, mejora15Body, correg15Body, desem16Body, mejora16Body, correg16Body,
            desem17Body, mejora17Body, correg17Body, desem18Body, mejora18Body, correg18Body, desem19Body, mejora19Body, correg19Body, desem20Body, mejora20Body,
            correg20Body, desem21Body, mejora21Body, correg21Body, desem22Body, mejora22Body, correg22Body, desem23Body, mejora23Body, correg23Body, desem24Body,
            mejora24Body, correg24Body, comentaBody, respondBody)

        progressBar = ProgressDialog(context)
        progressBar?.setCancelable(true)
        progressBar?.setMessage("Guardando...")
        progressBar?.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL)
        progressBar?.progress = 0
        progressBar?.max = 100
        progressBar?.setProgressDrawable(resources.getDrawable(R.drawable.custom_progress))
        //Log.e("Error", response.code().toString())
        progressBar?.show()
        statusBar = 0

        responseCall?.enqueue(object : Callback<String> {
            override fun onFailure(call: Call<String>, t: Throwable) {
                Toast.makeText(activity?.baseContext, "Error: ${t.toString()}", Toast.LENGTH_SHORT).show()
            }

            override fun onResponse(call: Call<String>, response: Response<String>) {
                if (response.isSuccessful){
                    if (response.code() == 200) {
                        //statusBar = 0
                        Thread(Runnable {
                            while (statusBar < 100) {
                                statusBar+=1
                                try {
                                    Thread.sleep(300)
                                } catch (ex: InterruptedException) {
                                    ex.printStackTrace()
                                }

                                progressHandler.post {
                                    progressBar?.progress = statusBar

                                    if (statusBar == 100) {
                                        progressBar?.dismiss()
                                        val jsonResponse = response.body().toString()
                                        parseRegData(jsonResponse)
                                        Toast.makeText(activity?.baseContext, "Se ha guardado exitosamente", Toast.LENGTH_SHORT).show()
                                    }
                                }
                            }
                        }).start()
                    } else {
                        //statusBar = 0
                        Thread(Runnable {
                            while (statusBar < 100) {
                                statusBar+=1
                                try {
                                    Thread.sleep(300)
                                } catch (ex: InterruptedException) {
                                    ex.printStackTrace()
                                }

                                progressHandler.post {
                                    progressBar?.progress = statusBar

                                    if (statusBar == 100) {
                                        progressBar?.dismiss()
                                        Toast.makeText(activity?.baseContext, "Ha ocurrido un error...", Toast.LENGTH_SHORT).show()
                                    }
                                }
                            }
                        }).start()
                    }
                }
            }
        })
    }

    private fun parseRegData(response: String) {
        preferenceHelper.putIsLogin(true)
        try {
            val jsonObject = JSONObject(response)
            if (jsonObject.optString("error").equals("false")) {
                val dataArray = jsonObject.getJSONArray("images")
                for (i in 0 until dataArray.length()) {
                    val dataObj = dataArray.getJSONObject(i)
                    preferenceHelper.putName(dataObj.getString("cliente"))
                    preferenceHelper.putName(dataObj.getString("sucur"))
                    preferenceHelper.putName(dataObj.getString("local"))
                    preferenceHelper.putName(dataObj.getString("perscon"))
                    preferenceHelper.putName(dataObj.getString("felab"))
                    preferenceHelper.putName(dataObj.getString("califica"))
                    preferenceHelper.putName(dataObj.getString("desem1"))
                    preferenceHelper.putName(dataObj.getString("mejora1"))
                    preferenceHelper.putName(dataObj.getString("correg1"))
                    preferenceHelper.putName(dataObj.getString("desem2"))
                    preferenceHelper.putName(dataObj.getString("mejora2"))
                    preferenceHelper.putName(dataObj.getString("correg2"))
                    preferenceHelper.putName(dataObj.getString("desem3"))
                    preferenceHelper.putName(dataObj.getString("mejora3"))
                    preferenceHelper.putName(dataObj.getString("correg3"))
                    preferenceHelper.putName(dataObj.getString("desem4"))
                    preferenceHelper.putName(dataObj.getString("mejora4"))
                    preferenceHelper.putName(dataObj.getString("correg4"))
                    preferenceHelper.putName(dataObj.getString("desem5"))
                    preferenceHelper.putName(dataObj.getString("mejora5"))
                    preferenceHelper.putName(dataObj.getString("correg5"))
                    preferenceHelper.putName(dataObj.getString("desem6"))
                    preferenceHelper.putName(dataObj.getString("mejora6"))
                    preferenceHelper.putName(dataObj.getString("correg6"))
                    preferenceHelper.putName(dataObj.getString("desem7"))
                    preferenceHelper.putName(dataObj.getString("mejora7"))
                    preferenceHelper.putName(dataObj.getString("correg7"))
                    preferenceHelper.putName(dataObj.getString("desem8"))
                    preferenceHelper.putName(dataObj.getString("mejora8"))
                    preferenceHelper.putName(dataObj.getString("correg8"))
                    preferenceHelper.putName(dataObj.getString("desem9"))
                    preferenceHelper.putName(dataObj.getString("mejora9"))
                    preferenceHelper.putName(dataObj.getString("correg9"))
                    preferenceHelper.putName(dataObj.getString("desem10"))
                    preferenceHelper.putName(dataObj.getString("mejora10"))
                    preferenceHelper.putName(dataObj.getString("correg10"))
                    preferenceHelper.putName(dataObj.getString("desem11"))
                    preferenceHelper.putName(dataObj.getString("mejora11"))
                    preferenceHelper.putName(dataObj.getString("correg11"))
                    preferenceHelper.putName(dataObj.getString("desem12"))
                    preferenceHelper.putName(dataObj.getString("mejora12"))
                    preferenceHelper.putName(dataObj.getString("correg12"))
                    preferenceHelper.putName(dataObj.getString("desem13"))
                    preferenceHelper.putName(dataObj.getString("mejora13"))
                    preferenceHelper.putName(dataObj.getString("correg13"))
                    preferenceHelper.putName(dataObj.getString("desem14"))
                    preferenceHelper.putName(dataObj.getString("mejora14"))
                    preferenceHelper.putName(dataObj.getString("correg14"))
                    preferenceHelper.putName(dataObj.getString("desem15"))
                    preferenceHelper.putName(dataObj.getString("mejora15"))
                    preferenceHelper.putName(dataObj.getString("correg15"))
                    preferenceHelper.putName(dataObj.getString("desem16"))
                    preferenceHelper.putName(dataObj.getString("mejora16"))
                    preferenceHelper.putName(dataObj.getString("correg16"))
                    preferenceHelper.putName(dataObj.getString("desem17"))
                    preferenceHelper.putName(dataObj.getString("mejora17"))
                    preferenceHelper.putName(dataObj.getString("correg17"))
                    preferenceHelper.putName(dataObj.getString("desem18"))
                    preferenceHelper.putName(dataObj.getString("mejora18"))
                    preferenceHelper.putName(dataObj.getString("correg18"))
                    preferenceHelper.putName(dataObj.getString("desem19"))
                    preferenceHelper.putName(dataObj.getString("mejora19"))
                    preferenceHelper.putName(dataObj.getString("correg19"))
                    preferenceHelper.putName(dataObj.getString("desem20"))
                    preferenceHelper.putName(dataObj.getString("mejora20"))
                    preferenceHelper.putName(dataObj.getString("correg20"))
                    preferenceHelper.putName(dataObj.getString("desem21"))
                    preferenceHelper.putName(dataObj.getString("mejora21"))
                    preferenceHelper.putName(dataObj.getString("correg21"))
                    preferenceHelper.putName(dataObj.getString("desem22"))
                    preferenceHelper.putName(dataObj.getString("mejora22"))
                    preferenceHelper.putName(dataObj.getString("correg22"))
                    preferenceHelper.putName(dataObj.getString("desem23"))
                    preferenceHelper.putName(dataObj.getString("mejora23"))
                    preferenceHelper.putName(dataObj.getString("correg23"))
                    preferenceHelper.putName(dataObj.getString("desem24"))
                    preferenceHelper.putName(dataObj.getString("mejora24"))
                    preferenceHelper.putName(dataObj.getString("correg24"))
                    preferenceHelper.putName(dataObj.getString("comenta"))
                    preferenceHelper.putName(dataObj.getString("respon"))
                    preferenceHelper.putName(dataObj.getString("revid1"))
                    preferenceHelper.putName(dataObj.getString("revid2"))
                    preferenceHelper.putName(dataObj.getString("revid3"))
                    preferenceHelper.putName(dataObj.getString("revid4"))
                    preferenceHelper.putName(dataObj.getString("revid5"))
                }
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

    /*private fun getRealPathFromUri(uri: Uri): String? {
        val proj = arrayOf(MediaStore.Images.Media.DATA)
        val loader = CursorLoader(activity?.applicationContext!!, uri, proj, null, null, null)
        val cursor = loader.loadInBackground()
        val column_index = cursor?.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
        cursor?.moveToFirst()
        val result = cursor?.getString(column_index!!)
        cursor?.close()
        return result
    }*/

    private fun getDataDirectory(albumName: String): File? {
        val file = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), albumName)
        return file
    }

    /*private fun parseRegData(response: String) {
        try {
            val jsonObject = JSONObject(response)
            if (jsonObject.optString("status").equals("true")) {
                //saveInfo(response)
                Toast.makeText(activity?.baseContext!!, jsonObject.getString("message"), Toast.LENGTH_SHORT).show()
                /*val navOptions = NavOptions.Builder()
                    .setPopUpTo(R.id.stablishmentFragment, true)
                    .setLaunchSingleTop(true)
                    .build()*/
                findNavController().navigate(R.id.loginFragment)
            } else {
                Toast.makeText(activity?.baseContext!!, jsonObject.getString("message"), Toast.LENGTH_SHORT).show()
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

    private fun uploadImageEvidence(bmp: Bitmap) {
        val byteArrayOutputStream = ByteArrayOutputStream()
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream)
        val encodedImage: String = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT)
        val createPostCall = apiFDSI?.uploadImage(encodedImage)
        createPostCall?.enqueue(object : Callback<String> {
            override fun onFailure(call: Call<String>, t: Throwable) {
            }

            override fun onResponse(call: Call<String>, response: Response<String>) {
                Log.e("ResponseString", response.body().toString())
                if (response.isSuccessful) {
                    if (response.body() != null) {
                        val jsonResponse = response.body().toString()
                        try {
                            parseRegData(jsonResponse)
                        } catch (ex: JSONException) {
                            ex.printStackTrace()
                        }
                    } else {
                        Log.e("ResponseString", "Returned empty response")
                    }
                }
            }

        })
    }

    private fun saveImage() {
        val evidenciaDrawable = imgEvidencia.drawable as BitmapDrawable
        val bmp = evidenciaDrawable.bitmap
        uploadImageEvidence(bmp)
    }*/

    /*fun verifyStoragePermissions(activity: Activity?) {
        val permission = ActivityCompat.checkSelfPermission(activity!!, Manifest.permission.WRITE_EXTERNAL_STORAGE)

        if (permission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), 1)
        }
    }*/
}
