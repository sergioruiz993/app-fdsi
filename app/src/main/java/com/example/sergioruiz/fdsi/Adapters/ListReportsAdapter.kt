package com.example.sergioruiz.fdsi.Adapters

import android.content.Context
import android.database.Cursor
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.sergioruiz.fdsi.BaseDatosSQLite.SQLiteClass
import com.example.sergioruiz.fdsi.Models.ReportsModel
import com.example.sergioruiz.fdsi.R

class ListReportsAdapter(var context: Context?, var reports: MutableList<ReportsModel>): RecyclerView.Adapter<ListReportsAdapter.ListReportViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListReportViewHolder {
        return ListReportViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.rv_list_valuation_item, null, false))
    }

    override fun getItemCount(): Int {
        return reports.size
    }

    override fun onBindViewHolder(holder: ListReportViewHolder, position: Int) {
        holder.txvCliente.text = reports[position].cliente
        holder.txvSucursal.text = reports[position].sucursal
        holder.txvFecha.text = reports[position].fecha_elaboracion
        if (holder.txvCliente.text == "Soriana Tienda 260 Echegaray" || holder.txvCliente.text == "Soriana Tienda 277 San Mateo" ||
            holder.txvCliente.text == "Soriana Tienda 258 Lomas Verdes" || holder.txvCliente.text == "Soriana tienda 281 Peri Norte" ||
            holder.txvCliente.text == "Soriana tienda 406 Cuautitlan" || holder.txvCliente.text == "Soriana Tienda 197 Sendero Toluca" ||
            holder.txvCliente.text == "Soriana Tienda 262 Iztapalapa" || holder.txvCliente.text == "Soriana Tienda 397 Parque Delta" ||
            holder.txvCliente.text == "Soriana Tienda 009 La Fe" || holder.txvCliente.text == "Soriana Tienda 027 Santa Maria" ||
            holder.txvCliente.text == "Soriana Tienda 419 Plaza Cantil" || holder.txvCliente.text == "Soriana Tienda 522 Buenavista" ||
            holder.txvCliente.text == "Soriana Tienda 250 Mixcoac" || holder.txvCliente.text == "Soriana Tienda 269 Zaragoza" ||
            holder.txvCliente.text == "Soriana Tienda 255 La Villa" || holder.txvCliente.text == "Soriana Tienda 279 Eugenia" ||
            holder.txvCliente.text == "Soriana Tienda 257 Tacubaya" || holder.txvCliente.text == "Soriana Tienda 682 Pabellón Azcapotzalco" ||
            holder.txvCliente.text == "Soriana Tienda 261 El Rosario" || holder.txvCliente.text == "Soriana Tienda 253 Cuitláhuac" ||
            holder.txvCliente.text == "Soriana Tienda 251 La Viga" || holder.txvCliente.text == "Soriana Tienda 286 El Salado" ||
            holder.txvCliente.text == "Soriana Tienda 418 Consulado" || holder.txvCliente.text == "Soriana Tienda 686 Vía Vallejo" ||
            holder.txvCliente.text == "Soriana Tienda 252 Miyana Polanco" || holder.txvCliente.text == "Soriana Tienda 356 Los Angeles" ||
            holder.txvCliente.text == "Soriana Tienda 349 Montes Berneses" || holder.txvCliente.text == "Soriana Tienda 345 Vista Hermosa" ||
            holder.txvCliente.text == "Soriana Tienda 341 Fundidora" || holder.txvCliente.text == "Soriana Tienda 20 Terraza Coapa") {
            holder.imgLogoStablishment.setImageResource(R.drawable.soriana)
        } else if (holder.txvCliente.text == "Banorte Suc. Banorte Masaryk" || holder.txvCliente.text == "Banorte Suc. IXE Toriello Guerra" ||
            holder.txvCliente.text == "Banorte Suc. Isidro Fabela Toluca" || holder.txvCliente.text == "Banorte Bahia de Santa Barbara" ||
            holder.txvCliente.text == "Banorte Troncoso" || holder.txvCliente.text == "Banorte Jamaica México" ||
            holder.txvCliente.text == "Banorte Aeropuerto llegada Nacional" || holder.txvCliente.text == "Banorte Boulevard Aeropuerto México" ||
            holder.txvCliente.text == "Banorte Agricola Oriental" || holder.txvCliente.text == "Banorte Milpa Alta" ||
            holder.txvCliente.text == "Banorte Palacio de los Deportes" || holder.txvCliente.text == "Banorte Portal Churubusco" ||
            holder.txvCliente.text == "Banorte CAP Hipotecario (Jardín Balbuena)" || holder.txvCliente.text == "Banorte San Pedro de los Pinos" ||
            holder.txvCliente.text == "Banorte Patriotismo Holbein" || holder.txvCliente.text == "Banorte Constituyentes" ||
            holder.txvCliente.text == "Banorte Reforma Lomas" || holder.txvCliente.text == "Banorte Palmas México" ||
            holder.txvCliente.text == "Banorte Multiplaza Aragón I" || holder.txvCliente.text == "Banorte Suc.  La Viga Apatlaco" ||
            holder.txvCliente.text == "Banorte Plaza San Juan de Aragón" || holder.txvCliente.text == "Banorte Cuajimalpa Centro" ||
            holder.txvCliente.text == "Banorte Valle de Aragón" || holder.txvCliente.text == "Banorte La Fontaine" ||
            holder.txvCliente.text == "Banorte Toluca Centro" || holder.txvCliente.text == "Banorte Morelos Toluca" ||
            holder.txvCliente.text == "Banorte Pilares Tolloacan" || holder.txvCliente.text == "Banorte  Rancho Dolores" ||
            holder.txvCliente.text == "Banorte Urawa Tollocan" || holder.txvCliente.text == "Banorte Capacitacion Isidro Fabela" ||
            holder.txvCliente.text == "Banorte Julio Verne" || holder.txvCliente.text == "Banorte Forum Buenavista" ||
            holder.txvCliente.text == "Banorte La Villa México" || holder.txvCliente.text == "Banorte Lindavista México" ||
            holder.txvCliente.text == "Banorte Torres Lindavista" || holder.txvCliente.text == "Banorte Direccion Regional Lindavista" ||
            holder.txvCliente.text == "Banorte Cuautitlán Romero Rubio" || holder.txvCliente.text == "Banorte Parque Industrial Naucalpan" ||
            holder.txvCliente.text == "Banorte Suc. Banorte San Cosme" || holder.txvCliente.text == "Banorte Valle Dorado" ||
            holder.txvCliente.text == "Banorte Circuitos Medicos" || holder.txvCliente.text == "Banorte Ecatepec" ||
            holder.txvCliente.text == "Banorte Cuautitlán Izcalli" || holder.txvCliente.text == "Banorte Villas de la Hacienda Atizapán" ||
            holder.txvCliente.text == "Banorte la Cúspide Naucalpan" || holder.txvCliente.text == "Banorte Hacienda Ojo de Agua" ||
            holder.txvCliente.text == "Banorte Coacalco Power Center" || holder.txvCliente.text == "Banorte Suc. Banorte Atizapán Zona Esmeralda" ||
            holder.txvCliente.text == "Banorte Plaza Jardines" || holder.txvCliente.text == "Banorte Central de Abastos Ecatepec" ||
            holder.txvCliente.text == "Banorte Zumpango" || holder.txvCliente.text == "Banorte La Antigua Tlalnepantla" ||
            holder.txvCliente.text == "Banorte Venustiano Carranza Toluca" || holder.txvCliente.text == "Banorte Pino Suarez Toluca" ||
            holder.txvCliente.text == "Banorte Direccion Regional Sur México Manacar" || holder.txvCliente.text == "Banorte Suc. Banorte Nonoalco" ||
            holder.txvCliente.text == "Banorte Suc. Banorte Alce Blanco" || holder.txvCliente.text == "Banorte Suc. Azcapotzalco" ||
            holder.txvCliente.text == "Banorte Suc. Tacubaya" || holder.txvCliente.text == "Banorte Suc. Montevideo" ||
            holder.txvCliente.text == "Banorte Suc. Cto. Cmal. Santa Fe" || holder.txvCliente.text == "Banorte Suc. Atizapán Alamedas" ||
            holder.txvCliente.text == "Banorte Bellavista Gustavo Baz" || holder.txvCliente.text == "Banorte  suc. Condesa" ||
            holder.txvCliente.text == "Banorte suc. Pedregal Santa Teresa" || holder.txvCliente.text == "Banorte suc. Camara de Diputados" ||
            holder.txvCliente.text == "Banorte Direccion Regional Optima Palmas" || holder.txvCliente.text == "Banorte suc. Av. De la Paz" ||
            holder.txvCliente.text == "Banorte Cajeros T1 y T2" || holder.txvCliente.text == "Banorte Circuito C. Comercial Satelite" ||
            holder.txvCliente.text == "Banorte World Trade Center" || holder.txvCliente.text == "Banorte San Jerónimo" ||
            holder.txvCliente.text == "Banorte Zaragoza Agricola" || holder.txvCliente.text == "Banorte Legaria" ||
            holder.txvCliente.text == "Banorte Huixquilucan" || holder.txvCliente.text == "Banorte Tecámac" ||
            holder.txvCliente.text == "Banorte Angel de La Independencia" || holder.txvCliente.text == "Banorte Pedregal" ||
            holder.txvCliente.text == "Banorte Fuentes de Satelite" || holder.txvCliente.text == "Banorte Copilco" ||
            holder.txvCliente.text == "Banorte Tlalpan Azteca" || holder.txvCliente.text == "Banorte bodega Eulalia Guzman" ||
            holder.txvCliente.text == "Banorte suc. Venustiano Carranza" || holder.txvCliente.text == "Banorte Suc. Oficinas Masaryk" ||
            holder.txvCliente.text == "Banorte Avenida Toluca" || holder.txvCliente.text == "Banorte Taxqueña" ||
            holder.txvCliente.text == "Banorte Mariscal Sucre" || holder.txvCliente.text == "Banorte Fuentes Brotantes" ||
            holder.txvCliente.text == "Banorte División del Norte" || holder.txvCliente.text == "Banorte Medica Sur" ||
            holder.txvCliente.text == "Banorte Villa Coapa" || holder.txvCliente.text == "Banorte Coaplaza" ||
            holder.txvCliente.text == "Banorte Perisur" || holder.txvCliente.text == "Banorte Pacífico" ||
            holder.txvCliente.text == "Banorte Baranca del Muerto" || holder.txvCliente.text == "Banorte CPI Coyoacan" ||
            holder.txvCliente.text == "Banorte Eugenia" || holder.txvCliente.text == "Banorte Manacar" ||
            holder.txvCliente.text == "Banorte Universidad México" || holder.txvCliente.text == "Banorte Merced" ||
            holder.txvCliente.text == "Banorte San Lorenzo" || holder.txvCliente.text == "Banorte 5 de Mayo" ||
            holder.txvCliente.text == "Banorte Condesa I" || holder.txvCliente.text == "Banorte Suc. Banorte Lázaro Cárdenas" ||
            holder.txvCliente.text == "Banorte Suc. Banorte Heriberto Enriquez Toluca" || holder.txvCliente.text == "Banorte Suc. Irrigación") {
            holder.imgLogoStablishment.setImageResource(R.drawable.banorte)
        }
        holder.itemView.setOnClickListener {
            val cliente = holder.txvCliente.text.toString()
            val fecha = holder.txvFecha.text.toString()
            val sucursal = holder.txvSucursal.text.toString()
            val args = bundleOf("cliente" to cliente, "fecha" to fecha, "sucursal" to sucursal)
            val bundle2 = Bundle()
            bundle2.putBundle("bundle2", args)
            holder.itemView.findNavController().navigate(R.id.valuationFormatFragment, bundle2)
        }
    }

    inner class ListReportViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        var txvCliente: TextView = itemView.findViewById(R.id.txvCliente)
        var txvSucursal: TextView = itemView.findViewById(R.id.txvSucursal)
        var txvFecha: TextView = itemView.findViewById(R.id.txvFecha)
        var imgLogoStablishment: ImageView = itemView.findViewById(R.id.imgLogoStablishment)
    }
}