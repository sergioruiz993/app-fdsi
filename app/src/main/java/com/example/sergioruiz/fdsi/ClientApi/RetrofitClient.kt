package com.example.sergioruiz.fdsi.ClientApi

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.util.concurrent.TimeUnit

class RetrofitClient {
    var retrofit: Retrofit? = null
    var BASE_URL_PRUEBA = "http://aztechsolutions.online/"
    var BASE_URL_PRODUCCION = "http://www.fdsi-bd.com.mx/fdsi-information/"

    fun getClient(): Retrofit? {
        if (retrofit == null) {
            /*val httpClient = OkHttpClient.Builder()
            httpClient.readTimeout(5, TimeUnit.MINUTES)
            httpClient.connectTimeout(5, TimeUnit.MINUTES)
            httpClient.addInterceptor(object : Interceptor {
                override fun intercept(chain: Interceptor.Chain): Response {
                    val original = chain.request()
                    val requestBuild = original.newBuilder()
                    val request = requestBuild.build()
                    return chain.proceed(request)
                }
            })

            val gson = GsonBuilder()
                .setLenient()
                .create()


            val client = httpClient.build()
            retrofit = Retrofit.Builder()
                .baseUrl("http://aztechsolutions.online/")
                .client(client)
                //.addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()*/

            /*val httpClient = OkHttpClient.Builder()
                .addInterceptor { chain ->
                    val original = chain.request()
                    val requestBuild = original.newBuilder()
                        .addHeader("Content-Type", "application/json")
                    val request = requestBuild.build()
                    chain.proceed(request)
                }
                .build()

            val gson: Gson = GsonBuilder()
                .setLenient()
                .create()*/

            retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL_PRODUCCION)
                //.client(httpClient)
                .addConverterFactory(ScalarsConverterFactory.create())
                .build()
        }
        return retrofit
    }
}

class RetrofitClient2 {

    var retrofit2: Retrofit? = null
    var BASE_URL_PRODUCCION = "http://www.fdsi-bd.com.mx/fdsi-information/"
    fun getClient2(): Retrofit? {
        var interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        var okhttpclient = OkHttpClient.Builder().addInterceptor(object : Interceptor {
            override fun intercept(chain: Interceptor.Chain): Response {
                var original = chain.request()
                var requestBuilder = original.newBuilder()
                    .method(original.method, original.body)
                    .addHeader("Content-Type", "application/json")
                var request = requestBuilder.build()
                return chain.proceed(request)
            }
        }).addInterceptor(interceptor).connectTimeout(60, TimeUnit.SECONDS).readTimeout(60, TimeUnit.SECONDS).build()
        //val gson = GsonBuilder().setLenient().create()
        if (retrofit2 == null) {
            retrofit2 = Retrofit.Builder()
                .baseUrl(BASE_URL_PRODUCCION)
                //.client(httpClient)
                .addConverterFactory(ScalarsConverterFactory.create())
                .client(okhttpclient)
                .build()
        }
        return retrofit2
    }
}