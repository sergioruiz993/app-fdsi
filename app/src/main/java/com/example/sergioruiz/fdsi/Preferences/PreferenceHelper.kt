package com.example.sergioruiz.fdsi.Preferences

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences

class PreferenceHelper(context: Context) {
    private var INTRO = "Intro"
    private var NOMBRE = "Nombre"
    private var USUARIO = "Usuario"
    private var CONTRAS = "Contras"
    private var name = "nombre"
    private var url = "url"
    private var app_prefs: SharedPreferences? = null
    private var context: Context? = null

    init {
        app_prefs = context.getSharedPreferences("shared", Context.MODE_PRIVATE)
        this.context = context
    }

    fun putIsLogin(loginrout: Boolean) {
        val edit = app_prefs?.edit()
        edit?.putBoolean(INTRO, loginrout)
        edit?.apply()
    }

    fun getIsLogin(): Boolean {
        return app_prefs?.getBoolean(INTRO, false)!!
    }

    fun putNombre(loginrout: String) {
        val edit = app_prefs?.edit()
        edit?.putString(NOMBRE, loginrout)
        edit?.apply()
    }

    fun getNombre(): String {
        return app_prefs?.getString(NOMBRE, "")!!
    }

    fun putUsuario(loginrout: String) {
        val edit = app_prefs?.edit()
        edit?.putString(USUARIO, loginrout)
        edit?.apply()
    }

    fun getUsuario(): String {
        return app_prefs?.getString(USUARIO, "")!!
    }

    fun putName(loginrout: String) {
        val edit = app_prefs?.edit()
        edit?.putString(name, loginrout)
        edit?.apply()
    }

    fun putUrl(loginrout: String) {
        val edit = app_prefs?.edit()
        edit?.putString(url, loginrout)
        edit?.apply()
    }
}