package com.example.sergioruiz.fdsi.BaseDatosSQLite

import android.annotation.SuppressLint
import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteDatabase.CursorFactory
import android.database.sqlite.SQLiteOpenHelper
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import androidx.core.os.bundleOf
import com.itextpdf.text.Image
import java.io.ByteArrayOutputStream
import java.sql.Blob

class SQLiteClass(context: Context?) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {
    companion object {
        const val DATABASE_VERSION = 1
        const val DATABASE_NAME = "estados.db"
    }
    val TABLA_EVALUACION = "estados_stablishment"
    val COLUMNA_ID = "id"
    val COLUMNA_CLIENTE = "cliente"
    val COLUMNA_SUCURSAL = "sucursal"
    val COLUMNA_LOCALIDAD = "localidad"
    val PERSONAL_CONTRATADO = "personal_contratado"
    val RESPONSABLE = "responsable"
    val FECHA_ELABORACION = "fecha_elaboracion"
    val EVALUACION = "evaluacion"

    val PERSONAL_UNIFORMADO = "personal_uniformado"
    val DESEPÑETO_OPTIMO_1 = "depempeño_optimo_1"
    val OPORTUNIDAD_DE_MEJORA_1 = "oportunidad_de_mejora_1"
    val POR_CORREGIR_1 = "por_corregir_1"

    val PERSONAL_CON_CREDENCIAL = "personal_con_credencial"
    val DESEPÑETO_OPTIMO_2 = "depempeño_optimo_2"
    val OPORTUNIDAD_DE_MEJORA_2 = "oportunidad_de_mejora_2"
    val POR_CORREGIR_2 = "por_corregir_2"

    val MATERIAL_COMPLETO = "material_completo"
    val DESEPÑETO_OPTIMO_3 = "depempeño_optimo_3"
    val OPORTUNIDAD_DE_MEJORA_3 = "oportunidad_de_mejora_3"
    val POR_CORREGIR_3 = "por_corregir_3"

    val EQUIPO_COMPLETO = "equipo_completo"
    val DESEPÑETO_OPTIMO_4 = "depempeño_optimo_4"
    val OPORTUNIDAD_DE_MEJORA_4 = "oportunidad_de_mejora_4"
    val POR_CORREGIR_4 = "por_corregir_4"

    val PASILLO_EXTERIOR_DE_CAJAS = "pasillo_exterior_de_cajas"
    val DESEPÑETO_OPTIMO_5 = "depempeño_optimo_5"
    val OPORTUNIDAD_DE_MEJORA_5 = "oportunidad_de_mejora_5"
    val POR_CORREGIR_5 = "por_corregir_5"

    val CAJAS_Y_PASILLO_INTERIOR = "cajas_y_pasillo_interior"
    val DESEPÑETO_OPTIMO_6 = "depempeño_optimo_6"
    val OPORTUNIDAD_DE_MEJORA_6 = "oportunidad_de_mejora_6"
    val POR_CORREGIR_6 = "por_corregir_6"

    val FRUTAS_Y_VERDURAS = "frutas_y_verduras"
    val DESEPÑETO_OPTIMO_7 = "depempeño_optimo_7"
    val OPORTUNIDAD_DE_MEJORA_7 = "oportunidad_de_mejora_7"
    val POR_CORREGIR_7 = "por_corregir_7"

    val AREA_DE_ALIMENTOS_Y_COMENSALES = "area_de_alimentos_y_comensales"
    val DESEPÑETO_OPTIMO_8 = "depempeño_optimo_8"
    val OPORTUNIDAD_DE_MEJORA_8 = "oportunidad_de_mejora_8"
    val POR_CORREGIR_8 = "por_corregir_8"

    val PASILLO_DE_CONGELADOS_Y_LACTEOS = "pasillo_de_congelados_y_lacteos"
    val DESEPÑETO_OPTIMO_9 = "depempeño_optimo_9"
    val OPORTUNIDAD_DE_MEJORA_9 = "oportunidad_de_mejora_9"
    val POR_CORREGIR_9 = "por_corregir_9"

    val VINOS_Y_LICORES = "vinos_y_licores"
    val DESEPÑETO_OPTIMO_10 = "depempeño_optimo_10"
    val OPORTUNIDAD_DE_MEJORA_10 = "oportunidad_de_mejora_10"
    val POR_CORREGIR_10 = "por_corregir_10"

    val PAPELERIA = "papeleria"
    val DESEPÑETO_OPTIMO_11 = "depempeño_optimo_11"
    val OPORTUNIDAD_DE_MEJORA_11 = "oportunidad_de_mejora_11"
    val POR_CORREGIR_11 = "por_corregir_11"

    val DEPORTES = "deportes"
    val DESEPÑETO_OPTIMO_12 = "depempeño_optimo_12"
    val OPORTUNIDAD_DE_MEJORA_12 = "oportunidad_de_mejora_12"
    val POR_CORREGIR_12 = "por_corregir_12"

    val JUGUETERIA = "jugueteria"
    val DESEPÑETO_OPTIMO_13 = "depempeño_optimo_13"
    val OPORTUNIDAD_DE_MEJORA_13 = "oportunidad_de_mejora_13"
    val POR_CORREGIR_13 = "por_corregir_13"

    val MUEBLES = "muebles"
    val DESEPÑETO_OPTIMO_14 = "depempeño_optimo_14"
    val OPORTUNIDAD_DE_MEJORA_14 = "oportunidad_de_mejora_14"
    val POR_CORREGIR_14 = "por_corregir_14"

    val ELECTRONICA = "electronica"
    val DESEPÑETO_OPTIMO_15 = "depempeño_optimo_15"
    val OPORTUNIDAD_DE_MEJORA_15 = "oportunidad_de_mejora_15"
    val POR_CORREGIR_15 = "por_corregir_15"

    val QUIMICOS_COMESTIBLES = "quimicos_comestibles"
    val DESEPÑETO_OPTIMO_16 = "depempeño_optimo_16"
    val OPORTUNIDAD_DE_MEJORA_16 = "oportunidad_de_mejora_16"
    val POR_CORREGIR_16 = "por_corregir_16"

    val FERRETERIA_Y_JARDINERIA = "ferreteria_y_jardineria"
    val DESEPÑETO_OPTIMO_17 = "depempeño_optimo_17"
    val OPORTUNIDAD_DE_MEJORA_17 = "oportunidad_de_mejora_17"
    val POR_CORREGIR_17 = "por_corregir_17"

    val LINEABLANCA = "lineablanca"
    val DESEPÑETO_OPTIMO_18 = "depempeño_optimo_18"
    val OPORTUNIDAD_DE_MEJORA_18 = "oportunidad_de_mejora_18"
    val POR_CORREGIR_18 = "por_corregir_18"

    val FARMACIA = "farmacia"
    val DESEPÑETO_OPTIMO_19 = "depempeño_optimo_19"
    val OPORTUNIDAD_DE_MEJORA_19 = "oportunidad_de_mejora_19"
    val POR_CORREGIR_19 = "por_corregir_19"

    val PERFUMERIA = "perfumeria"
    val DESEPÑETO_OPTIMO_20 = "depempeño_optimo_20"
    val OPORTUNIDAD_DE_MEJORA_20 = "oportunidad_de_mejora_20"
    val POR_CORREGIR_20 = "por_corregir_20"

    val ABARROTES = "abarrotes"
    val DESEPÑETO_OPTIMO_21 = "depempeño_optimo_21"
    val OPORTUNIDAD_DE_MEJORA_21 = "oportunidad_de_mejora_21"
    val POR_CORREGIR_21 = "por_corregir_21"

    val ROPA_DAMAS_Y_NIÑAS = "ropa_damas_y_niñas"
    val DESEPÑETO_OPTIMO_22 = "depempeño_optimo_22"
    val OPORTUNIDAD_DE_MEJORA_22 = "oportunidad_de_mejora_22"
    val POR_CORREGIR_22 = "por_corregir_22"

    val CORSETERIA = "corseteria"
    val DESEPÑETO_OPTIMO_23 = "depempeño_optimo_23"
    val OPORTUNIDAD_DE_MEJORA_23 = "oportunidad_de_mejora_23"
    val POR_CORREGIR_23 = "por_corregir_23"

    val BEBES = "bebes"
    val DESEPÑETO_OPTIMO_24 = "depempeño_optimo_24"
    val OPORTUNIDAD_DE_MEJORA_24 = "oportunidad_de_mejora_24"
    val POR_CORREGIR_24 = "por_corregir_24"

    val ROPA_CABALLEROS_Y_NIÑOS = "ropa_caballeros_y_niños"
    val DESEPÑETO_OPTIMO_25 = "depempeño_optimo_25"
    val OPORTUNIDAD_DE_MEJORA_25 = "oportunidad_de_mejora_25"
    val POR_CORREGIR_25 = "por_corregir_25"

    val BAÑOS_DE_CLIENTES = "baños_de_clientes"
    val DESEPÑETO_OPTIMO_26 = "depempeño_optimo_26"
    val OPORTUNIDAD_DE_MEJORA_26 = "oportunidad_de_mejora_26"
    val POR_CORREGIR_26 = "por_corregir_26"

    val BAÑOS_DE_OFICINA = "baños_de_oficina"
    val DESEPÑETO_OPTIMO_27 = "depempeño_optimo_27"
    val OPORTUNIDAD_DE_MEJORA_27 = "oportunidad_de_mejora_27"
    val POR_CORREGIR_27 = "por_corregir_27"


    val RETIRO_DE_BASURA_GENERAL = "retiro_de_basura_general"
    val DESEPÑETO_OPTIMO_28 = "depempeño_optimo_28"
    val OPORTUNIDAD_DE_MEJORA_28 = "oportunidad_de_mejora_28"
    val POR_CORREGIR_28 = "por_corregir_28"

    val MOPEADO_DE_PISOS = "mopeado_de_pisos"
    val DESEPÑETO_OPTIMO_29 = "depempeño_optimo_29"
    val OPORTUNIDAD_DE_MEJORA_29 = "oportunidad_de_mejora_29"
    val POR_CORREGIR_29 = "por_corregir_29"

    val DESMANCHADO_DE_PISO = "desmanchado_de_piso"
    val DESEPÑETO_OPTIMO_30 = "depempeño_optimo_30"
    val OPORTUNIDAD_DE_MEJORA_30 = "oportunidad_de_mejora_30"
    val POR_CORREGIR_30 = "por_corregir_30"

    val LIMPIEZA_DE_BAÑO_DE_ANAQUELES = "limpieza_de_baño_de_anaqueles"
    val DESEPÑETO_OPTIMO_31 = "depempeño_optimo_31"
    val OPORTUNIDAD_DE_MEJORA_31 = "oportunidad_de_mejora_31"
    val POR_CORREGIR_31 = "por_corregir_31"

    val LIMPIEZA_DE_OFICINAS = "limpieza_de_oficinas"
    val DESEPÑETO_OPTIMO_32 = "depempeño_optimo_32"
    val OPORTUNIDAD_DE_MEJORA_32 = "oportunidad_de_mejora_32"
    val POR_CORREGIR_32 = "por_corregir_32"

    val CRISTALES_DE_OFICINA = "cristales_de_oficina"
    val DESEPÑETO_OPTIMO_33 = "depempeño_optimo_33"
    val OPORTUNIDAD_DE_MEJORA_33 = "oportunidad_de_mejora_33"
    val POR_CORREGIR_33 = "por_corregir_33"

    val COMEDOR_DE_OFICINA = "comedor_de_oficina"
    val DESEPÑETO_OPTIMO_34 = "depempeño_optimo_34"
    val OPORTUNIDAD_DE_MEJORA_34 = "oportunidad_de_mejora_34"
    val POR_CORREGIR_34 = "por_corregir_34"

    val DESORILLADOS = "desorillados"
    val DESEPÑETO_OPTIMO_35 = "depempeño_optimo_35"
    val OPORTUNIDAD_DE_MEJORA_35 = "oportunidad_de_mejora_35"
    val POR_CORREGIR_35 = "por_corregir_35"

    val FUGAS_Y_DERRAMES = "fugas_y_derrames"
    val DESEPÑETO_OPTIMO_36 = "depempeño_optimo_36"
    val OPORTUNIDAD_DE_MEJORA_36 = "oportunidad_de_mejora_36"
    val POR_CORREGIR_36 = "por_corregir_36"

    val GOTERAS = "goteras"
    val DESEPÑETO_OPTIMO_37 = "depempeño_optimo_37"
    val OPORTUNIDAD_DE_MEJORA_37 = "oportunidad_de_mejora_37"
    val POR_CORREGIR_37 = "por_corregir_37"

    val EVIDENCE_1 = "evidencia_1"
    val EVIDENCE_2 = "evidencia_2"
    val EVIDENCE_3 = "evidencia_3"

    val SQL_CREAR = "create table $TABLA_EVALUACION (${COLUMNA_ID} integer primary key autoincrement, " +
            "$COLUMNA_CLIENTE text not null, $COLUMNA_SUCURSAL text not null, $COLUMNA_LOCALIDAD text not null, " +
            "$PERSONAL_CONTRATADO text not null, $RESPONSABLE text not null, $FECHA_ELABORACION text not null, $EVALUACION text not null," +
            "$PERSONAL_UNIFORMADO text not null, $DESEPÑETO_OPTIMO_1 text not null, $PERSONAL_CON_CREDENCIAL text not null, $DESEPÑETO_OPTIMO_2 text not null, " +
            "$MATERIAL_COMPLETO text not null, $DESEPÑETO_OPTIMO_3 text not null, $EQUIPO_COMPLETO text not null, $DESEPÑETO_OPTIMO_4 text not null, " +
            "$PASILLO_EXTERIOR_DE_CAJAS text not null, $DESEPÑETO_OPTIMO_5 text not null, $CAJAS_Y_PASILLO_INTERIOR text not null, $DESEPÑETO_OPTIMO_6 text not null, " +
            "$FRUTAS_Y_VERDURAS text not null, $DESEPÑETO_OPTIMO_7 text not null, $AREA_DE_ALIMENTOS_Y_COMENSALES text not null, $DESEPÑETO_OPTIMO_8 text not null," +
            "$PASILLO_DE_CONGELADOS_Y_LACTEOS text not null, $DESEPÑETO_OPTIMO_9 text not null, $VINOS_Y_LICORES text not null, $DESEPÑETO_OPTIMO_10 text not null," +
            "$PAPELERIA text not null, $DESEPÑETO_OPTIMO_11 text not null, $DEPORTES text not null, $DESEPÑETO_OPTIMO_12 text not null, " +
            "$JUGUETERIA text not null, $DESEPÑETO_OPTIMO_13 text not null, $MUEBLES text not null, $DESEPÑETO_OPTIMO_14 text not null, " +
            "$ELECTRONICA text not null, $DESEPÑETO_OPTIMO_15 text not null, $QUIMICOS_COMESTIBLES text not null, $DESEPÑETO_OPTIMO_16 text not null," +
            "$FERRETERIA_Y_JARDINERIA text not null, $DESEPÑETO_OPTIMO_17 text not null, $LINEABLANCA text not null, $DESEPÑETO_OPTIMO_18 text not null," +
            "$FARMACIA text not null, $DESEPÑETO_OPTIMO_19 text not null, $PERFUMERIA text not null, $DESEPÑETO_OPTIMO_20 text not null," +
            "$ABARROTES text not null, $DESEPÑETO_OPTIMO_21 text not null, $ROPA_DAMAS_Y_NIÑAS text not null, $DESEPÑETO_OPTIMO_22 text not null, " +
            "$CORSETERIA text not null, $DESEPÑETO_OPTIMO_23 text not null, $BEBES text not null, $DESEPÑETO_OPTIMO_24 text not null)"

    val tabla_evaluacion_banorte = "estados_stablishment_banorte"
    val columna_id = "id"
    val columna_cliente = "cliente"
    val columna_sucursal = "sucursal"
    val columna_localidad = "localidad"
    val personal_contratado = "personal_contratado"
    val responsableb = "responsable"
    val fecha_elaboracion = "fecha_elaboracion"
    val evaluacionb = "evaluacion"

    val AREA_DE_CAJAS= "area_de_cajas"
    val desempeño_optimo_1 = "depempeño_optimo_1"
    val oportunidad_de_mejora_1 = "oportunidad_de_mejora_1"
    val por_coregir_1 = "por_corregir_1"

    val SANITARIOSB= "SANITARIOS"
    val desempeño_optimo_2 = "depempeño_optimo_2"
    val oportunidad_de_mejora_2 = "oportunidad_de_mejora_2"
    val por_coregir_2 = "por_corregir_2"

    val MOBILIARIOB = "MOBILIARIO"
    val desempeño_optimo_3 = "depempeño_optimo_3"
    val oportunidad_de_mejora_3 = "oportunidad_de_mejora_3"
    val por_coregir_3 = "por_corregir_3"

    val MANTENIMIENTO_DE_PISO= "MANTENIMIENTO_DE_PISO"
    val desempeño_optimo_4 = "depempeño_optimo_4"
    val oportunidad_de_mejora_4 = "oportunidad_de_mejora_4"
    val por_coregir_4 = "por_corregir_4"

    val OFICINASB = "OFICINAS"
    val desempeño_optimo_5 = "depempeño_optimo_5"
    val oportunidad_de_mejora_5 = "oportunidad_de_mejora_5"
    val por_coregir_5 = "por_corregir_5"

    val COCINETA = "COCINETA"
    val desempeño_optimo_6 = "depempeño_optimo_6"
    val oportunidad_de_mejora_6 = "oportunidad_de_mejora_6"
    val por_coregir_6 = "por_corregir_6"

    val PERIFERIAB = "PERIFERIA"
    val desempeño_optimo_7 = "depempeño_optimo_7"
    val oportunidad_de_mejora_7 = "oportunidad_de_mejora_7"
    val por_coregir_7 = "por_corregir_7"

    val VIDRIOS_Y_PERSIANASB= "VIDRIOS_Y_PERSIANAS"
    val desempeño_optimo_8 = "depempeño_optimo_8"
    val oportunidad_de_mejora_8 = "oportunidad_de_mejora_8"
    val por_coregir_8 = "por_corregir_8"

    val CAJEROS_AUTOMATICOS= "CAJEROS_AUTOMATICOS"
    val desempeño_optimo_9 = "depempeño_optimo_9"
    val oportunidad_de_mejora_9 = "oportunidad_de_mejora_9"
    val por_coregir_9 = "por_corregir_9"

    val TAPICERIAB = "TAPICERIA"
    val desempeño_optimo_10 = "depempeño_optimo_10"
    val oportunidad_de_mejora_10 = "oportunidad_de_mejora_10"
    val por_coregir_10 = "por_corregir_10"

    val PARTES_ALTASB= "PARTE_ ALTAS"
    val desempeño_optimo_11 = "depempeño_optimo_11"
    val oportunidad_de_mejora_11 = "oportunidad_de_mejora_11"
    val por_coregir_11 = "por_corregir_11"

    val DESORILLADO_DE_PISOB= "DESORILLADO_DE_PISO"
    val desempeño_optimo_12 = "depempeño_optimo_12"
    val oportunidad_de_mejora_12 = "oportunidad_de_mejora_12"
    val por_coregir_12 = "por_corregir_12"

    val ESTACIONAMIENTO = "ESTACIONAMIENTO"
    val desempeño_optimo_13 = "depempeño_optimo_13"
    val oportunidad_de_mejora_13 = "oportunidad_de_mejora_13"
    val por_coregir_13 = "por_corregir_13"

    val ESCALERASB = "ESCALERAS"
    val desempeño_optimo_14 = "depempeño_optimo_14"
    val oportunidad_de_mejora_14 = "oportunidad_de_mejora_14"
    val por_coregir_14 = "por_corregir_14"

    val UNIFILASB = "UNIFILAS"
    val desempeño_optimo_15 = "depempeño_optimo_15"
    val oportunidad_de_mejora_15 = "oportunidad_de_mejora_15"
    val por_coregir_15 = "por_corregir_15"

    val PUERTAS_DE_VIDRIO= "PUERTAS_DE_VIDRIO"
    val desempeño_optimo_16 = "depempeño_optimo_16"
    val oportunidad_de_mejora_16 = "oportunidad_de_mejora_16"
    val por_coregir_16 = "por_corregir_16"

    val RETIRO_DE_BASURAB= "RETIRO_DE_BASURA"
    val desempeño_optimo_17 = "depempeño_optimo_17"
    val oportunidad_de_mejora_17 = "oportunidad_de_mejora_17"
    val por_coregir_17 = "por_corregir_17"

    val MATERIALES_E_INSUMOSB= "MATERIALES_E_INSUMOS"
    val desempeño_optimo_18 = "depempeño_optimo_18"
    val oportunidad_de_mejora_18 = "oportunidad_de_mejora_18"
    val por_coregir_18 = "por_corregir_18"

    val MAMPARAS_Y_MUROSB= "MAMPARAS_Y_MUROS"
    val desempeño_optimo_19 = "depempeño_optimo_19"
    val oportunidad_de_mejora_19 = "oportunidad_de_mejora_19"
    val por_coregir_19 = "por_corregir_19"

    val SALAS_DE_JUNTASB= "SALAS_DE_JUNTAS"
    val desempeño_optimo_20 = "depempeño_optimo_20"
    val oportunidad_de_mejora_20 = "oportunidad_de_mejora_20"
    val por_coregir_20 = "por_corregir_20"

    val evidencia_1 = "evidencia_1"
    val evidencia_2 = "evidencia_2"
    val evidencia_3 = "evidencia_3"
    val evidencia_4 = "evidencia_4"
    val evidencia_5 = "evidencia_5"
    val evidencia_6 = "evidencia_6"
    val evidencia_7 = "evidencia_7"
    val evidencia_8 = "evidencia_8"
    val evidencia_9 = "evidencia_9"


    val SQL_CREAR_BANORTE = "create table $tabla_evaluacion_banorte (${columna_id} integer primary key autoincrement, " +
            "$columna_cliente text not null, $columna_sucursal text not null, $columna_localidad text not null, " +
            "$personal_contratado text not null, $responsableb text not null, $fecha_elaboracion text not null, $evaluacionb text not null," +
            "$AREA_DE_CAJAS text not null, $desempeño_optimo_1 text not null, $SANITARIOSB text not null, $desempeño_optimo_2 text not null, " +
            "$MOBILIARIOB text not null, $desempeño_optimo_3 text not null, $MANTENIMIENTO_DE_PISO text not null, $desempeño_optimo_4 text not null, " +
            "$OFICINASB text not null, $desempeño_optimo_5 text not null, $COCINETA text not null, $desempeño_optimo_6 text not null, " +
            "$PERIFERIAB text not null, $desempeño_optimo_7 text not null, $VIDRIOS_Y_PERSIANASB text not null, $desempeño_optimo_8 text not null," +
            "$CAJEROS_AUTOMATICOS text not null, $desempeño_optimo_9 text not null, $TAPICERIAB text not null, $desempeño_optimo_10 text not null," +
            "$PARTES_ALTASB text not null, $desempeño_optimo_11 text not null, $DESORILLADO_DE_PISOB text not null, $desempeño_optimo_12 text not null, " +
            "$ESTACIONAMIENTO text not null, $desempeño_optimo_13 text not null, $ESCALERASB text not null, $desempeño_optimo_14 text not null, " +
            "$UNIFILASB text not null, $desempeño_optimo_15 text not null, $PUERTAS_DE_VIDRIO text not null, $desempeño_optimo_16 text not null," +
            "$RETIRO_DE_BASURAB text not null, $desempeño_optimo_17 text not null, $MATERIALES_E_INSUMOSB text not null, $desempeño_optimo_18 text not null," +
            "$MAMPARAS_Y_MUROSB text not null, $desempeño_optimo_19 text not null, $SALAS_DE_JUNTASB text not null, $desempeño_optimo_20 text not null)"

    val TABLA_EVALUACION_RESTO = "estados_stablishment_resto"
    val COLUMNA_ID_RESTO = "id"
    val COLUMNA_CLIENTE_RESTO = "cliente"
    val COLUMNA_SUCURSAL_RESTO = "sucursal"
    val COLUMNA_LOCALIDAD_RESTO = "localidad"
    val PERSONAL_CONTRATADO_RESTO = "personal_contratado_resto"
    val RESPONSABLE_RESTO = "responsable"
    val FECHA_ELABORACION_RESTO = "fecha_elaboracion"
    val EVALUACION_RESTO = "evaluacion"

    val RECEPCION = "recepcion"
    val DESEPÑETO_OP_1 = "depempeño_optimo_1"
    val OPORTUNIDAD_DE_MEJ_1 = "oportunidad_de_mejora_1"
    val POR_CO_1 = "por_corregir_1"

    val SANITARIOS = "sanitarios"
    val DESEPÑETO_OP_2 = "depempeño_optimo_2"
    val OPORTUNIDAD_DE_MEJ_2 = "oportunidad_de_mejora_2"
    val POR_CO_2 = "por_corregir_2"

    val MOBILIARIO = "mobiliario"
    val DESEPÑETO_OP_3 = "depempeño_optimo_3"
    val OPORTUNIDAD_DE_MEJ_3 = "oportunidad_de_mejora_3"
    val POR_CO_3 = "por_corregir_3"

    val MANTENIMIENTO_DEL_PISO = "mantnimiento_del_piso"
    val DESEPÑETO_OP_4 = "depempeño_optimo_4"
    val OPORTUNIDAD_DE_MEJ_4 = "oportunidad_de_mejora_4"
    val POR_CO_4 = "por_corregir_4"

    val OFICINAS = "oficinas"
    val DESEPÑETO_OP_5 = "depempeño_optimo_5"
    val OPORTUNIDAD_DE_MEJ_5 = "oportunidad_de_mejora_5"
    val POR_CO_5 = "por_corregir_5"

    val COCINETA_Y_CAFETERAS = "cocineta_y_cafeteras"
    val DESEPÑETO_OP_6 = "depempeño_optimo_6"
    val OPORTUNIDAD_DE_MEJ_6 = "oportunidad_de_mejora_6"
    val POR_CO_6 = "por_corregir_6"

    val PERIFERIA = "periferia"
    val DESEPÑETO_OP_7 = "depempeño_optimo_7"
    val OPORTUNIDAD_DE_MEJ_7 = "oportunidad_de_mejora_7"
    val POR_CO_7 = "por_corregir_7"

    val VIDRIOS_Y_PERSIANAS = "vidrios_y_persianas"
    val DESEPÑETO_OP_8 = "depempeño_optimo_8"
    val OPORTUNIDAD_DE_MEJ_8 = "oportunidad_de_mejora_8"
    val POR_CO_8 = "por_corregir_8"

    val TAPICERIA = "tapiceria"
    val DESEPÑETO_OP_9 = "depempeño_optimo_9"
    val OPORTUNIDAD_DE_MEJ_9 = "oportunidad_de_mejora_9"
    val POR_CO_9 = "por_corregir_9"

    val PARTES_ALTAS = "partes_altas"
    val DESEPÑETO_OP_10 = "depempeño_optimo_10"
    val OPORTUNIDAD_DE_MEJ_10 = "oportunidad_de_mejora_10"
    val POR_CO_10 = "por_corregir_10"

    val DESORILLADO_DE_PISO = "desorillado_de_piso"
    val DESEPÑETO_OP_11 = "depempeño_optimo_11"
    val OPORTUNIDAD_DE_MEJ_11 = "oportunidad_de_mejora_11"
    val POR_CO_11 = "por_corregir_11"

    val ESTACIONAMIENTOS = "estacionamientos"
    val DESEPÑETO_OP_12 = "depempeño_optimo_12"
    val OPORTUNIDAD_DE_MEJ_12 = "oportunidad_de_mejora_12"
    val POR_CO_12 = "por_corregir_12"

    val ESCALERAS = "escaleras"
    val DESEPÑETO_OP_13 = "depempeño_optimo_13"
    val OPORTUNIDAD_DE_MEJ_13 = "oportunidad_de_mejora_13"
    val POR_CO_13 = "por_corregir_13"

    val UNIFILAS = "unifilas"
    val DESEPÑETO_OP_14 = "depempeño_optimo_14"
    val OPORTUNIDAD_DE_MEJ_14 = "oportunidad_de_mejora_14"
    val POR_CO_14 = "por_corregir_14"

    val PUERTAS = "puertas"
    val DESEPÑETO_OP_15 = "depempeño_optimo_15"
    val OPORTUNIDAD_DE_MEJ_15 = "oportunidad_de_mejora_15"
    val POR_CO_15 = "por_corregir_15"

    val RETIRO_DE_BASURA = "retiro_de_basura"
    val DESEPÑETO_OP_16 = "depempeño_optimo_16"
    val OPORTUNIDAD_DE_MEJ_16 = "oportunidad_de_mejora_16"
    val POR_CO_16 = "por_corregir_16"

    val MATERIALES_E_INSUMOS = "materiales_e_insumos"
    val DESEPÑETO_OP_17 = "depempeño_optimo_17"
    val OPORTUNIDAD_DE_MEJ_17 = "oportunidad_de_mejora_17"
    val POR_CO_17 = "por_corregir_17"

    val MAMPARAS_Y_MUROS = "mamparas_y_muros"
    val DESEPÑE1TO_OP_18 = "depempeño_optimo_18"
    val OPORTUNIDAD_DE_MEJ_18 = "oportunidad_de_mejora_18"
    val POR_CO_18 = "por_corregir_18"

    val PASILLOS = "pasillos"
    val DESEPÑETO_OP_19 = "depempeño_optimo_19"
    val OPORTUNIDAD_DE_MEJ_19 = "oportunidad_de_mejora_19"
    val POR_CO_19 = "por_corregir_19"

    val ELEVADORES = "elevadores"
    val DESEPÑETO_OP_20 = "depempeño_optimo_20"
    val OPORTUNIDAD_DE_MEJ_20 = "oportunidad_de_mejora_20"
    val POR_CO_20 = "por_corregir_20"

    val SALAS_DE_JUNTAS = "salas_de_juntas"
    val DESEPÑETO_OP_21 = "depempeño_optimo_21"
    val OPORTUNIDAD_DE_MEJ_21 = "oportunidad_de_mejora_21"
    val POR_CO_21 = "por_corregir_21"

    val CUADROS_DECORATIVOS = "cuadros_decorativos"
    val DESEPÑETO_OP_22 = "depempeño_optimo_22"
    val OPORTUNIDAD_DE_MEJ_22 = "oportunidad_de_mejora_22"
    val POR_CO_22 = "por_corregir_22"

    val CANCELERIA = "canceleria"
    val DESEPÑETO_OP_23 = "depempeño_optimo_23"
    val OPORTUNIDAD_DE_MEJ_23 = "oportunidad_de_mejora_23"
    val POR_CO_23 = "por_corregir_23"

    val OFICINAS_PRIVADAS = "oficinas_privadas"
    val DESEPÑETO_OP_24 = "depempeño_optimo_24"
    val OPORTUNIDAD_DE_MEJ_24 = "oportunidad_de_mejora_24"
    val POR_CO_24 = "por_corregir_24"

    val MANTENIMIENTO_DE_ALFOMBRAS = "mantenimiento_de_alfombras"
    val DESEPÑETO_OP_25 = "depempeño_optimo_25"
    val OPORTUNIDAD_DE_MEJ_25 = "oportunidad_de_mejora_25"
    val POR_CO_25 = "por_corregir_25"

    val COMEDOR = "comedor"
    val DESEPÑETO_OP_26 = "depempeño_optimo_26"
    val OPORTUNIDAD_DE_MEJ_26 = "oportunidad_de_mejora_26"
    val POR_CO_26 = "por_corregir_26"

    val EVIDENCE_RESTO_1 = "evidencia_1"
    val EVIDENCE_RESTO_2 = "evidencia_2"
    val EVIDENCE_RESTO_3 = "evidencia_3"
    val EVIDENCE_RESTO_4 = "evidencia_4"
    val EVIDENCE_RESTO_5 = "evidencia_5"
    val EVIDENCE_RESTO_6 = "evidencia_6"
    val EVIDENCE_RESTO_7 = "evidencia_7"
    val EVIDENCE_RESTO_8 = "evidencia_8"
    val EVIDENCE_RESTO_9 = "evidencia_9"


    val SQL_CREAR_RESTO = "create table $TABLA_EVALUACION_RESTO (${COLUMNA_ID_RESTO} integer primary key autoincrement, " +
            "$COLUMNA_CLIENTE_RESTO text not null, $COLUMNA_SUCURSAL_RESTO text not null, $COLUMNA_LOCALIDAD_RESTO text not null, " +
            "$PERSONAL_CONTRATADO_RESTO text not null, $RESPONSABLE_RESTO text not null, $FECHA_ELABORACION_RESTO text not null, $EVALUACION_RESTO text not null," +
            "$RECEPCION text not null, $DESEPÑETO_OP_1 text not null, $SANITARIOS text not null, $DESEPÑETO_OP_2 text not null, " +
            "$MOBILIARIO text not null, $DESEPÑETO_OP_3 text not null, $MANTENIMIENTO_DEL_PISO text not null, $DESEPÑETO_OP_4 text not null, " +
            "$OFICINAS text not null, $DESEPÑETO_OP_5 text not null, $COCINETA_Y_CAFETERAS text not null, $DESEPÑETO_OP_6 text not null, " +
            "$PERIFERIA text not null, $DESEPÑETO_OP_7 text not null, $VIDRIOS_Y_PERSIANAS text not null, $DESEPÑETO_OP_8 text not null," +
            "$TAPICERIA text not null, $DESEPÑETO_OP_9 text not null, $PARTES_ALTAS text not null, $DESEPÑETO_OP_10 text not null," +
            "$DESORILLADO_DE_PISO text not null, $DESEPÑETO_OP_11 text not null, $ESTACIONAMIENTOS text not null, $DESEPÑETO_OP_12 text not null, " +
            "$ESCALERAS text not null, $DESEPÑETO_OP_13 text not null, $UNIFILAS text not null, $DESEPÑETO_OP_14 text not null, " +
            "$PUERTAS text not null, $DESEPÑETO_OP_15 text not null, $RETIRO_DE_BASURA text not null, $DESEPÑETO_OP_16 text not null," +
            "$MATERIALES_E_INSUMOS text not null, $DESEPÑETO_OP_17 text not null, $MAMPARAS_Y_MUROS text not null, $DESEPÑE1TO_OP_18 text not null," +
            "$PASILLOS text not null, $DESEPÑETO_OP_19 text not null, $ELEVADORES text not null, $DESEPÑETO_OP_20 text not null," +
            "$SALAS_DE_JUNTAS text not null, $DESEPÑETO_OP_21 text not null, $CUADROS_DECORATIVOS text not null, $DESEPÑETO_OP_22 text not null, " +
            "$CANCELERIA text not null, $DESEPÑETO_OP_23 text not null, $OFICINAS_PRIVADAS text not null, $DESEPÑETO_OP_24 text not null, " +
            "$MANTENIMIENTO_DE_ALFOMBRAS text not null, $DESEPÑETO_OP_25 text not null, $COMEDOR text not null, $DESEPÑETO_OP_26 text not null)"




    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL(SQL_CREAR)
        db.execSQL(SQL_CREAR_BANORTE)
        db.execSQL(SQL_CREAR_RESTO)
    }

    fun agregar(cliente: String?,
                sucursal: String?,
                localidad: String?,
                personal: String?,
                responsable: String?,
                fecha: String?,
                evaluacion: String?,
                question1: String?,
                desempeño1: String?,
                //oportunidad1: String?,
                //corregir1: String?,
                question2: String?,
                desempeño2: String?,
                //oportunidad2: String?,
                //corregir2: String?,
                question3: String?,
                desempeño3: String?,
                //oportunidad3: String?,
                //corregir3: String?,
                question4: String?,
                desempeño4: String?,
                //oportunidad4: String?,
                //corregir4: String?,
                question5: String?,
                desempeño5: String?,
                //oportunidad5: String?,
                //corregir5: String?,
                question6: String?,
                desempeño6: String?,
                //oportunidad6: String?,
                //corregir6: String?,
                question7: String?,
                desempeño7: String?,//,
                //oportunidad7: String?,
                /*corregir7: String?*/
                question8: String?,
                desempeño8: String?,

                question9: String?,
                desempeño9: String?,

                question10: String?,
                desempeño10: String?,

                question11: String?,
                desempeño11: String?,

                question12: String?,
                desempeño12: String?,

                question13: String?,
                desempeño13: String?,

                question14: String?,
                desempeño14: String?,

                question15: String?,
                desempeño15: String?,

                question16: String?,
                desempeño16: String?,

                question17: String?,
                desempeño17: String?,

                question18: String?,
                desempeño18: String?,

                question19: String?,
                desempeño19: String?,

                question20: String?,
                desempeño20: String?,

                question21: String?,
                desempeño21: String?,

                question22: String?,
                desempeño22: String?,

                question23: String?,
                desempeño23: String?,

                question24: String?,
                desempeño24: String?){
        val db = this.writableDatabase

        /*var bitmap: Bitmap? = null
        var stream = ByteArrayOutputStream()
        bitmap?.compress(Bitmap.CompressFormat.JPEG, 100, stream)
        var evidencia = stream.toByteArray()*/

        val values = ContentValues()
        values.put(COLUMNA_CLIENTE, cliente)
        values.put(COLUMNA_SUCURSAL, sucursal)
        values.put(COLUMNA_LOCALIDAD, localidad)
        values.put(PERSONAL_CONTRATADO, personal)
        values.put(RESPONSABLE, responsable)
        values.put(FECHA_ELABORACION, fecha)
        values.put(EVALUACION, evaluacion)
        values.put(PERSONAL_UNIFORMADO, question1)
        values.put(DESEPÑETO_OPTIMO_1, desempeño1)
        //values.put(OPORTUNIDAD_DE_MEJORA_1, oportunidad1)
        //values.put(POR_CORREGIR_1, corregir1)
        values.put(PERSONAL_CON_CREDENCIAL, question2)
        values.put(DESEPÑETO_OPTIMO_2, desempeño2)
        //values.put(OPORTUNIDAD_DE_MEJORA_2, oportunidad2)
        //values.put(POR_CORREGIR_2, corregir2)
        values.put(MATERIAL_COMPLETO, question3)
        values.put(DESEPÑETO_OPTIMO_3, desempeño3)
        //values.put(OPORTUNIDAD_DE_MEJORA_3, oportunidad3)
        //values.put(POR_CORREGIR_3, corregir3)
        values.put(EQUIPO_COMPLETO, question4)
        values.put(DESEPÑETO_OPTIMO_4, desempeño4)
        //values.put(OPORTUNIDAD_DE_MEJORA_4, oportunidad4)
        //values.put(POR_CORREGIR_4, corregir4)
        values.put(PASILLO_EXTERIOR_DE_CAJAS, question5)
        values.put(DESEPÑETO_OPTIMO_5, desempeño5)
        //values.put(OPORTUNIDAD_DE_MEJORA_5, oportunidad5)
        //values.put(POR_CORREGIR_5, corregir5)
        values.put(CAJAS_Y_PASILLO_INTERIOR, question6)
        values.put(DESEPÑETO_OPTIMO_6, desempeño6)
        //values.put(OPORTUNIDAD_DE_MEJORA_6, oportunidad6)
        //values.put(POR_CORREGIR_6, corregir6)
        values.put(FRUTAS_Y_VERDURAS, question7)
        values.put(DESEPÑETO_OPTIMO_7, desempeño7)
        //values.put(OPORTUNIDAD_DE_MEJORA_7, oportunidad7)
        //values.put(POR_CORREGIR_7, corregir7)
        values.put(AREA_DE_ALIMENTOS_Y_COMENSALES, question8)
        values.put(DESEPÑETO_OPTIMO_8, desempeño8)

        values.put(PASILLO_DE_CONGELADOS_Y_LACTEOS, question9)
        values.put(DESEPÑETO_OPTIMO_9, desempeño9)

        values.put(VINOS_Y_LICORES, question10)
        values.put(DESEPÑETO_OPTIMO_10, desempeño10)

        values.put(PAPELERIA, question11)
        values.put(DESEPÑETO_OPTIMO_11, desempeño11)

        values.put(DEPORTES, question12)
        values.put(DESEPÑETO_OPTIMO_12, desempeño12)

        values.put(JUGUETERIA, question13)
        values.put(DESEPÑETO_OPTIMO_13, desempeño13)

        values.put(MUEBLES, question14)
        values.put(DESEPÑETO_OPTIMO_14, desempeño14)

        values.put(ELECTRONICA, question15)
        values.put(DESEPÑETO_OPTIMO_15, desempeño15)

        values.put(QUIMICOS_COMESTIBLES, question16)
        values.put(DESEPÑETO_OPTIMO_16, desempeño16)

        values.put(FERRETERIA_Y_JARDINERIA, question17)
        values.put(DESEPÑETO_OPTIMO_17, desempeño17)

        values.put(LINEABLANCA, question18)
        values.put(DESEPÑETO_OPTIMO_18, desempeño18)

        values.put(FARMACIA, question19)
        values.put(DESEPÑETO_OPTIMO_19, desempeño19)

        values.put(PERFUMERIA, question20)
        values.put(DESEPÑETO_OPTIMO_20, desempeño20)

        values.put(ABARROTES, question21)
        values.put(DESEPÑETO_OPTIMO_21, desempeño21)

        values.put(ROPA_DAMAS_Y_NIÑAS, question22)
        values.put(DESEPÑETO_OPTIMO_22, desempeño22)

        values.put(CORSETERIA, question23)
        values.put(DESEPÑETO_OPTIMO_23, desempeño23)

        values.put(BEBES, question24)
        values.put(DESEPÑETO_OPTIMO_24, desempeño24)

        //var newRowId: Long? = null
        db.insert(TABLA_EVALUACION, null, values)
        db.close()
        //return newRowId!!.toInt()
    }

    fun agregarBanorte(cliente: String?,
                       sucursal: String?,
                       localidad: String?,
                       personal: String?,
                       responsable: String?,
                       fecha: String?,
                       evaluacion: String?,
                       question1: String?,
                       desempeño1: String?,
                       question2: String?,
                       desempeño2: String?,
                       question3: String?,
                       desempeño3: String?,
                       question4: String?,
                       desempeño4: String?,
                       question5: String?,
                       desempeño5: String?,
                       question6: String?,
                       desempeño6: String?,
                       question7: String?,
                       desempeño7: String?,
                       question8: String?,
                       desempeño8: String?,
                       question9: String?,
                       desempeño9: String?,
                       question10: String?,
                       desempeño10: String?,
                       question11: String?,
                       desempeño11: String?,
                       question12: String?,
                       desempeño12: String?,
                       question13: String?,
                       desempeño13: String?,
                       question14: String?,
                       desempeño14: String?,
                       question15: String?,
                       desempeño15: String?,
                       question16: String?,
                       desempeño16: String?,
                       question17: String?,
                       desempeño17: String?,
                       question18: String?,
                       desempeño18: String?,
                       question19: String?,
                       desempeño19: String?,
                       question20: String?,
                       desempeño20: String?) {
        val db = this.writableDatabase

        val values = ContentValues()
        values.put(columna_cliente, cliente)
        values.put(columna_sucursal, sucursal)
        values.put(columna_localidad, localidad)
        values.put(personal_contratado, personal)
        values.put(responsableb, responsable)
        values.put(fecha_elaboracion, fecha)
        values.put(evaluacionb, evaluacion)
        values.put(AREA_DE_CAJAS, question1)
        values.put(desempeño_optimo_1, desempeño1)
        values.put(SANITARIOSB, question2)
        values.put(desempeño_optimo_2, desempeño2)
        values.put(MOBILIARIOB, question3)
        values.put(desempeño_optimo_3, desempeño3)
        values.put(MANTENIMIENTO_DE_PISO, question4)
        values.put(desempeño_optimo_4, desempeño4)
        values.put(OFICINASB, question5)
        values.put(desempeño_optimo_5, desempeño5)
        values.put(COCINETA, question6)
        values.put(desempeño_optimo_6, desempeño6)
        values.put(PERIFERIAB, question7)
        values.put(desempeño_optimo_7, desempeño7)
        values.put(VIDRIOS_Y_PERSIANASB, question8)
        values.put(desempeño_optimo_8, desempeño8)
        values.put(CAJEROS_AUTOMATICOS, question9)
        values.put(desempeño_optimo_9, desempeño9)
        values.put(TAPICERIAB, question10)
        values.put(desempeño_optimo_10, desempeño10)
        values.put(PARTES_ALTASB, question11)
        values.put(desempeño_optimo_11, desempeño11)
        values.put(DESORILLADO_DE_PISOB, question12)
        values.put(desempeño_optimo_12, desempeño12)
        values.put(ESTACIONAMIENTO, question13)
        values.put(desempeño_optimo_13, desempeño13)
        values.put(ESCALERASB, question14)
        values.put(desempeño_optimo_14, desempeño14)
        values.put(UNIFILASB, question15)
        values.put(desempeño_optimo_15, desempeño15)
        values.put(PUERTAS_DE_VIDRIO, question16)
        values.put(desempeño_optimo_16, desempeño16)
        values.put(RETIRO_DE_BASURAB, question17)
        values.put(desempeño_optimo_17, desempeño17)
        values.put(MATERIALES_E_INSUMOSB, question18)
        values.put(desempeño_optimo_18, desempeño18)
        values.put(MAMPARAS_Y_MUROSB, question19)
        values.put(desempeño_optimo_19, desempeño19)
        values.put(SALAS_DE_JUNTASB, question20)
        values.put(desempeño_optimo_20, desempeño20)
        db.insert(TABLA_EVALUACION, null, values)
        db.close()
    }

    fun agregarRestoClientes(cliente: String?,
                             sucursal: String?,
                             localidad: String?,
                             personal: String?,
                             responsable: String?,
                             fecha: String?,
                             evaluacion: String?,
                             question1: String?,
                             desempeño1: String?,
                             question2: String?,
                             desempeño2: String?,
                             question3: String?,
                             desempeño3: String?,
                             question4: String?,
                             desempeño4: String?,
                             question5: String?,
                             desempeño5: String?,
                             question6: String?,
                             desempeño6: String?,
                             question7: String?,
                             desempeño7: String?,
                             question8: String?,
                             desempeño8: String?,
                             question9: String?,
                             desempeño9: String?,
                             question10: String?,
                             desempeño10: String?,
                             question11: String?,
                             desempeño11: String?,
                             question12: String?,
                             desempeño12: String?,
                             question13: String?,
                             desempeño13: String?,
                             question14: String?,
                             desempeño14: String?,
                             question15: String?,
                             desempeño15: String?,
                             question16: String?,
                             desempeño16: String?,
                             question17: String?,
                             desempeño17: String?,
                             question18: String?,
                             desempeño18: String?,
                             question19: String?,
                             desempeño19: String?,
                             question20: String?,
                             desempeño20: String?,
                             question21: String?,
                             desempeño21: String?,
                             question22: String,
                             desempeño22: String?,
                             question23: String?,
                             desempeño23: String?,
                             question24: String?,
                             desempeño24: String?,
                             question25: String?,
                             desempeño25: String?,
                             question26: String,
                             desempeño26: String) {
        val db = this.writableDatabase

        val values = ContentValues()
        values.put(COLUMNA_CLIENTE_RESTO, cliente)
        values.put(COLUMNA_SUCURSAL_RESTO, sucursal)
        values.put(COLUMNA_LOCALIDAD_RESTO, localidad)
        values.put(PERSONAL_CONTRATADO_RESTO, personal)
        values.put(RESPONSABLE_RESTO, responsable)
        values.put(FECHA_ELABORACION_RESTO, fecha)
        values.put(EVALUACION_RESTO, evaluacion)
        values.put(RECEPCION, question1)
        values.put(DESEPÑETO_OP_1, desempeño1)
        values.put(SANITARIOS, question2)
        values.put(DESEPÑETO_OP_2, desempeño2)
        values.put(MOBILIARIO, question3)
        values.put(DESEPÑETO_OP_3, desempeño3)
        values.put(MANTENIMIENTO_DEL_PISO, question4)
        values.put(DESEPÑETO_OP_4, desempeño4)
        values.put(OFICINAS, question5)
        values.put(DESEPÑETO_OP_5, desempeño5)
        values.put(COCINETA_Y_CAFETERAS, question6)
        values.put(DESEPÑETO_OP_6, desempeño6)
        values.put(PERIFERIA, question7)
        values.put(DESEPÑETO_OP_7, desempeño7)
        values.put(VIDRIOS_Y_PERSIANAS, question8)
        values.put(DESEPÑETO_OP_8, desempeño8)
        values.put(TAPICERIA, question9)
        values.put(DESEPÑETO_OP_9, desempeño9)
        values.put(PARTES_ALTAS, question10)
        values.put(DESEPÑETO_OP_10, desempeño10)
        values.put(DESORILLADO_DE_PISO, question11)
        values.put(DESEPÑETO_OP_11, desempeño11)
        values.put(ESTACIONAMIENTOS, question12)
        values.put(DESEPÑETO_OP_12, desempeño12)
        values.put(ESCALERAS, question13)
        values.put(DESEPÑETO_OP_13, desempeño13)
        values.put(UNIFILAS, question14)
        values.put(DESEPÑETO_OP_14, desempeño14)
        values.put(PUERTAS, question15)
        values.put(DESEPÑETO_OP_15, desempeño15)
        values.put(RETIRO_DE_BASURA, question16)
        values.put(DESEPÑETO_OP_16, desempeño16)
        values.put(MATERIALES_E_INSUMOS, question17)
        values.put(DESEPÑETO_OP_17, desempeño17)
        values.put(MAMPARAS_Y_MUROS, question18)
        values.put(DESEPÑE1TO_OP_18, desempeño18)
        values.put(PASILLOS, question19)
        values.put(DESEPÑETO_OP_19, desempeño19)
        values.put(ELEVADORES, question20)
        values.put(DESEPÑETO_OP_20, desempeño20)
        values.put(SALAS_DE_JUNTAS, question21)
        values.put(DESEPÑETO_OP_21, desempeño21)
        values.put(CUADROS_DECORATIVOS, question22)
        values.put(DESEPÑETO_OP_22, desempeño22)
        values.put(CANCELERIA, question23)
        values.put(DESEPÑETO_OP_23, desempeño23)
        values.put(OFICINAS_PRIVADAS, question24)
        values.put(DESEPÑETO_OP_24, desempeño24)
        values.put(MANTENIMIENTO_DE_ALFOMBRAS, question25)
        values.put(DESEPÑETO_OP_25, desempeño25)
        values.put(COMEDOR, question26)
        values.put(DESEPÑETO_OP_26, desempeño26)
        db.insert(TABLA_EVALUACION, null, values)
        db.close()
    }

    @SuppressLint("Recycle")
    fun obtener(fecha: String, cliente: String, database: SQLiteDatabase): Cursor {
        val cursor = database.rawQuery("SELECT * FROM $TABLA_EVALUACION WHERE $FECHA_ELABORACION = ? AND $COLUMNA_CLIENTE = ?", arrayOf(fecha, cliente))
        if (cursor != null) {
            cursor.moveToFirst()
            //db.close()
        }
        return cursor
    }

    fun obtenerBanorte(fecha: String, cliente: String, database: SQLiteDatabase): Cursor {
        val cursor = database.rawQuery("SELECT * FROM $tabla_evaluacion_banorte WHERE $fecha_elaboracion = ? AND $columna_cliente = ?", arrayOf(fecha, cliente))
        if (cursor != null) {
            cursor.moveToFirst()
            //db.close()
        }
        return cursor
    }

    fun obtenerRestoClientes(fecha: String, cliente: String, database: SQLiteDatabase): Cursor {
        val cursor = database.rawQuery("SELECT * FROM $TABLA_EVALUACION_RESTO WHERE $FECHA_ELABORACION_RESTO = ? AND $COLUMNA_CLIENTE_RESTO = ?", arrayOf(fecha, cliente))
        if (cursor != null) {
            cursor.moveToFirst()
            //db.close()
        }
        return cursor
    }

    fun obtenerTodo(database: SQLiteDatabase): Cursor {
        val cursor = database.rawQuery("SELECT * FROM $TABLA_EVALUACION", null)
        return cursor
    }

    fun actualizar(fecha: String?,
                   evaluacion: String?,
                   desempeño1: String?,
                   desempeño2: String?,
                   desempeño3: String?,
                   desempeño4: String?,
                   desempeño5: String?,
                   desempeño6: String?,
                   desempeño7: String?,
                   desempeño8: String?,
                   desempeño9: String?,
                   desempeño10: String?,
                   desempeño11: String?,
                   desempeño12: String?,
                   desempeño13: String?,
                   desempeño14: String?,
                   desempeño15: String?,
                   desempeño16: String?,
                   desempeño17: String?,
                   desempeño18: String?,
                   desempeño19: String?,
                   desempeño20: String?,
                   desempeño21: String?,
                   desempeño22: String?,
                   desempeño23: String?,
                   desempeño24: String?) {
        val db = this.writableDatabase
        val values = ContentValues()
        values.put(EVALUACION, evaluacion)
        values.put(DESEPÑETO_OPTIMO_1, desempeño1)
        values.put(DESEPÑETO_OPTIMO_2, desempeño2)
        values.put(DESEPÑETO_OPTIMO_3, desempeño3)
        values.put(DESEPÑETO_OPTIMO_4, desempeño4)
        values.put(DESEPÑETO_OPTIMO_5, desempeño5)
        values.put(DESEPÑETO_OPTIMO_6, desempeño6)
        values.put(DESEPÑETO_OPTIMO_7, desempeño7)
        values.put(DESEPÑETO_OPTIMO_8, desempeño8)
        values.put(DESEPÑETO_OPTIMO_9, desempeño9)
        values.put(DESEPÑETO_OPTIMO_10, desempeño10)
        values.put(DESEPÑETO_OPTIMO_11, desempeño11)
        values.put(DESEPÑETO_OPTIMO_12, desempeño12)
        values.put(DESEPÑETO_OPTIMO_13, desempeño13)
        values.put(DESEPÑETO_OPTIMO_14, desempeño14)
        values.put(DESEPÑETO_OPTIMO_15, desempeño15)
        values.put(DESEPÑETO_OPTIMO_16, desempeño16)
        values.put(DESEPÑETO_OPTIMO_17, desempeño17)
        values.put(DESEPÑETO_OPTIMO_18, desempeño18)
        values.put(DESEPÑETO_OPTIMO_19, desempeño19)
        values.put(DESEPÑETO_OPTIMO_20, desempeño20)
        values.put(DESEPÑETO_OPTIMO_21, desempeño21)
        values.put(DESEPÑETO_OPTIMO_22, desempeño22)
        values.put(DESEPÑETO_OPTIMO_23, desempeño23)
        values.put(DESEPÑETO_OPTIMO_24, desempeño24)


        db.update(TABLA_EVALUACION, values, "$FECHA_ELABORACION = ?", arrayOf(fecha))
        db.close()
    }


    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
    }

}